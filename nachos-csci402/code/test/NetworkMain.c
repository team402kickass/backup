#include "syscall.h"


int main()
{
	int lock,
		second,
		condition;

	lock = CreateServerLock("lock",sizeof("lock"),-1);
	condition = CreateServerCV("c", sizeof("c"),-1);
	ServerLockAcquire(lock);
	ServerCVWait(condition, lock);
	Write("done,",sizeof("done"),1);
}
