#include "syscall.h"
#include "initHospital.h"

int main()
{
	int i;
    int yieldTimes;

    doCreate();

    while(1)
    {
        
        /*ServerLockAcquire(g_pharmStatusLock);
        for(i = 0; i < NUM_USED; i++)
        {
            if(GetMV(g_pharmLineCount, i) > 0 && GetMV(g_pharmStatus, i) == 2)
            {
                Write("Alerting a lazy pharmacist",
                    sizeof("Alerting a lazy pharmacist"), 1);

                SetMV(g_pharmStatus, 1 , i);
                ServerCVSignal(g_pharmBreakCV[i], g_pharmBreakLock[i]);
                break;
            }
        } 
        ServerLockRelease(g_pharmStatusLock);


        ServerLockAcquire(g_cashStatusLock);
        for(i = 0; i < NUM_USED; i++)
        {
            if(GetMV(g_cashLineCount, i) > 0 && GetMV(g_cashStatus, i) == 2)
            {
                Write("Alerting a lazy cashier",
                    sizeof("Alerting a lazy cashier"), 1);

                SetMV(g_cashStatus, 1 , i);
                ServerCVSignal(g_cashBreakCV[i], g_cashBreakLock[i]);
                break;
            }
        }
        ServerLockRelease(g_cashStatusLock);*/
       

        ServerLockAcquire(g_recepStatusLock);
        for(i = 0; i < NUM_USED; i++)
        {
            if(GetMV(g_recepLineCount, i) > 0 && GetMV(g_recepStatus, i) == 2)
            {
                Write("Alerting a lazy receptionist",
                    sizeof("Alerting a lazy receptionist"), 1);

                SetMV(g_recepStatus, 1 , i);
                ServerLockAcquire(g_recepBreakLock[i]);
                ServerCVSignal(g_recepBreakCV[i],g_recepBreakLock[i]);
                ServerLockRelease(g_recepBreakLock[i]);
                break;
            }
        }
        ServerLockRelease(g_recepStatusLock);
        
      /*  Acquire(g_doorboyStateLock);
        for(i = 0; i < NUM_USED; i++)
        {

            if(g_DoorboyState[i] == 2 && g_doorboyDoctorLine[i] > 0)
            {
                Write("Alerting a lazy doorboy",
                    sizeof("Alerting a lazy doorboy"), 1);

                g_DoorboyState[i] = 1;
                Signal(g_dbBreakCV[i], g_dbBreakLock[i]);
                break;
            }
        }
        Release(g_doorboyStateLock);

        /* IMPLEMENT PAYMENT RECORDS CALCULATIONS ONCE PAYMENT CODE IS IMPLEMENTED PROPERLY */

        yieldTimes = i * 250 + 10000;
        for(i = 0; i < yieldTimes; i++)
        {
            Yield();
        }
    }
}