#include "syscall.h"

int cit_c1;	/* condition */
int cit_l1;	/* lock */

cit_t1()
{
	Write("Starting Condition Input Test", sizeof("Starting Condition Input Test"), 1);
	Write("Test Phase: Wait_Syscall\n", sizeof("Test Phase: Wait_Syscall\n"), 1);

	/* Test all possible invalid inputs for the Wait syscall */
	Write("Testing index < 0: ", sizeof("Testing index < 0: "), 1);
	Acquire(cit_l1);
	Wait(-1, cit_l1);

	Write("Testing index out of bounds: ", sizeof("Testing index out of bounds: "), 1);
	Wait(cit_c1+1, cit_l1);

	Write("Testing lockindex < 0: ", sizeof("Testing lockindex < 0: "), 1);
	Wait(cit_c1, -1);

	Write("Testing lockindex out of bounds: ", sizeof("Testing lockindex out of bounds: "), 1);
	Wait(cit_c1, cit_l1+1);

	Write("\nTest Phase: Signal_Syscall\n", sizeof("\nTest Phase: Signal_Syscall\n"), 1);

	/* Test all possible invalid inputs for Signal syscall */
	Write("Testing index < 0: ", sizeof("Testing index < 0: "), 1);
	Signal(-1, cit_l1);

	Write("Testing index out of bounds: ", sizeof("Testing index out of bounds: "), 1);
	Signal(cit_c1+1, cit_l1);

	Write("Testing lockindex < 0: ", sizeof("Testing lockindex < 0: "), 1);
	Signal(cit_c1, -1);

	Write("Testing lockindex out of bounds: ", sizeof("Testing lockindex out of bounds: "), 1);
	Signal(cit_c1, cit_l1+1);

	Write("\nTest Phase: Broadcast_Syscall\n", sizeof("\nTest Phase: Broadcast_Syscall\n"), 1);

	/* Testing all invalid inputs for the Broadcast syscall */
	Write("Testing index < 0: ", sizeof("Testing index < 0: "), 1);
	Broadcast(-1, cit_l1);

	Write("Testing index out of bounds: ", sizeof("Testing index out of bounds: "), 1);
	Broadcast(cit_c1+1, cit_l1);

	Write("Testing lockindex < 0: ", sizeof("Testing lockindex < 0: "), 1);
	Broadcast(cit_c1, -1);

	Write("Testing lockindex out of bounds: ", sizeof("Testing lockindex out of bounds: "), 1);
	Broadcast(cit_c1, cit_l1+1);

	Release(cit_l1);
	Write("\nEND OF CONDITION BAD INPUT TESTS", sizeof("\nEND OF CONDITION BAD INPUT TESTS"), 1);
	Exit(0);
}

int main()
{
	char* buffer = "Test input condition";

	cit_c1 = CreateCondition(-1, sizeof(buffer));
	cit_c1 = CreateCondition(buffer, sizeof(buffer));

	buffer = "Test condition input lock";
	cit_l1 = CreateLock(buffer, sizeof(buffer));

	Fork((void*)cit_t1);
}









