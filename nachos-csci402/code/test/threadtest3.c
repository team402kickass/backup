#include "syscall.h"

int t3_l1;
int t3_c1;

/* --------------------------------------------------
   t3_waiter()
   These threads will wait on the t3_c1 condition variable.  Only
   one t3_waiter will be released
   -------------------------------------------------- */

void t3_waiter() {
    Acquire(t3_l1);
    Write("t3_waiter Acquired t3_l1, waiting on t3_c1",
    	sizeof("t3_waiter Acquired t3_l1, waiting on t3_c1"), 1);
    Wait(t3_c1, t3_l1);
    Write("t3_waiter freed from t3_c1",
    	sizeof("t3_waiter freed from t3_c1"), 1);
    Release(t3_l1);
    Exit(0);
}

/* --------------------------------------------------
   t3_signaller()
   This thread will signal the t3_c1 condition variable. Only
   one t3_signaller will be released
   -------------------------------------------------- */

void t3_signaller() {
    /* Don't signal until someone's waiting */
    int i;

    for (i = 0; i < 10000 ; i++ ) Yield();
    Acquire(t3_l1);
    Write("t3_signaller Acquired t3_l1, signalling t3_c1",
    	sizeof("t3_signaller Acquired t3_l1, signalling t3_c1"), 1);
    Signal(t3_c1, t3_l1);
    Write("t3_signaller Releasing t3_l1", sizeof("t3_signaller Releasing t3_l1"), 1);
    Release(t3_l1);
    Exit(0);
}

int main()
{
  int i;

	char* buffer = "t3_l1";
	t3_l1 = CreateLock(buffer, sizeof(buffer));

	buffer = "t3_c1";
	t3_c1 = CreateCondition(buffer, sizeof(buffer));

  for(i = 0; i < 5; i++)
  {
	   Fork((void*)t3_waiter);
  }
  Fork((void*)t3_signaller);
}