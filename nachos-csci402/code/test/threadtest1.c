#include "syscall.h"

int t1_l1;


/* --------------------------------------------------
	t1_t1() -- test1 thread 1
	This is the rightful lock owner
   -------------------------------------------------- */

void t1_t1()
{
	int i;
	Acquire(t1_l1);
	Write("t1_t1 Acquired t1_l1, waiting for t3",
		sizeof("t1_t1 Acquired t1_l1, waiting for t3"), 1);
	Yield();
	Yield();

	Write("t1_t1 working in CS", sizeof("t1_t1 working in CS"), 1);
	for(i = 0; i < 100000; i++) ;

	Write("t1_t1 Releasing t1_l1", sizeof("t1_t1 Releasing t1_l1"), 1);
	Release(t1_l1);
	Exit(0);
}

/* --------------------------------------------------
	t1_t2() -- test1 thread 2
	This thread will wait on the held lock.
   -------------------------------------------------- */

void t1_t2()
{
	int i;

	Write("t1_t2 trying to Acquire t1_l1", 
		sizeof("t1_t2 trying to Acquire t1_l1"), 1);
	Acquire(t1_l1);

	Write("t1_t2 Acquired lock t1_l1, working in CS",
		sizeof("t1_t2 Acquired lock t1_l1, working in CS"), 1);
	for(i = 0; i < 10; i++) ;

	Write("t1_t2 Releasing t1_l1", sizeof("t1_t2 Releasing t1_l1"), 1);
	Release(t1_l1);
	Exit(0);
}

/* --------------------------------------------------
	t1_t3() -- test1 thread 3
	This thread will try to release the lock illegally.
   -------------------------------------------------- */

void t1_t3()
{
	int i;

	for(i = 0; i < 3; i++)
	{
		Write("t1_t3 trying to Release t1_l1",
			sizeof("t1_t3 trying to Release t1_l1"), 1);
		Release(t1_l1);
	}
	Exit(0);
}

int main()
{
	char *buffer = "t1_l1";
	t1_l1 = CreateLock(buffer, sizeof(buffer));

	Fork((void*)t1_t1);
	Fork((void*)t1_t2);
	Fork((void*)t1_t3);
}











