
#include "syscall.h"
#include "initHospital.h"

int main()
{
	int index;
    int roomNumber;

    doCreate();
    ServerLockAcquire(g_nextDoorBoyLock);
    index = GetMV(g_nextDoorBoyIndex, 0);
    SetMV(g_nextDoorBoyIndex, index+1, 0);
    ServerLockRelease(g_nextDoorBoyLock);

    while(1)
    {
        ServerLockAcquire(g_doorboyDoctorLineLock);

        ServerLockAcquire(g_doorboyStateLock[index]);
        SetMV(g_DoorboyState, 0, index);
        ServerLockRelease(g_doorboyStateLock[index]);

        if(GetMV(g_doorboyDoctorLine,index) > 0)
        {
            ServerLockAcquire(g_doorboyStateLock[index]);
            SetMV(g_DoorboyState,1,index);
            ServerLockRelease(g_doorboyStateLock[index]);

            ServerCVSignal(g_doorboyDoctorWaitLineCV[index], g_doorboyDoctorLineLock); 
        }
        else
        {
           
            ServerLockAcquire(g_doorboyWaitingForDoctorLock[index]);

            ServerLockAcquire(g_doorboyStateLock[index]);
            SetMV(g_DoorboyState,2,index);
            ServerLockRelease(g_doorboyStateLock[index]);

            MyPrint("Doorboy is going on break because no Doctor Calling",
                sizeof("Doorboy is going on break because no Doctor Calling"), GenerateSingle(index +1 ,0), 0);
            ServerCVWait(g_doorboyWaitingForDoctorCV[index], g_doorboyWaitingForDoctorLock[index]);

            ServerLockAcquire(g_doorboyLock[index]);
            ServerLockRelease(g_doorboyWaitingForDoctorLock[index]);
        }

        ServerCVWait(g_doorboyDoctorWaitLineCV[index], g_doorboyDoctorLineLock);
        
        ServerLockAcquire(g_doctorRoomLock[index]);
        roomNumber = GetMV(g_doctorRoom,index);
        ServerLockRelease(g_doctorRoomLock[index]);
        
        MyPrint("Doorboy has been told to bring a Patient.",
                sizeof("Doorboy has been told to bring a Patient."), GenerateSingle(index +1 ,0), 0);
        ServerCVSignal(g_doorboyDoctorWaitLineCV[index], g_doorboyDoctorLineLock);
        ServerLockRelease(g_doorboyDoctorLineLock);

        /*patient section*/
        ServerLockAcquire(g_doorboyLineLock);
        if(GetMV(g_doorboyLine,index) > 0)
        {
            MyPrint("Doorboy signalled the Patient",
                sizeof("Doorboy signalled the Patient"), GenerateSingle(index +1 ,0), 0);
            ServerCVSignal(g_doorboyWaitLineCV[index], g_doorboyLineLock);

            ServerLockAcquire(g_doorboyLock[index]);
            ServerLockRelease(g_doorboyLineLock);
        }
        else
        {
            ServerLockAcquire(g_doorboyWaitingForPatientLock[index]);
            ServerLockRelease(g_doorboyLineLock);

            ServerLockAcquire(g_doorboyStateLock[index]);
            SetMV(g_DoorboyState,3,index);
            ServerLockRelease(g_doorboyStateLock[index]);

            MyPrint("Doorboy is going on break because no Patients",
                sizeof("Doorboy is going on break because no Patients"), GenerateSingle(index +1 ,0), 0);
            ServerCVWait(g_doorboyWaitingForPatientCV[index], g_doorboyWaitingForPatientLock[index]);

            ServerLockAcquire(g_doorboyLock[index]);
            ServerLockRelease(g_doorboyWaitingForPatientLock[index]);

            ServerCVSignal(g_doorboyPatientCV[index],g_doorboyLock[index]);

        }

        ServerCVWait(g_doorboyPatientCV[index], g_doorboyLock[index]);
        
        ServerLockAcquire(g_doctorRoomLock[index]);
        SetMV(g_roomNumber,roomNumber,index);

        ServerLockRelease(g_doctorRoomLock[index]);
        
        MyPrint("Doorboy has told Patient to go to examination room",
                sizeof("Doorboy has told Patient to go to examination room"), GenerateSingle(index +1 ,0), 0);
        ServerCVSignal(g_doorboyPatientCV[index], g_doorboyLock[index]);
        ServerCVWait(g_doorboyPatientCV[index], g_doorboyLock[index]);

        ServerLockRelease(g_doorboyLock[index]);
    }
}