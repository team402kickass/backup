#include "syscall.h"

int t2_l1;
int t2_c1;

/* --------------------------------------------------
	t2_t1() -- test2 thread 1
	This thread will signal a variable with nothing waiting
   -------------------------------------------------- */

void t2_t1()
{
	Acquire(t2_l1);
	Write("t2_t1 Acquired lock, signalling t2_c1",
		sizeof("t2_t1 Acquired lock, signalling t2_c1"), 1);
	Signal(t2_c1, t2_l1);

	Write("t2_t1 Releasing t2_l1", sizeof("t2_t1 Releasing t2_l1"), 1);
	Release(t2_l1);
	Exit(0);
}

/* --------------------------------------------------
	t2_t2() -- test2 thread 2
	This thread will wait on a pre-signalled variable
   -------------------------------------------------- */

void t2_t2()
{
	Acquire(t2_l1);
	Write("t2_t2 Acquired t2_l1, waiting on t2_c1",
		sizeof("t2_t2 Acquired t2_l1, waiting on t2_c1"), 1);
	Wait(t2_c1, t2_l1);

	Write("t2_t2 Releasing t2_l1", sizeof("t2_t2 Releasing t2_l1"), 1);
	Release(t2_l1);
	Exit(0);
}

int main()
{
	char* buffer = "t2_l1";
	t2_l1 = CreateLock(buffer, sizeof(buffer));

	buffer = "t2_c1";
	t2_c1 = CreateCondition(buffer, sizeof(buffer));

	Fork((void*)t2_t1);
	Fork((void*)t2_t2);
}