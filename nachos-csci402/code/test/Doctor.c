#include "syscall.h"
#include "initHospital.h"

int main()
{	int mvtemp;
	int index;
    int i;
    int doorboyIndex;
    int shortestLine;
    int randomNum;
    int fee;
    g_diagnosis diagnosis;
    
    doCreate();
    ServerLockAcquire( g_nextDoctorLock );
    index = GetMV(g_nextDoctorIndex, 0);
    SetMV(g_nextDoctorIndex, index+1,0);
    ServerLockRelease( g_nextDoctorLock );
    MyPrint("Doctor has been created", 
            sizeof("Doctor has been created"), GenerateSingle(index +1 ,0), 0);
    while(1)
    {
        /*doctor doorboy interaction*/
        ServerLockAcquire(g_doorboyDoctorLineLock);
        SetMV(g_DoctorState,0,index);
        doorboyIndex = 0;
        shortestLine = GetMV(g_doorboyDoctorLine,0);
        for(i = 0; i < NUM_USED; i++)
        {
            if(GetMV(g_doorboyDoctorLine,i) < shortestLine)
            {
                shortestLine = GetMV(g_doorboyDoctorLine,i);
                doorboyIndex = i;
            }
            ServerLockAcquire(g_doorboyStateLock[i]);
            if(GetMV(g_DoorboyState,i) == 2)
            {
                
                doorboyIndex = i;
                SetMV(g_DoorboyState,1,i);
                shortestLine = -1;
                ServerLockRelease(g_doorboyStateLock[i]);
                break;
            }
            ServerLockRelease(g_doorboyStateLock[i]);
        }

        SetMV(g_DoctorState,1,index);
        if(shortestLine > -1)
        {
        	mvtemp = GetMV(g_doorboyDoctorLine,doorboyIndex);
            SetMV(g_doorboyDoctorLine,mvtemp + 1,doorboyIndex);
            
            ServerCVWait(g_doorboyDoctorWaitLineCV[doorboyIndex], g_doorboyDoctorLineLock);
            
            mvtemp = GetMV(g_doorboyDoctorLine,doorboyIndex);
            SetMV(g_doorboyDoctorLine,mvtemp - 1,doorboyIndex);
            
            ServerLockAcquire(g_doorboyDoctorLock[doorboyIndex]);
        }
        else
        {
            ServerLockAcquire(g_doorboyWaitingForDoctorLock[doorboyIndex]);
            ServerCVSignal(g_doorboyWaitingForDoctorCV[doorboyIndex], g_doorboyWaitingForDoctorLock[doorboyIndex]);
            ServerLockAcquire(g_doorboyDoctorLock[doorboyIndex]);
            ServerLockRelease(g_doorboyWaitingForDoctorLock[doorboyIndex]);
        }

        MyPrint("Doctor has told a Doorboy to bring a Patient to Examining Room", 
            sizeof("Doctor has told a Doorboy to bring a Patient to Examining Room"), GenerateSingle(index +1 ,0), 0);
        
        ServerLockAcquire(g_doctorRoomLock[doorboyIndex]);
        SetMV(g_doctorRoom,index,doorboyIndex);
        ServerLockRelease(g_doctorRoomLock[doorboyIndex]);

        ServerCVSignal(g_doorboyDoctorWaitLineCV[doorboyIndex], g_doorboyDoctorLineLock);
         MyPrint("Doctor waiting for doorboy's response", 
            sizeof("Doctor waiting for doorboy's response"), GenerateSingle(index +1 ,0), 0);
        ServerCVWait(g_doorboyDoctorWaitLineCV[doorboyIndex], g_doorboyDoctorLineLock);
        ServerLockRelease(g_doorboyDoctorLock[doorboyIndex]);

        /*doctor patient interaction*/
        ServerLockAcquire(g_pDoctorLock[index]);
        ServerLockRelease(g_doorboyDoctorLineLock);
        
        ServerCVWait(g_pDocCV[index], g_pDoctorLock[index]);

        randomNum = MAX + 10;
        for(i = 0; i < randomNum; i++)
        {
            Yield();
        }
        MyPrint("Doctor is examining a Patient", sizeof("Doctor is examining a Patient"), GenerateSingle(index +1 ,0), 0);
        randomNum = MAX-2;
        diagnosis = NONE;
        fee = 25;
        switch(randomNum)
        {
            case 0:
                MyPrint("Doctor has determined Patient is not sick.", 
                    sizeof("Doctor has determined Patient is not sick."), GenerateSingle(index +1 ,0), 0);
                break;
            case 1:
                diagnosis = COLD;
                SetMV(g_medicine,1,index);/*Ibuprofen*/
                fee += MAX;
                MyPrint("Doctor has determined that the patient has a cold.",
                    sizeof("Doctor has determined that the patient has a cold."), GenerateSingle(index +1 ,0), 0);
                break;
            case 2:
                diagnosis = FOOD_PSNG;
                SetMV(g_medicine,2,index);/*Antibiotic*/
                fee *= randomNum;
                MyPrint("Doctor has determined that the patient has food poisoning.",
                    sizeof("Doctor has determined that the patient has food poisoning."), GenerateSingle(index +1 ,0), 0);
                break;
            case 3:
                diagnosis = STREP;
                SetMV(g_medicine,3,index);/*Antibiotic*/
                fee *- randomNum;
                MyPrint("Doctor has determined that the patient has strep throat.",
                    sizeof("Doctor has determined that the patient has strep throat."), GenerateSingle(index +1 ,0), 0);
                break;
            case 4:
                diagnosis = FLU;
                SetMV(g_medicine,4,index); /*Ibuprofen*/
                fee *= randomNum;
                MyPrint("Doctor has determined that the patient has the flu.",
                    sizeof("Doctor has determined that the patient has the flu."), GenerateSingle(index +1 ,0), 0);
                break;
            default:
                break;
        }
        MyPrint("Doctor is handing prescription to patient.", 
            sizeof("Doctor is handing prescription to patient."), GenerateSingle(index +1 ,0), 0);
        
        ServerCVSignal(g_pDocCV[index], g_pDoctorLock[index]);
        
        ServerCVWait(g_pDocCV[index], g_pDoctorLock[index]);
        
        ServerLockRelease(g_pDoctorLock[index]);

        ServerLockAcquire(g_cashDoctorLock[index]);
        MyPrint("Doctor-cashier code has to be implemented", 
            sizeof("Doctor-cashier code has to be implemented"), GenerateSingle(index +1 ,0), 0);
        ServerLockRelease(g_cashDoctorLock[index]);

        /*re-enter doctor patient interaction*/
        ServerLockAcquire(g_pDoctorLock[index]);
        
        ServerCVSignal(g_pDocCV[index], g_pDoctorLock[index]);
        
        MyPrint("Doctor is signalling Patient to leave. Goodbye Patient.",
            sizeof("Doctor is signalling Patient to leave. Goodbye Patient."), GenerateSingle(index +1 ,0), 0);
        
        ServerCVWait(g_pDocCV[index], g_pDoctorLock[index]);
        
        SetMV(g_currentPatient,-1,index);

        randomNum = MAX;
        if(randomNum == 9)
        {
            SetMV(g_DoctorState,2,index);
        }
        ServerLockRelease(g_pDoctorLock[index]);

        if(GetMV(g_DoctorState,index) == 2)
        {
            randomNum = MAX + 10;
            for(i = 0; i < randomNum; i++)
            {
                Yield();
            }
        }
    }
}