#include "syscall.h"

#define MAX 5



int NUM_USED = 5;


 int g_nextCashierIndex = 0;

 int g_nextPatientIndex = 0;
 int g_nextPharmacistIndex = 0;
 int g_nextReceptionistIndex = 0;
 int g_nextDoctorIndex = 0 ;
 int g_nextDoorBoyIndex = 0;


 /* RECEPTIONIST Variables */
 int g_recepLock[MAX] = {-1,-1,-1,-1,-1};
 int g_recepLineLock = -1;
 int g_recepLineCount[MAX] = {0,0,0,0,0};
 int g_recepLineCV[MAX] = {-1,-1,-1,-1,-1};
 int g_recepPatientCV[MAX] = {-1,-1,-1,-1,-1};
 int g_recepStatus[MAX] = {1,1,1,1,1};
 int g_recepBreakLock[MAX] = {-1,-1,-1,-1,-1};
 int g_recepBreakCV[MAX] = {-1,-1,-1,-1,-1}; 
 int g_recepToken[MAX] = {-1,-1,-1,-1,-1};
 int g_receptokenLock = -1;
 int g_receptoken = 0;
 int g_recepStatusLock = -1;


 /* DOORBOY Variables */
 int g_doorboyLock[MAX] = {-1, -1, -1, -1, -1};
 int g_doorboyPatientCV[MAX] = {-1, -1, -1, -1, -1};
 int g_doorboyWaitingForDoctorLock[MAX] = {-1,-1,-1,-1,-1};
 int g_doorboyWaitingForDoctorCV[MAX] = {-1,-1,-1,-1,-1};
 int g_roomNumber[MAX] = {-1,-1,-1,-1,-1};

 /* doctor line */
 int g_doorboyDoctorLine[MAX] = {0, 0, 0, 0, 0};
 int g_doorboyDoctorLineLock = -1;
 int g_doorboyDoctorWaitLineCV[MAX] = {-1, -1, -1, -1, -1};
 int g_doorboyDoctorLock[MAX] = {-1,-1,-1,-1,-1};

 /* patient line */
 int g_doorboyLineLock = -1;
 int g_doorboyWaitLineCV[MAX] = {-1, -1, -1, -1, -1};
 int g_doctorRoomLock[MAX] = {-1,-1,-1,-1,-1};
 int g_doorboyLine[MAX] = {0, 0, 0, 0, 0};
 int g_doctorRoom[MAX] = {-1, -1, -1, -1, -1};

 /* doorboy break */
 int g_dbBreakLock[MAX] = {-1,-1,-1,-1,-1};
 int g_dbBreakCV[MAX] = {-1, -1, -1, -1, -1};

 /* doorboy waiting on patients */
 int g_doorboyWaitingForPatientLock[MAX] = {-1,-1,-1,-1,-1};
 int g_doorboyWaitingForPatientCV[MAX] = {-1, -1, -1, -1, -1};

 /* doorboy state */
 int g_doorboyStateLock[MAX] = {-1,-1,-1,-1,-1};
 int g_DoorboyState[MAX] = {1, 1, 1, 1, 1};

 /* CASHIER Variables */
 int g_cashLock[MAX] = {-1,-1,-1,-1,-1};
 int g_cashierLineLock = -1;
 int g_cashLineCount[MAX] = {0,0,0,0,0};
 int g_cashLineCV[MAX] = {-1,-1,-1,-1,-1};
 int g_cashPatientCV[MAX] = {-1,-1,-1,-1,-1};
 int g_cashStatus[MAX] = {1,1,1,1,1};
 int g_cashPayment = 0;
 int g_cashPayments[MAX] = {0,0,0,0,0};
 int g_cashTokenLock = -1;
 int g_cashBreakLock[MAX] = {-1,-1,-1,-1,-1};
 int g_cashBreakCV[MAX] = {-1,-1,-1,-1,-1};
 int g_cashStatusLock = -1;
 /*vector for cash payment records*/

 /* PHARMACY CLERK variable*/
 int g_pharmLock[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmLineLock = -1;
 int g_pharmLineCount[MAX] = {0,0,0,0,0};
 int g_pharmLineCV[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmPatientCV[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmStatus[MAX] = {1,1,1,1,1};
 int g_pharmPayment = 0;
 int g_pharmPayments[MAX] = {0,0,0,0,0};
 int g_pharmTokenLock = -1;
 int g_pharmBreakLock[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmBreakCV[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmStatusLock = -1;
 /*vector for pharm payment records*/

 /* DOCTOR Variables */
 int g_pDoctorLock[MAX] = {-1,-1,-1,-1,-1};
 int g_pDocCV[MAX] = {-1, -1, -1, -1, -1};
 int g_cashDoctorLock[MAX] = {-1, -1, -1, -1, -1}; 
 int g_DoctorState[MAX] = {1, 1, 1, 1, 1};
 int g_currentPatient[MAX] = {1, 1, 1, 1, 1};
 typedef enum {NONE, FLU, COLD, FOOD_PSNG, STREP} g_diagnosis;
 typedef enum {None, Ibuprofen, Antibiotic} g_Medicine;
 g_Medicine g_medicine[MAX] = {None, None, None, None, None}; 



 int g_nextCashierLock = -1;
 int g_nextPatientLock = -1;
 int g_nextPharacistLock = -1;
 int g_nextReceptionistLock = -1;
 int g_nextDoctorLock = -1;
 int g_nextDoorBoyLock = -1;


/*put 0 in if you do not want somethign printed out */
int GenerateSingle(int actioner, int reciever)
{
    int number;
    number = actioner * 100;
    number = number + reciever;
    return number;
}

void hospitalManager()
{
    int i;
    int yieldTimes;

    while(1)
    {
        
        Acquire(g_pharmStatusLock);
        for(i = 0; i < NUM_USED; i++)
        {
            if(g_pharmLineCount[i] > 0 && g_pharmStatus[i] == 2)
            {
                Write("Alerting a lazy pharmacist",
                    sizeof("Alerting a lazy pharmacist"), 1);

                g_pharmStatus[i] = 1;
                Signal(g_pharmBreakCV[i], g_pharmBreakLock[i]);
                break;
            }
        } 
        Release(g_pharmStatusLock);


        Acquire(g_cashStatusLock);
        for(i = 0; i < NUM_USED; i++)
        {
            if(g_cashLineCount[i] > 0 && g_cashStatus[i] == 2)
            {
                Write("Alerting a lazy cashier",
                    sizeof("Alerting a lazy cashier"), 1);

                g_cashStatus[i] = 1;
                Signal(g_cashBreakCV[i], g_cashBreakLock[i]);
                break;
            }
        }
        Release(g_cashStatusLock);

        Acquire(g_recepStatusLock);
        for(i = 0; i < NUM_USED; i++)
        {
            if(g_recepLineCount[i] > 0 && g_recepStatus[i] == 2)
            {
                Write("Alerting a lazy receptionist",
                    sizeof("Alerting a lazy receptionist"), 1);

                g_recepStatus[i] = 1;
                Signal(g_recepBreakCV[i],g_recepBreakLock[i]);
                break;
            }
        }
        Release(g_recepStatusLock);
        
      /*  Acquire(g_doorboyStateLock);
        for(i = 0; i < NUM_USED; i++)
        {

            if(g_DoorboyState[i] == 2 && g_doorboyDoctorLine[i] > 0)
            {
                Write("Alerting a lazy doorboy",
                    sizeof("Alerting a lazy doorboy"), 1);

                g_DoorboyState[i] = 1;
                Signal(g_dbBreakCV[i], g_dbBreakLock[i]);
                break;
            }
        }
        Release(g_doorboyStateLock);

        /* IMPLEMENT PAYMENT RECORDS CALCULATIONS ONCE PAYMENT CODE IS IMPLEMENTED PROPERLY */

        yieldTimes = i * 250 + 1000;
        for(i = 0; i < yieldTimes; i++)
        {
            Yield();
        }
    }
}




void patient()
{
    /*patient receptionist interaction*/
    int p_myRoom;
    int index;
    int p_shortestLine;
    int p_lineIndex;
    int p_myToken;
    int i;
    g_Medicine p_medicine;
    Acquire(g_nextPatientLock);
    index = g_nextPatientIndex++;
    Release(g_nextPatientLock);

    Acquire(g_recepLineLock);
    p_shortestLine = g_recepLineCount[0];
    p_lineIndex = 0;
    MyPrint("Patient has arrived", sizeof("Patient has arrived"), GenerateSingle(index +1  ,0), 0);
    for(i=0; i<NUM_USED; i++)
    {
        if(g_recepLineCount[i] < p_shortestLine)
        {
            p_lineIndex = i;
            p_shortestLine = g_recepLineCount[i];
        }
        Acquire(g_recepStatusLock);
        if(g_recepStatus[i] == 0)
        {
            g_recepStatus[i] = 1;
            p_lineIndex = i;
            p_shortestLine = -1;
            Release(g_recepStatusLock);
            break;
        }
        Release(g_recepStatusLock);
    }

    if(p_shortestLine > -1)
    {
        g_recepLineCount[p_lineIndex]++;
        MyPrint("Patient waiting on Receptionist", sizeof("Patient waiting on Receptionist"), GenerateSingle(index +1  ,0), 0);
        Wait(g_recepLineCV[p_lineIndex], g_recepLineLock);
        g_recepLineCount[p_lineIndex]--;
    }

    Acquire(g_recepStatusLock);
    g_recepStatus[p_lineIndex] = 1;
    Release(g_recepStatusLock);

    Acquire(g_recepLock[p_lineIndex]);
    Release(g_recepLineLock);

    MyPrint("Patient Signaling the receptionist", sizeof("Patient Signaling the receptionist"), GenerateSingle(index +1  ,0), 0);
    Signal(g_recepPatientCV[p_lineIndex],g_recepLock[p_lineIndex]);
    Wait(g_recepPatientCV[p_lineIndex],g_recepLock[p_lineIndex]);

    p_myToken = g_recepToken[p_lineIndex];
    MyPrint("Patient has recived Token", sizeof("Patient has recived Token"), GenerateSingle(index +1  +1 ,0), 0);
    Signal(g_recepPatientCV[p_lineIndex],g_recepLock[p_lineIndex]);
    Release(g_recepLock[p_lineIndex]);

    /*patient door boy interaction*/
    Acquire(g_doorboyLineLock);
    MyPrint("Patient Acquired door boy line lock", sizeof("Patient Acquired door boy line lock"), GenerateSingle(index +1  ,0), 0);
    p_shortestLine = g_doorboyLine[0];
    p_lineIndex = 0;
    for(i=0; i<NUM_USED; i++)
    {
        
        if(g_doorboyLine[i] < p_shortestLine)
        {
            p_lineIndex = i;
            p_shortestLine = g_doorboyLine[i];
        }
        Acquire(g_doorboyStateLock[i]);
        if(g_DoorboyState[i] == 3)
        {
            g_DoorboyState[i] = 1;
            p_lineIndex = i;
            p_shortestLine = -1;
            Release(g_doorboyStateLock[i]);
            break;
        }
        Release(g_doorboyStateLock[i]);
    }

    if(p_shortestLine > -1)
    {
        g_doorboyLine[p_lineIndex]++;
        MyPrint("Patient is waiting for door boy", sizeof("Patient is waiting for door boy"), GenerateSingle(index +1   ,0), 0);
        Wait(g_doorboyWaitLineCV[p_lineIndex],g_doorboyLineLock);
        g_doorboyLine[p_lineIndex]--;
        Acquire(g_doorboyLock[p_lineIndex]);
        Release(g_doorboyLineLock);
    }
    else
    {
        Acquire(g_doorboyWaitingForPatientLock[p_lineIndex]);
        Release(g_doorboyLineLock);

        MyPrint("Patient signaled by a doorboy", sizeof("Patient signaled by a door boy"), GenerateSingle(index +1  ,0), 0);
        Signal(g_doorboyWaitingForPatientCV[p_lineIndex],g_doorboyWaitingForPatientLock[p_lineIndex]);
        Acquire(g_doorboyLock[p_lineIndex]);
        Release(g_doorboyWaitingForPatientLock[p_lineIndex]);

        Wait(g_doorboyPatientCV[p_lineIndex],g_doorboyLock[p_lineIndex]);
    }

    
    
    Signal(g_doorboyPatientCV[p_lineIndex],g_doorboyLock[p_lineIndex]);
    Wait(g_doorboyPatientCV[p_lineIndex], g_doorboyLock[p_lineIndex]);
    
    Acquire(g_doctorRoomLock[p_lineIndex]);
    p_myRoom = g_roomNumber[p_lineIndex];
    MyPrint("Patient has been told by doorboy", sizeof("Patient has been told by doorboy"), GenerateSingle(index +1  ,0), 0);
    g_roomNumber[p_lineIndex] = -1;
    Release(g_doctorRoomLock[p_lineIndex]);

    Signal(g_doorboyPatientCV[p_lineIndex],g_doorboyLock[p_lineIndex]);
    MyPrint("Patient is going to Examining Room", sizeof("Patient is going to Examining Room"), GenerateSingle(index +1  ,0), 0);
    Release(g_doorboyLock[p_lineIndex]);

    /*interacting with doctor*/
    Acquire(g_pDoctorLock[p_myRoom]);
    Signal(g_pDocCV[p_myRoom], g_pDoctorLock[p_myRoom]);
    g_currentPatient[p_myRoom] = p_myToken;
    MyPrint("Patient is waiting to be examined by the Doctor in ExaminingRoom",
        sizeof("Patient is waiting to be examined by the Doctor in ExaminingRoom"), GenerateSingle(index +1 ,0), 0);
    Wait(g_pDocCV[p_myRoom], g_pDoctorLock[p_myRoom]);
    p_medicine = g_medicine[p_myRoom];

    g_medicine[p_myRoom] = None;
    Signal(g_pDocCV[p_myRoom], g_pDoctorLock[p_myRoom]);
    MyPrint("Patient Thank you for the prescription", sizeof("Patient Thank you for the prescription"), GenerateSingle(index +1 ,0), 0);
    Wait(g_pDocCV[p_myRoom],g_pDoctorLock[p_myRoom]);
    Signal(g_pDocCV[p_myRoom], g_pDoctorLock[p_myRoom]);
    MyPrint("Patient Leaving doctor", sizeof("Patient Leaving doctor"), GenerateSingle(index +1 ,0), 0);
    Release(g_pDoctorLock[p_myRoom]);

    /*interacting with cashier*/
    Acquire(g_cashierLineLock);
    p_shortestLine = g_cashLineCount[0];
    p_lineIndex = 0;
    for(i=0; i<NUM_USED; i++)
    {
        if(g_cashLineCount[i] < p_shortestLine)
        {
            p_lineIndex = i;
            p_shortestLine = g_cashLineCount[i];
        }
        Acquire(g_cashStatusLock);
        if(g_cashStatus[i] == 0)
        {
            g_cashStatus[i] = 1;
            p_lineIndex = i;
            p_shortestLine = -1;
            Release(g_cashStatusLock);
            break;
        }
        Release(g_cashStatusLock);
    }

    if(p_shortestLine > -1)
    {
        g_cashLineCount[p_lineIndex]++;
        MyPrint("Patient waiting on Cashier", sizeof("Patient waiting on Cashier"), GenerateSingle(index +1 ,0), 0);
        Wait(g_cashLineCV[p_lineIndex], g_cashierLineLock);
        g_cashLineCount[p_lineIndex]--;
    }
    Acquire(g_cashStatusLock);
    g_cashStatus[p_lineIndex] = 1;
    Release(g_cashStatusLock);

    Acquire(g_cashLock[p_lineIndex]);
    Release(g_cashierLineLock);

    Signal(g_cashPatientCV[p_lineIndex],g_cashLock[p_lineIndex]);
    Wait(g_cashPatientCV[p_lineIndex],g_cashLock[p_lineIndex]);
    MyPrint("Patient Signal the cashier to pay the bill", sizeof("Patient Signal the cashier to pay the bill"), GenerateSingle(index +1 ,0), 0);
    Signal(g_cashPatientCV[p_lineIndex],g_cashLock[p_lineIndex]);
    MyPrint("Patient Leaving the Cashier", sizeof("Patient Leaving the Cashier"), GenerateSingle(index +1 ,0), 0);
    Release(g_cashLock[p_lineIndex]);


    /*interacting with pharmacy clerk*/
    Acquire(g_pharmLineLock);
    p_shortestLine = g_pharmLineCount[0];
    p_lineIndex = 0;
    for(i=0; i<NUM_USED; i++)
    {
        if(g_pharmLineCount[i] < p_shortestLine)
        {
            p_lineIndex = i;
            p_shortestLine = g_pharmLineCount[i];
        }

        Acquire(g_pharmStatusLock);
        if(g_pharmStatus[i] == 0)
        {
            g_pharmStatus[i] = 1;
            p_lineIndex = i;
            p_shortestLine = -1;
            Release(g_pharmStatusLock);
            break;
        }
        Release(g_pharmStatusLock);
    }

    if(p_shortestLine > -1)
    {
        g_pharmLineCount[p_lineIndex]++;
        MyPrint("Patient waiting on Pharmacy", sizeof("Patient waiting on Pharmacy"), GenerateSingle(index +1 ,0), 0);
        Wait(g_pharmLineCV[p_lineIndex], g_pharmLineLock);
        g_pharmLineCount[p_lineIndex]--;
    }

    Acquire(g_pharmStatusLock);
    g_pharmStatus[p_lineIndex] = 1;
    Release(g_pharmStatusLock);

    Acquire(g_pharmLock[p_lineIndex]);
    Release(g_pharmLineLock);

    Signal(g_pharmPatientCV[p_lineIndex],g_pharmLock[p_lineIndex]);
    Wait(g_pharmPatientCV[p_lineIndex],g_pharmLock[p_lineIndex]);
    MyPrint("Patient Signal the Pharmacy to pay the bill", sizeof("Signal the Pharmacy to pay the bill"), GenerateSingle(index +1 ,0), 0);
    Signal(g_pharmPatientCV[p_lineIndex],g_pharmLock[p_lineIndex]);

    MyPrint("Patient waiting for the bill", sizeof("Patient waiting for the bill"), GenerateSingle(index +1 ,0), 0);
    Wait(g_pharmPatientCV[p_lineIndex],g_pharmLock[p_lineIndex]);
    MyPrint("Patient got the bill", sizeof("Patient got the bill"), GenerateSingle(index +1 ,0), 0);

    MyPrint("Patient paying the bill", sizeof("Patient paying the bill"), GenerateSingle(index +1 ,0), 0);
    Signal(g_pharmPatientCV[p_lineIndex],g_pharmLock[p_lineIndex]);


    Wait(g_pharmPatientCV[p_lineIndex],g_pharmLock[p_lineIndex]);
    Signal(g_pharmPatientCV[p_lineIndex],g_pharmLock[p_lineIndex]);
    MyPrint("Patient Leaving the Pharmacy", sizeof("Patient Leaving the Pharmacy"), GenerateSingle(index +1 ,0), 0);
    Release(g_pharmLock[p_lineIndex]);
    Exit(0);
    return;

}

void receptionist()
{
    int index;
    Acquire( g_nextReceptionistLock );
    index = g_nextReceptionistIndex++;
    Release( g_nextReceptionistLock );
    MyPrint("Receptionist has been created", sizeof("Receptionist has been created"), GenerateSingle(index +1 ,0), 0);
    
    while(1)
    {

        Acquire( g_recepLineLock ); 


        Acquire(g_recepStatusLock);

        g_recepStatus[index] = 0; 
        Release(g_recepStatusLock);

        if(g_recepLineCount[index] > 0)
        {
            Acquire(g_recepStatusLock);
            g_recepStatus[index] = 1; 
            Release(g_recepStatusLock);

            Signal(g_recepLineCV[index], g_recepLineLock);
            MyPrint("Receptionist has signalled a patient", 
                sizeof("Receptionist has signalled a patient"), GenerateSingle(index +1 ,0), 0);
        }
        else
        {
            Acquire(g_recepBreakLock[index]);

            Acquire(g_recepStatusLock);
            g_recepStatus[index] = 2; 
            Release(g_recepStatusLock);

            Release( g_recepLineLock ); 
            MyPrint("Receptionist going on break", sizeof("Receptionist going on break"), GenerateSingle(index +1 ,0), 0);
            Wait(g_recepBreakCV[index],g_recepBreakLock[index]);

            Acquire( g_recepLineLock );
            Release(g_recepBreakLock[index]);
            Signal(g_recepLineCV[index], g_recepLineLock);
        }

        Acquire( g_recepLock[index] ); 
        Release(  g_recepLineLock );   
        MyPrint("Receptionist Waiting for a patient", 
                sizeof("Receptionist Waiting for a patient"), GenerateSingle(index +1 ,0), 0);
        Wait(g_recepPatientCV[index], g_recepLock[index] ); 
       
        Acquire( g_receptokenLock ); 
        g_recepToken[index] = g_receptoken; 
        g_receptoken++;
     
        Release(  g_receptokenLock ); 

      
        Signal(  g_recepPatientCV[index],  g_recepLock[index] ); 
        Wait(g_recepPatientCV[index], g_recepLock[index] ); 
        Release( g_recepLock[index] ); 
        
    }

}

void doorboy()
{
    int index;
    int roomNumber;
    Acquire(g_nextDoorBoyLock);
    index = g_nextDoorBoyIndex++;
    Release(g_nextDoorBoyLock);

    while(1)
    {
        Acquire(g_doorboyDoctorLineLock);

        Acquire(g_doorboyStateLock[index]);
        g_DoorboyState[index] = 0;
        Release(g_doorboyStateLock[index]);

        if(g_doorboyDoctorLine[index] > 0)
        {
            Acquire(g_doorboyStateLock[index]);
            g_DoorboyState[index] = 1;
            Release(g_doorboyStateLock[index]);

            Signal(g_doorboyDoctorWaitLineCV[index], g_doorboyDoctorLineLock); 
        }
        else
        {
            /*Acquire(g_dbBreakLock[index]);
            Release(g_doorboyDoctorLineLock);

            Acquire(g_doorboyStateLock);
            g_DoorboyState[index] = 2;
            Release(g_doorboyStateLock);

            MyPrint("Doorboy is waiting for a doctor.",
                sizeof("Doorboy is waiting for a doctor."), GenerateSingle(index +1 ,0), 0);
            Wait(g_dbBreakCV[index], g_dbBreakLock[index]);
            MyPrint("Doorboy is coming off break.",
                sizeof("Doorboy is coming off break."), GenerateSingle(index +1 ,0), 0);
            Acquire(g_doorboyDoctorLineLock);
            Signal(g_doorboyDoctorWaitLineCV[index], g_doorboyDoctorLineLock);
            Release(g_dbBreakLock[index]);*/
            Acquire(g_doorboyWaitingForDoctorLock[index]);
            /*Release(g_doorboyDoctorLineLock);*/

            Acquire(g_doorboyStateLock[index]);
            g_DoorboyState[index] = 2;
            Release(g_doorboyStateLock[index]);

            MyPrint("Doorboy is going on break because no Doctor Calling",
                sizeof("Doorboy is going on break because no Doctor Calling"), GenerateSingle(index +1 ,0), 0);
            Wait(g_doorboyWaitingForDoctorCV[index], g_doorboyWaitingForDoctorLock[index]);

            Acquire(g_doorboyLock[index]);
            Release(g_doorboyWaitingForDoctorLock[index]);

            /*Signal(g_doorboyDoctorWaitLineCV[index],g_doorboyDoctorLock[index]);*/
        }

        Wait(g_doorboyDoctorWaitLineCV[index], g_doorboyDoctorLineLock);
        
        Acquire(g_doctorRoomLock[index]);
        roomNumber = g_doctorRoom[index];
        Release(g_doctorRoomLock[index]);
        
        MyPrint("Doorboy has been told to bring a Patient.",
                sizeof("Doorboy has been told to bring a Patient."), GenerateSingle(index +1 ,0), 0);
        Signal(g_doorboyDoctorWaitLineCV[index], g_doorboyDoctorLineLock);
        Release(g_doorboyDoctorLineLock);

        /*patient section*/
        Acquire(g_doorboyLineLock);
        if(g_doorboyLine[index] > 0)
        {
            MyPrint("Doorboy signalled the Patient",
                sizeof("Doorboy signalled the Patient"), GenerateSingle(index +1 ,0), 0);
            Signal(g_doorboyWaitLineCV[index], g_doorboyLineLock);

            Acquire(g_doorboyLock[index]);
            Release(g_doorboyLineLock);
        }
        else
        {
            Acquire(g_doorboyWaitingForPatientLock[index]);
            Release(g_doorboyLineLock);

            Acquire(g_doorboyStateLock[index]);
            g_DoorboyState[index] = 3;
            Release(g_doorboyStateLock[index]);

            MyPrint("Doorboy is going on break because no Patients",
                sizeof("Doorboy is going on break because no Patients"), GenerateSingle(index +1 ,0), 0);
            Wait(g_doorboyWaitingForPatientCV[index], g_doorboyWaitingForPatientLock[index]);

            Acquire(g_doorboyLock[index]);
            Release(g_doorboyWaitingForPatientLock[index]);

            Signal(g_doorboyPatientCV[index],g_doorboyLock[index]);

        }

        Wait(g_doorboyPatientCV[index], g_doorboyLock[index]);
        
        Acquire(g_doctorRoomLock[index]);
        g_roomNumber[index] = roomNumber;

        Release(g_doctorRoomLock[index]);
        
        MyPrint("Doorboy has told Patient to go to examination room",
                sizeof("Doorboy has told Patient to go to examination room"), GenerateSingle(index +1 ,0), 0);
        Signal(g_doorboyPatientCV[index], g_doorboyLock[index]);
        Wait(g_doorboyPatientCV[index], g_doorboyLock[index]);

        Release(g_doorboyLock[index]);
    }
}

void pharmacyClerk()
{
    int index;
    Acquire(g_nextPharacistLock);
    index = g_nextPharmacistIndex++;
    Release(g_nextPharacistLock);

    while(1)
    {
        Acquire(g_pharmLineLock);

        Acquire(g_pharmStatusLock);
        g_pharmStatus[index] = 0;
        Release(g_pharmStatusLock);

        if(g_pharmLineCount[index] > 0)
        {
            Acquire(g_pharmStatusLock);
            g_pharmStatus[index] = 1;
            Release(g_pharmStatusLock);
            Signal(g_pharmLineCV[index], g_pharmLineLock);
        }
        else
        {
            MyPrint("Pharmacy clerk going on break", sizeof("Pharmacy clerk going on break"),GenerateSingle(index +1 ,0), 0);
            Acquire(g_pharmBreakLock[index]);
            Acquire(g_pharmStatusLock);
            g_pharmStatus[index] = 2; 
            Release(g_pharmStatusLock);
            Release( g_pharmLineLock ); 

            Wait(g_pharmBreakCV[index],g_pharmBreakLock[index]);

            Acquire( g_pharmLineLock );
            Release(g_pharmBreakLock[index]);
            Signal(g_pharmLineCV[index], g_pharmLineLock);

        }
        Acquire(g_pharmLock[index]);
        MyPrint("Pharmacy Signaling a patient to come", sizeof("Pharmacy Signaling a patient to come"), GenerateSingle(index +1 ,0), 0);
        Release(g_pharmLineLock);

        Wait(g_pharmPatientCV[index], g_pharmLock[index]);
        MyPrint("Pharmacy gets prescription from patient", sizeof("Pharmacy gets prescription from patient"), GenerateSingle(index +1 ,0), 0);
        Signal(g_pharmPatientCV[index], g_pharmLock[index]);
        MyPrint("Pharmacy give prescription to patient", sizeof("Pharmacy give prescription to patient"), GenerateSingle(index +1 ,0), 0);
        Wait(g_pharmPatientCV[index], g_pharmLock[index]);
        MyPrint("Pharmacy you owe us about three fifity", sizeof("Pharmacy you owe us about three fifity"), GenerateSingle(index +1 ,0), 0);
        Signal(g_pharmPatientCV[index],g_pharmLock[index]);
        Wait(g_pharmPatientCV[index], g_pharmLock[index]);
        MyPrint("Pharmacy got money from the patient", sizeof("Pharmacy got money from the patient"), GenerateSingle(index +1 ,0), 0);
        Acquire(g_pharmTokenLock);
        /*TODO: push back to payment record*/

        g_pharmPayments[index] = -1;
        Release(g_pharmTokenLock);

        Signal(g_pharmPatientCV[index], g_pharmLock[index]);
        Wait(g_pharmPatientCV[index], g_pharmLock[index]);
        Release(g_pharmLock[index]);
    }

}
void cashier()
{
    int index;
    Acquire(g_nextCashierLock);
    index = g_nextCashierIndex++;
    Release(g_nextCashierLock);
    while(1)
    {
        Acquire(g_cashierLineLock);
        MyPrint("Cashier acquired lock", sizeof("Cashier acquired lock"), GenerateSingle(index +1 ,0), 0);
        
        Acquire(g_cashStatusLock);
        g_cashStatus[index] = 0;
        Release(g_cashStatusLock);
        
        if(g_cashLineCount[index] > 0)
        {
            MyPrint("Cashier Alerting patient in line", sizeof("Cashier Alerting patient in line"), GenerateSingle(index +1 ,0), 0);
            
            Acquire(g_cashStatusLock);
            g_cashStatus[index] = 1;
            Release(g_cashStatusLock);

            Signal(g_cashLineCV[index],g_cashierLineLock);
        }
        else
        {
            MyPrint("Cashier going on break", sizeof("Cashier going on break"),GenerateSingle(index +1 ,0), 0);
            Acquire(g_cashBreakLock[index]);
            Acquire(g_cashStatusLock);
            g_cashStatus[index] = 2; 
            Release(g_cashStatusLock);
            Release( g_cashierLineLock ); 

            Wait(g_cashBreakCV[index],g_cashBreakLock[index]);

            Acquire( g_cashierLineLock );
            Release(g_cashBreakLock[index]);
            Signal(g_cashLineCV[index], g_cashierLineLock);

        }
        Acquire(g_cashLock[index]);
        Release(g_cashierLineLock);

        MyPrint("Cashier Waiting for patient to approach us", sizeof("Cashier Waiting for patient to approach us"), GenerateSingle(index +1 ,0), 0);
        Wait(g_cashPatientCV[index], g_cashLock[index]);
        MyPrint("Patient arrived", sizeof("Patient arrived"), GenerateSingle(index +1 ,0), 0);
        Acquire(g_cashTokenLock);
        g_cashPayment += 20;
        g_cashPayments[index] = g_cashPayment;
        MyPrint("Cashier Putting money on display", sizeof("Cashier Putting money on display"), GenerateSingle(index +1 ,0), 0);
        Release(g_cashTokenLock);

        MyPrint("Cashier Telling customer about money", sizeof("Cashier Telling customer about money"), GenerateSingle(index +1 ,0), 0);
        Signal(g_cashPatientCV[index], g_cashLock[index]);
        Wait(g_cashPatientCV[index], g_cashLock[index]);
        MyPrint("Cashier Customer paid the bill", sizeof("Cashier Customer paid the bill"), GenerateSingle(index +1 ,0), 0);
        Acquire(g_cashTokenLock);

        /* TODO: record our payment vector push back*/
        g_cashPayments[index] = -1;
        MyPrint("Cashier Depositing the money", sizeof("DCashier espositing the money"), GenerateSingle(index +1 ,0), 0);
        Release(g_cashTokenLock);

        MyPrint("Cashier finished", sizeof("Cashier finished"), GenerateSingle(index +1 ,0), 0);
        Release(g_cashLock[index]);
    }
}


void doctor()
{
    int index;
    int i;
    int doorboyIndex;
    int shortestLine;
    int randomNum;
    int fee;
    g_diagnosis diagnosis;

    Acquire( g_nextDoctorLock );
    index = g_nextDoctorIndex++;
    Release( g_nextDoctorLock );

    while(1)
    {
        /*doctor doorboy interaction*/
        Acquire(g_doorboyDoctorLineLock);
        g_DoctorState[index] = 0;
        doorboyIndex = 0;
        shortestLine = g_doorboyDoctorLine[0];
        for(i = 0; i < NUM_USED; i++)
        {
            if(g_doorboyDoctorLine[i] < shortestLine)
            {
                shortestLine = g_doorboyDoctorLine[i];
                doorboyIndex = i;
            }
            Acquire(g_doorboyStateLock[i]);
            if(g_DoorboyState[i] == 2)
            {
                
                doorboyIndex = i;
                g_DoorboyState[i] = 1;
                shortestLine = -1;
                Release(g_doorboyStateLock[i]);
                break;
            }
            Release(g_doorboyStateLock[i]);
        }

        g_DoctorState[index] = 1;
        if(shortestLine > -1)
        {
            g_doorboyDoctorLine[doorboyIndex]++;
            Wait(g_doorboyDoctorWaitLineCV[doorboyIndex], g_doorboyDoctorLineLock);
            g_doorboyDoctorLine[doorboyIndex]--;
            Acquire(g_doorboyDoctorLock[doorboyIndex]);
        }
        else
        {
            Acquire(g_doorboyWaitingForDoctorLock[doorboyIndex]);
            Signal(g_doorboyWaitingForDoctorCV[doorboyIndex], g_doorboyWaitingForDoctorLock[doorboyIndex]);
            Acquire(g_doorboyDoctorLock[doorboyIndex]);
            Release(g_doorboyWaitingForDoctorLock[doorboyIndex]);
        }

        MyPrint("Doctor has told a Doorboy to bring a Patient to Examining Room", 
            sizeof("Doctor has told a Doorboy to bring a Patient to Examining Room"), GenerateSingle(index +1 ,0), 0);
        
        Acquire(g_doctorRoomLock[doorboyIndex]);
        g_doctorRoom[doorboyIndex] = index;
        Release(g_doctorRoomLock[doorboyIndex]);

        Signal(g_doorboyDoctorWaitLineCV[doorboyIndex], g_doorboyDoctorLineLock);
         MyPrint("Doctor waiting for doorboy's response", 
            sizeof("Doctor waiting for doorboy's response"), GenerateSingle(index +1 ,0), 0);
        Wait(g_doorboyDoctorWaitLineCV[doorboyIndex], g_doorboyDoctorLineLock);
        Release(g_doorboyDoctorLock[doorboyIndex]);

        /*doctor patient interaction*/
        Acquire(g_pDoctorLock[index]);
        Release(g_doorboyDoctorLineLock);
        
        Wait(g_pDocCV[index], g_pDoctorLock[index]);

        randomNum = MAX + 10;
        for(i = 0; i < randomNum; i++)
        {
            Yield();
        }
        MyPrint("Doctor is examining a Patient", sizeof("Doctor is examining a Patient"), GenerateSingle(index +1 ,0), 0);
        randomNum = MAX-2;
        diagnosis = NONE;
        fee = 25;
        switch(randomNum)
        {
            case 0:
                MyPrint("Doctor has determined Patient is not sick.", 
                    sizeof("Doctor has determined Patient is not sick."), GenerateSingle(index +1 ,0), 0);
                break;
            case 1:
                diagnosis = COLD;
                g_medicine[index] = Ibuprofen;
                fee += MAX;
                MyPrint("Doctor has determined that the patient has a cold.",
                    sizeof("Doctor has determined that the patient has a cold."), GenerateSingle(index +1 ,0), 0);
                break;
            case 2:
                diagnosis = FOOD_PSNG;
                g_medicine[index] = Antibiotic;
                fee *= randomNum;
                MyPrint("Doctor has determined that the patient has food poisoning.",
                    sizeof("Doctor has determined that the patient has food poisoning."), GenerateSingle(index +1 ,0), 0);
                break;
            case 3:
                diagnosis = STREP;
                g_medicine[index] = Antibiotic;
                fee *- randomNum;
                MyPrint("Doctor has determined that the patient has strep throat.",
                    sizeof("Doctor has determined that the patient has strep throat."), GenerateSingle(index +1 ,0), 0);
                break;
            case 4:
                diagnosis = FLU;
                g_medicine[index] = Ibuprofen;
                fee *= randomNum;
                MyPrint("Doctor has determined that the patient has the flu.",
                    sizeof("Doctor has determined that the patient has the flu."), GenerateSingle(index +1 ,0), 0);
                break;
            default:
                break;
        }
        MyPrint("Doctor is handing prescription to patient.", 
            sizeof("Doctor is handing prescription to patient."), GenerateSingle(index +1 ,0), 0);
        
        Signal(g_pDocCV[index], g_pDoctorLock[index]);
        
        Wait(g_pDocCV[index], g_pDoctorLock[index]);
        
        Release(g_pDoctorLock[index]);

        Acquire(g_cashDoctorLock[index]);
        MyPrint("Doctor-cashier code has to be implemented", 
            sizeof("Doctor-cashier code has to be implemented"), GenerateSingle(index +1 ,0), 0);
        Release(g_cashDoctorLock[index]);

        /*re-enter doctor patient interaction*/
        Acquire(g_pDoctorLock[index]);
        
        Signal(g_pDocCV[index], g_pDoctorLock[index]);
        
        MyPrint("Doctor is signalling Patient to leave. Goodbye Patient.",
            sizeof("Doctor is signalling Patient to leave. Goodbye Patient."), GenerateSingle(index +1 ,0), 0);
        
        Wait(g_pDocCV[index], g_pDoctorLock[index]);
        
        g_currentPatient[index] = -1;

        randomNum = MAX;
        if(randomNum == 9)
        {
            g_DoctorState[index] = 2;
        }
        Release(g_pDoctorLock[index]);

        if(g_DoctorState[index] == 2)
        {
            randomNum = MAX + 10;
            for(i = 0; i < randomNum; i++)
            {
                Yield();
            }
        }
    }
}

int main()
{
    int i;
    char *buffer = "Next_cashier lock";
    /*next staff lock*/
    g_nextCashierLock = CreateLock(buffer, sizeof(buffer));

    buffer = "Next_patient lock";
    g_nextPatientLock = CreateLock(buffer, sizeof(buffer));

    buffer = "Next_pharmacist lock";
    g_nextPharacistLock = CreateLock(buffer, sizeof(buffer));

    buffer = "Next_receptionist lock";
    g_nextReceptionistLock = CreateLock(buffer, sizeof(buffer));

    buffer = "Next_doctor lock";
    g_nextDoctorLock = CreateLock(buffer, sizeof(buffer));

    buffer = "Next_doorboy lock";
    g_nextDoorBoyLock = CreateLock(buffer, sizeof(buffer));

    /*Receptionist variables*/
    buffer = "6Receptionist Line Lock";
    g_recepLineLock = CreateLock(buffer, sizeof(buffer));

    buffer = "7Receptionist Token Lock";
    g_receptokenLock = CreateLock(buffer, sizeof(buffer));

    buffer="Receptionist Status lock";
    g_recepStatusLock = CreateLock(buffer, sizeof(buffer));

    /*Doorboy variables*/

    /*doctor line*/
    buffer = "1Doorboy wait for doctor lock";
    g_doorboyDoctorLineLock = CreateLock(buffer, sizeof(buffer));
    
    /*patient line*/
    buffer = "patient wait for door boy lock";
    g_doorboyLineLock = CreateLock(buffer, sizeof(buffer));


    
    
    /*doorboy waiting on patients
    buffer = "doorboy waiting for patient lock";
    g_doorboyWaitingForPatientLock = CreateLock(buffer, sizeof(buffer));*/

    
    
    /*Cashier Variables*/
    buffer = "Cashier line lock";
    g_cashierLineLock = CreateLock(buffer, sizeof(buffer));

    buffer = "Cashier token lock";
    g_cashTokenLock = CreateLock(buffer, sizeof(buffer));

    buffer = "Cahsier status lock";
    g_cashStatusLock = CreateLock(buffer, sizeof(buffer));
    /*Pharmacy clrk variable*/
    buffer = "pharmacist token lock";
    g_pharmTokenLock = CreateLock(buffer, sizeof(buffer));

    buffer = "pharmacist Line lock";
    g_pharmLineLock = CreateLock(buffer, sizeof(buffer));

    buffer = "pharmacist status lock";
    g_pharmStatusLock = CreateLock(buffer, sizeof(buffer));

    /*Doctor Variables*/
    




    for(i = 0; i < NUM_USED; i++)
    {
        /*Receptionist variables*/
        buffer = "1Receptionist Patient Lock";
        g_recepLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "2Receptionist Wait Lock";
        g_recepBreakLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "3Receptionist Wait CV";
        g_recepBreakCV[i] = CreateCondition(buffer, sizeof(buffer));

        buffer = "4Receptionist Line Condition variable";
        g_recepLineCV[i] = CreateCondition(buffer, sizeof(buffer));

        buffer = "5Receptionist Patient Condition variable";
        g_recepPatientCV[i] = CreateCondition(buffer, sizeof(buffer));

        /*Doorboy variables*/
        buffer = "3Doorboy doctor lock";
        g_doorboyLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "4Doorboy patient condition variable";
        g_doorboyPatientCV[i] = CreateCondition(buffer, sizeof(buffer));

        /*doctor line*/
        buffer = "5Door boy waiting for doctor condition variable";
        g_doorboyDoctorWaitLineCV[i] = CreateCondition(buffer, sizeof(buffer));

        /*patient line*/
        buffer = "doorboy wait in line CV";
        g_doorboyWaitLineCV[i] = CreateCondition(buffer, sizeof(buffer));

        /*doorboy break*/
        buffer = "6Door take break CV";
        g_dbBreakCV[i] = CreateCondition(buffer, sizeof(buffer));

        /*doorboy waiting on patients*/
        buffer = "7Doorboy waiting for patient CV";
        g_doorboyWaitingForPatientCV[i] = CreateCondition(buffer, sizeof(buffer));

        buffer = "7Doorboy waiting for patient Lock";
        g_doorboyWaitingForPatientLock[i] = CreateLock(buffer, sizeof(buffer));

        /*door boy waiting on doctor*/
        buffer = "Doorboy waiting for Doctor Lock";
        g_doorboyWaitingForDoctorLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "Doorboy waiting for Doctor CV";
        g_doorboyWaitingForDoctorCV[i] = CreateCondition(buffer, sizeof(buffer));

        buffer = "Doorboy Doctor Lock";
        g_doorboyDoctorLock[i] = CreateLock(buffer, sizeof(buffer));

        /*doorboy state*/
        buffer = "2Doorboy state lock";
        g_doorboyStateLock[i] = CreateLock(buffer, sizeof(buffer));
        
        /*doorboy break*/
        buffer = "8Doorboy take break lock";
        g_dbBreakLock[i] = CreateLock(buffer, sizeof(buffer));

        /*Cashier Variables*/
        buffer = "Cashier patient lock";
        g_cashLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "Cashier line CV";
        g_cashLineCV[i] = CreateCondition(buffer, sizeof(buffer));

        buffer = "Cashier patient CV";
        g_cashPatientCV[i] = CreateCondition(buffer, sizeof(buffer));

        buffer = "Cashier Break Lock";
        g_cashBreakLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "Cashier Break CV";
        g_cashBreakCV[i] = CreateCondition(buffer, sizeof(buffer));

        /*Pharmacy clrk variable*/
        buffer = "Create pharmacist lock";
        g_pharmLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "Pharmacist line CV";
        g_pharmLineCV[i] = CreateCondition(buffer, sizeof(buffer));

        buffer = "Pharmcist Patient CV";
        g_pharmPatientCV[i] = CreateCondition(buffer, sizeof(buffer));

        buffer = "Pharmcist Break Lock";
        g_pharmBreakLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "Pharmcist Break CV";
        g_pharmBreakCV[i] = CreateCondition(buffer, sizeof(buffer));

        /*Doctor Variables*/
        buffer = "Patient Doctor CV";
        g_pDocCV[i] = CreateCondition(buffer, sizeof(buffer));

        buffer = "Cashier Doctor Lock";
        g_cashDoctorLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "Doctor patient lock";
        g_pDoctorLock[i] = CreateLock(buffer, sizeof(buffer));

        buffer = "Patient enter each doctor room lock";
        g_doctorRoomLock[i] = CreateLock(buffer, sizeof(buffer));

    }
    
    for(i = 0; i<NUM_USED; i++)
    {
        Fork((void*)doctor);
        
        Fork((void*)hospitalManager);
        Fork((void*)patient);
        Fork((void*)cashier);
        Fork((void*)pharmacyClerk);
        Fork((void*)receptionist);
        Fork((void*)doorboy); 
    }
    return 0; 
}
