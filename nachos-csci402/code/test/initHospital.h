#ifndef INITHOSPITAL_H
#define INITHOSPITAL_H

#include "copyright.h"

#define MAX 5



int NUM_USED = 2;


 int g_nextCashierIndex = 0;

 int g_nextPatientIndex = 0;
 int g_nextPharmacistIndex = 0;
 int g_nextReceptionistIndex = 0;
 int g_nextDoctorIndex = 0 ;
 int g_nextDoorBoyIndex = 0;


 /* RECEPTIONIST Variables */
 int g_recepLock[MAX] = {-1,-1,-1,-1,-1};
 int g_recepLineLock = -1;
 int g_recepLineCount;
 int g_recepLineCV[MAX] = {-1,-1,-1,-1,-1};
 int g_recepPatientCV[MAX] = {-1,-1,-1,-1,-1};
 int g_recepStatus;
 int g_recepBreakLock[MAX] = {-1,-1,-1,-1,-1};
 int g_recepBreakCV[MAX] = {-1,-1,-1,-1,-1}; 
 int g_recepToken;
 int g_receptokenLock = -1;
 int g_receptoken = 0;
 int g_recepStatusLock = -1;


 /* DOORBOY Variables */
 int g_doorboyLock[MAX] = {-1, -1, -1, -1, -1};
 int g_doorboyPatientCV[MAX] = {-1, -1, -1, -1, -1};
 int g_doorboyWaitingForDoctorLock[MAX] = {-1,-1,-1,-1,-1};
 int g_doorboyWaitingForDoctorCV[MAX] = {-1,-1,-1,-1,-1};
 int g_roomNumber;

 /* doctor line */
 int g_doorboyDoctorLine;
 int g_doorboyDoctorLineLock = -1;
 int g_doorboyDoctorWaitLineCV[MAX] = {-1, -1, -1, -1, -1};
 int g_doorboyDoctorLock[MAX] = {-1,-1,-1,-1,-1};

 /* patient line */
 int g_doorboyLineLock = -1;
 int g_doorboyWaitLineCV[MAX] = {-1, -1, -1, -1, -1};
 int g_doctorRoomLock[MAX] = {-1,-1,-1,-1,-1};
 int g_doorboyLine;
 int g_doctorRoom;

 /* doorboy break */
 int g_dbBreakLock[MAX] = {-1,-1,-1,-1,-1};
 int g_dbBreakCV[MAX] = {-1, -1, -1, -1, -1};

 /* doorboy waiting on patients */
 int g_doorboyWaitingForPatientLock[MAX] = {-1,-1,-1,-1,-1};
 int g_doorboyWaitingForPatientCV[MAX] = {-1, -1, -1, -1, -1};

 /* doorboy state */
 int g_doorboyStateLock[MAX] = {-1,-1,-1,-1,-1};
 int g_DoorboyState;

 /* CASHIER Variables */
 int g_cashLock[MAX] = {-1,-1,-1,-1,-1};
 int g_cashierLineLock = -1;
 int g_cashLineCount[MAX] = {0,0,0,0,0};
 int g_cashLineCV[MAX] = {-1,-1,-1,-1,-1};
 int g_cashPatientCV[MAX] = {-1,-1,-1,-1,-1};
 int g_cashStatus[MAX] = {1,1,1,1,1};
 int g_cashPayment = 0;
 int g_cashPayments[MAX] = {0,0,0,0,0};
 int g_cashTokenLock = -1;
 int g_cashBreakLock[MAX] = {-1,-1,-1,-1,-1};
 int g_cashBreakCV[MAX] = {-1,-1,-1,-1,-1};
 int g_cashStatusLock = -1;
 /*vector for cash payment records*/

 /* PHARMACY CLERK variable*/
 int g_pharmLock[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmLineLock = -1;
 int g_pharmLineCount[MAX] = {0,0,0,0,0};
 int g_pharmLineCV[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmPatientCV[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmStatus[MAX] = {1,1,1,1,1};
 int g_pharmPayment = 0;
 int g_pharmPayments[MAX] = {0,0,0,0,0};
 int g_pharmTokenLock = -1;
 int g_pharmBreakLock[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmBreakCV[MAX] = {-1,-1,-1,-1,-1};
 int g_pharmStatusLock = -1;
 /*vector for pharm payment records*/

 /* DOCTOR Variables */
 int g_pDoctorLock[MAX] = {-1,-1,-1,-1,-1};
 int g_pDocCV[MAX] = {-1, -1, -1, -1, -1};
 int g_cashDoctorLock[MAX] = {-1, -1, -1, -1, -1}; 
 int g_DoctorState;
 int g_currentPatient;
 int g_medicine;
 typedef enum {NONE, FLU, COLD, FOOD_PSNG, STREP} g_diagnosis;
 typedef enum {None, Ibuprofen, Antibiotic} g_Medicine;
 /*g_Medicine g_medicine[MAX] = {None, None, None, None, None}; */



 int g_nextCashierLock = -1;
 int g_nextPatientLock = -1;
 int g_nextPharacistLock = -1;
 int g_nextReceptionistLock = -1;
 int g_nextDoctorLock = -1;
 int g_nextDoorBoyLock = -1;


void doCreate()
{
	int i;

    
    g_nextReceptionistIndex = CreateMV("RecepIndex", sizeof("RecepIndex"),NUM_USED, 0);
   
    g_nextPatientIndex = CreateMV("PatientIndex", sizeof("PatientIndex"),NUM_USED, 0);

    g_nextDoctorIndex = CreateMV("DoctorIndex", sizeof("DoctorIndex"),NUM_USED, 0);

    g_nextDoorBoyIndex = CreateMV("DoorboyIndex", sizeof("DoorboyIndex"),NUM_USED, 0);

    g_recepStatus = CreateMV("RecepStatus", sizeof("RecepStatus"),NUM_USED, 1);
    g_recepLineCount = CreateMV("RecepLine", sizeof("RecepLine"),NUM_USED, 0);

    g_receptoken = CreateMV("RecepToken", sizeof("RecepToken"),1, 1);
    g_recepToken = CreateMV("RecepTokenArray", sizeof("RecepTokenArray"),NUM_USED, 0);


    /* DOORBOY Variables 
    int g_roomNumber[MAX] = {-1,-1,-1,-1,-1};*/
    g_roomNumber = CreateMV("RoomNumber", sizeof("RoomNumber"),NUM_USED, -1);


    /* doctor line 
    int g_doorboyDoctorLine[MAX] = {0, 0, 0, 0, 0};*/
    
    g_doorboyDoctorLine = CreateMV("DoorboyDoctorLine", sizeof("DoorboyDoctorLine"),NUM_USED, 0);

    /* patient line 
    int g_doorboyLine[MAX] = {0, 0, 0, 0, 0};
    int g_doctorRoom[MAX] = {-1, -1, -1, -1, -1};*/
    g_doorboyLine = CreateMV("DoorboyLine", sizeof("DoorboyLine"),NUM_USED, 0);
    g_doctorRoom = CreateMV("DoctorRoom", sizeof("DoctorRoom"),NUM_USED, -1);
    
    /* doorboy state
    int g_DoorboyState[MAX] = {1, 1, 1, 1, 1};*/
    g_DoorboyState = CreateMV("DoorboyState", sizeof("DoorboyState"),NUM_USED, 1);
    
    /*Doctor variables
    int g_DoctorState[MAX] = {1, 1, 1, 1, 1};
    int g_currentPatient[MAX] = {1, 1, 1, 1, 1};*/
    
    g_DoctorState = CreateMV("DoctorState", sizeof("DoctorState"),NUM_USED, 1);
    g_currentPatient = CreateMV("CurrentPatient", sizeof("CurrentPatient"),NUM_USED, 1);

    g_medicine = CreateMV("Medicine", sizeof("Medicine"),NUM_USED, 0);

    /*Lock and CV*/
    g_receptokenLock = CreateServerLock("ReceptionistTokenLock", sizeof("ReceptionistTokenLock"),0);
    g_nextReceptionistLock = CreateServerLock("RecepLineLock", sizeof("RecepLineLock"),0);
  
    g_recepStatusLock = CreateServerLock("ReceptionistStatuslock", sizeof("ReceptionistStatuslock"),0);
    g_recepLineLock = CreateServerLock("ReceptionistLineLock", sizeof("ReceptionistLineLock"),0);
   
    g_nextPatientLock = CreateServerLock("Nextpatientlock", sizeof("Nextpatientlock"),0);

    g_nextDoctorLock = CreateServerLock("Nextdoctorlock", sizeof("Nextdoctorlock"),0);

 
    g_nextDoorBoyLock = CreateServerLock("Nextdoorboylock", sizeof("Nextdoorboylock"),0);

    g_doorboyLineLock = CreateServerLock("patientwaitdoorboylock", sizeof("patientwaitdoorboylock"),-1);

    g_doorboyDoctorLineLock = CreateServerLock("1Doorboywaitdoctorlock", sizeof("1Doorboywaitdoctorlock"),-1);
/*
    g_nextCashierLock = CreateServerLock("Next_cashierlock", sizeof("Next_cashierlock"),-1);

   
    

    g_nextPharacistLock = CreateServerLock("Next_pharmacistlock", sizeof("Next_pharmacistlock"),-1);

    

   

   

   

  
    
*/
   
    
/*
    
    */

/*
    WHY DOES THIS JUST BREAK IT WALL
    
*/

    /*
    
   
    
    
    g_cashierLineLock = CreateServerLock("Cashierlinelock", sizeof("Cashierlinelock"),-1);


    g_cashTokenLock = CreateServerLock("Cashiertokenlock", sizeof("Cashiertokenlock"),-1);


    g_cashStatusLock = CreateServerLock("Cahsierstatuslock", sizeof("Cahsierstatuslock"),-1);
 

    g_pharmTokenLock = CreateServerLock("pharmacisttokenlock", sizeof("pharmacisttokenlock"),-1);

    g_pharmLineLock = CreateServerLock("pharmacistLinelock", sizeof("pharmacistLinelock"),-1);

    g_pharmStatusLock = CreateServerLock("pharmaciststatuslock", sizeof("pharmaciststatuslock"),-1);

   */
    


    for(i = 0; i < NUM_USED; i++)
    {
        g_recepLineCV[i] = CreateServerCV("ReceptionistLineCV", sizeof("ReceptionistLineCV"),i);
       


        g_recepBreakLock[i] = CreateServerLock("ReceptionistWaitLock", sizeof("ReceptionistWaitLock"),i);
        g_recepBreakCV[i] = CreateServerCV("ReceptionistWaitCV", sizeof("ReceptionistWaitCV"),i);
        g_recepPatientCV[i] = CreateServerCV("receptionistPatientCV", sizeof("receptionistPatientCV"),i);
     
        g_recepLock[i] = CreateServerLock("ReceptionistPatientLock", sizeof("ReceptionistPatientLock"),i);
   
       

        g_doorboyPatientCV[i] = CreateServerCV("DoorboypatientCV", sizeof("DoorboypatientCV"),i);

        /*doorboy doctor patient variables*/
        g_doorboyLock[i] = CreateServerLock("Doorboydoctorlock", sizeof("Doorboydoctorlock"),i);

        
        g_doorboyPatientCV[i] = CreateServerCV("DoorboypatientCV", sizeof("DoorboypatientCV"),i);

        
       
        g_doorboyDoctorWaitLineCV[i] = CreateServerCV("DoorboywaitfordoctorCV", sizeof("DoorboywaitfordoctorCV"),i);

     
        g_doorboyWaitLineCV[i] = CreateServerCV("doorboywaitinlineCV", sizeof("doorboywaitinlineCV"),i);

       
       
        g_dbBreakCV[i] = CreateServerCV("DoortakebreakCV", sizeof("DoortakebreakCV"),i);

       
       
        g_doorboyWaitingForPatientCV[i] = CreateServerCV("DoorboywaitforpatientCV", sizeof("DoorboywaitforpatientCV"),i);

      
        g_doorboyWaitingForPatientLock[i] = CreateServerLock("DoorboywaitforpatientLock", sizeof("DoorboywaitforpatientLock"),i);

      
       
        g_doorboyWaitingForDoctorLock[i] = CreateServerLock("DoorboywaitforDoctorLock", sizeof("DoorboywaitforDoctorLock"),i);

       
        g_doorboyWaitingForDoctorCV[i] = CreateServerCV("DoorboywaitforDoctorCV", sizeof("DoorboywaitforDoctorCV"),i);

      
        g_doorboyDoctorLock[i] = CreateServerLock("DoorboyDoctorLock", sizeof("DoorboyDoctorLock"),i);

        g_doorboyStateLock[i] = CreateServerLock("Doorboystatelock", sizeof("Doorboystatelock"),i);
        
       
      
        g_dbBreakLock[i] = CreateServerLock("Doorboytakebreaklock", sizeof("Doorboytakebreaklock"),i);

        g_pDoctorLock[i] = CreateServerLock("Doctorpatientlock", sizeof("Doctorpatientlock"),i);

       
        g_doctorRoomLock[i] = CreateServerLock("Patiententereachdoctor", sizeof("Patiententereachdoctor"),i);

        g_pDocCV[i] = CreateServerCV("PatientDoctorCV", sizeof("PatientDoctorCV"),i);

    }

/*

       

      
       
       
       
        
        
       
        g_cashLock[i] = CreateServerLock("Cashierpatientlock", sizeof("Cashierpatientlock"),i);

     
        g_cashLineCV[i] = CreateServerCV("CashierlineCV", sizeof("CashierlineCV"),i);

        
        g_cashPatientCV[i] = CreateServerCV("CashierpatientCV", sizeof("CashierpatientCV"),i);

        
        g_cashBreakLock[i] = CreateServerLock("CashierBreakLock", sizeof("CashierBreakLock"),i);

        g_cashBreakCV[i] = CreateServerCV("CashierBreakCV", sizeof("CashierBreakCV"),i);

        
     
        g_pharmLock[i] = CreateServerLock("Createpharmacistlock", sizeof("Createpharmacistlock"),i);

        
        g_pharmLineCV[i] = CreateServerCV("PharmacistlineCV", sizeof("PharmacistlineCV"),i);

        
        g_pharmPatientCV[i] = CreateServerCV("PharmcistPatientCV", sizeof("PharmcistPatientCV"),i);

        
        g_pharmBreakLock[i] = CreateServerLock("PharmcistBreakLock", sizeof("PharmcistBreakLock"),i);

   
        g_pharmBreakCV[i] = CreateServerCV("PharmcistBreakCV", sizeof("PharmcistBreakCV"),i);

       
        g_cashDoctorLock[i] = CreateServerLock("CashierDoctorLock", sizeof("CashierDoctorLock"),i);

       
        
	  */
   /* }*/
    
    return;
}

int GenerateSingle(int actioner, int reciever)
{
    int number;
    number = actioner * 100;
    number = number + reciever;
    return number;
}



#endif