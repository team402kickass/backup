#include "syscall.h"

int t5_l1;
int t5_l2;
int t5_c1;

/* --------------------------------------------------
// t5_t1() -- test 5 thread 1
//     This thread will wait on a condition under t5_l1
// -------------------------------------------------- */
void t5_t1() {
    Acquire(t5_l1);
    Write("t5_t1 Acquired, waiting on t5_c1",
    	sizeof("t5_t1 Acquired, waiting on t5_c1"), 1);
    Wait(t5_c1, t5_l1);
    Write("t5_t1 Releasing Lock t5_l1", sizeof("t5_t1 Releasing Lock t5_l1"), 1);
    Release(t5_l1);
    Exit(0);
}

/* --------------------------------------------------
// t5_t1() -- test 5 thread 1
//     This thread will wait on a t5_c1 condition under t5_l2, which is
//     a Fatal error
// -------------------------------------------------- */
void t5_t2() {
    Acquire(t5_l1);
    Acquire(t5_l2);
    Write("t5_t2 Acquired t5_l2, signalling t5_c1",
    	sizeof("t5_t2 Acquired t5_l2, signalling t5_c1"), 1);
    Signal(t5_c1, t5_l2);
    Write("t5_t2 Releasing Lock t5_l2", sizeof("t5_t2 Releasing Lock t5_l2"), 1);
    Release(t5_l2);
	Write("t5_t2 Releasing Lock t5_l1", sizeof("t5_t2 Releasing Lock t5_l1"), 1);
    Release(t5_l1);
    Exit(0);
}

int main()
{
	char* buffer = "t5_l1";
	t5_l1 = CreateLock(buffer, sizeof(buffer));

	buffer = "t5_l1";
	t5_l1 = CreateLock(buffer, sizeof(buffer));

	buffer = "t5_c1";
	t5_c1 = CreateCondition(buffer, sizeof(buffer));

	Fork((void*)t5_t1);
	Fork((void*)t5_t2);
}