
#include "syscall.h"
#include "initHospital.h"




int main()
{
    int mvtemp;
	/*patient receptionist interaction*/
    int p_myRoom;
    int index;
    int p_shortestLine;
    int p_lineIndex;
    int p_myToken;
    int i;
    int p_medicine;
    /*g_Medicine p_medicine;*/



    doCreate();
   
   
    ServerLockAcquire(g_nextPatientLock);
    index =  GetMV(g_nextPatientIndex,0);
    SetMV(g_nextPatientIndex, index +1, 0);
    ServerLockRelease(g_nextPatientLock);

   

    ServerLockAcquire(g_recepLineLock);
    p_shortestLine = GetMV(g_recepLineCount,0);
    p_lineIndex = 0;
    MyPrint("Patient has arrived", sizeof("Patient has arrived"), GenerateSingle(index +1  ,0), 0);


    for(i=0; i<NUM_USED; i++)
    {
        if(GetMV(g_recepLineCount,i) < p_shortestLine)
        {
            p_lineIndex = i;
            p_shortestLine = GetMV(g_recepLineCount,i);
        }
        
        
        ServerLockAcquire(g_recepStatusLock);
        if(GetMV(g_recepStatus,i) == 0)
        {
            SetMV(g_recepStatus,1,i);
            p_lineIndex = i;
            p_shortestLine = -1;
            ServerLockRelease(g_recepStatusLock);
            break;
        }
        ServerLockRelease(g_recepStatusLock);
    }



    if(p_shortestLine > -1)
    {

        mvtemp = GetMV(g_recepLineCount,p_lineIndex);
        SetMV(g_recepLineCount,mvtemp + 1, p_lineIndex );
        
        MyPrint("Patient waiting on Receptionist", sizeof("Patient waiting on Receptionist"), GenerateSingle(index +1  ,0), 0);
        ServerCVWait(g_recepLineCV[p_lineIndex], g_recepLineLock);

        mvtemp = GetMV(g_recepLineCount,p_lineIndex);
        SetMV(g_recepLineCount,mvtemp - 1, p_lineIndex );

        /*
        SetMV(g_recepLineCount,p_shortestLine + 1, p_lineIndex );
         MyPrint("Patient waiting on Receptionist", sizeof("Patient waiting on Receptionist"), GenerateSingle(index +1  ,0), 0);
        ServerCVWait(g_recepLineCV[p_lineIndex], g_recepLineLock);

        p_shortestLine = GetMV(g_recepLineCount,p_lineIndex);
        SetMV(g_recepLineCount,p_shortestLine - 1, p_lineIndex );*/
    }
    
    ServerLockAcquire(g_recepStatusLock);
    SetMV(g_recepStatus,1,p_lineIndex);
    ServerLockRelease(g_recepStatusLock);

    ServerLockAcquire(g_recepLock[p_lineIndex]);
    ServerLockRelease(g_recepLineLock);

    MyPrint("Patient Signaling the receptionist", sizeof("Patient Signaling the receptionist"), GenerateSingle(index +1  ,0), 0);
    ServerCVSignal(g_recepPatientCV[p_lineIndex],g_recepLock[p_lineIndex]);
    ServerCVWait(g_recepPatientCV[p_lineIndex],g_recepLock[p_lineIndex]);
    
    ServerLockAcquire( g_receptokenLock ); 
    p_myToken=  GetMV(g_recepToken,p_lineIndex);
    ServerLockRelease(  g_receptokenLock ); 
   
    MyPrint("Patient has recived Token", sizeof("Patient has recived Token"), GenerateSingle(index +1  +1 ,0), 0);
    ServerCVSignal(g_recepPatientCV[p_lineIndex],g_recepLock[p_lineIndex]);
    ServerLockRelease(g_recepLock[p_lineIndex]);

    /*patient door boy interaction*/
    ServerLockAcquire(g_doorboyLineLock);
    MyPrint("Patient Acquired door boy line lock", sizeof("Patient Acquired door boy line lock"), GenerateSingle(index +1  ,0), 0);
    p_shortestLine = GetMV(g_doorboyLine,0);
    p_lineIndex = 0;
    for(i=0; i<NUM_USED; i++)
    {
        
        if(GetMV(g_doorboyLine,i) < p_shortestLine)
        {
            p_lineIndex = i;
            p_shortestLine = GetMV(g_doorboyLine,i);
        }
        ServerLockAcquire(g_doorboyStateLock[i]);
        if(GetMV(g_DoorboyState,i) == 3)
        {
            SetMV(g_DoorboyState,1,i);
            p_lineIndex = i;
            p_shortestLine = -1;
            ServerLockRelease(g_doorboyStateLock[i]);
            break;
        }
        ServerLockRelease(g_doorboyStateLock[i]);
    }

    if(p_shortestLine > -1)
    {
        
        mvtemp = GetMV(g_doorboyLine,p_lineIndex);
        SetMV(g_doorboyLine, mvtemp + 1, p_lineIndex);
        
        MyPrint("Patient is waiting for door boy", sizeof("Patient is waiting for door boy"), GenerateSingle(index +1   ,0), 0);
        ServerCVWait(g_doorboyWaitLineCV[p_lineIndex],g_doorboyLineLock);
        
        mvtemp = GetMV(g_doorboyLine,p_lineIndex);
        SetMV(g_doorboyLine,mvtemp - 1,p_lineIndex);
        ServerLockAcquire(g_doorboyLock[p_lineIndex]);
        ServerLockRelease(g_doorboyLineLock);
    }
    else
    {
        ServerLockAcquire(g_doorboyWaitingForPatientLock[p_lineIndex]);
        ServerLockRelease(g_doorboyLineLock);

        MyPrint("Patient signaled by a doorboy", sizeof("Patient signaled by a door boy"), GenerateSingle(index +1  ,0), 0);
        ServerCVSignal(g_doorboyWaitingForPatientCV[p_lineIndex],g_doorboyWaitingForPatientLock[p_lineIndex]);
        ServerLockAcquire(g_doorboyLock[p_lineIndex]);
        ServerLockRelease(g_doorboyWaitingForPatientLock[p_lineIndex]);

        ServerCVWait(g_doorboyPatientCV[p_lineIndex],g_doorboyLock[p_lineIndex]);
    }

    
    
    ServerCVSignal(g_doorboyPatientCV[p_lineIndex],g_doorboyLock[p_lineIndex]);
    ServerCVWait(g_doorboyPatientCV[p_lineIndex], g_doorboyLock[p_lineIndex]);
    
    ServerLockAcquire(g_doctorRoomLock[p_lineIndex]);
    p_myRoom = GetMV(g_roomNumber,p_lineIndex);
    MyPrint("Patient has been told by doorboy", sizeof("Patient has been told by doorboy"), GenerateSingle(index +1  ,0), 0);
    SetMV(g_roomNumber,-1,p_lineIndex);
    ServerLockRelease(g_doctorRoomLock[p_lineIndex]);

    ServerCVSignal(g_doorboyPatientCV[p_lineIndex],g_doorboyLock[p_lineIndex]);
    MyPrint("Patient is going to Examining Room", sizeof("Patient is going to Examining Room"), GenerateSingle(index +1  ,0), 0);
    ServerLockRelease(g_doorboyLock[p_lineIndex]);

    /*interacting with doctor*/
    ServerLockAcquire(g_pDoctorLock[p_myRoom]);
    ServerCVSignal(g_pDocCV[p_myRoom], g_pDoctorLock[p_myRoom]);
    SetMV(g_currentPatient,p_myToken,p_myRoom);
    MyPrint("Patient is waiting to be examined by the Doctor in ExaminingRoom",
        sizeof("Patient is waiting to be examined by the Doctor in ExaminingRoom"), GenerateSingle(index +1 ,0), 0);
    ServerCVWait(g_pDocCV[p_myRoom], g_pDoctorLock[p_myRoom]);
    p_medicine = GetMV(g_medicine,p_myRoom);
    SetMV(g_medicine,0,p_myRoom);
    
    ServerCVSignal(g_pDocCV[p_myRoom], g_pDoctorLock[p_myRoom]);
    MyPrint("Patient Thank you for the prescription", sizeof("Patient Thank you for the prescription"), GenerateSingle(index +1 ,0), 0);
    ServerCVWait(g_pDocCV[p_myRoom],g_pDoctorLock[p_myRoom]);
    ServerCVSignal(g_pDocCV[p_myRoom], g_pDoctorLock[p_myRoom]);
    MyPrint("Patient Leaving doctor", sizeof("Patient Leaving doctor"), GenerateSingle(index +1 ,0), 0);
    ServerLockRelease(g_pDoctorLock[p_myRoom]);
}
