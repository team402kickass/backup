#include "syscall.h"
#include "initHospital.h"



int main()
{
	int index;
	doCreate();

    ServerLockAcquire( g_nextReceptionistLock );
    index =  GetMV(g_nextReceptionistIndex,0);
    SetMV(g_nextReceptionistIndex, index +1, 0);
   
    ServerLockRelease( g_nextReceptionistLock );

    MyPrint("Receptionist has been created", sizeof("Receptionist has been created"), GenerateSingle(index +1 ,0), 0);
    
    while(1)
    {

        ServerLockAcquire( g_recepLineLock ); 

        ServerLockAcquire(g_recepStatusLock);
        SetMV(g_recepStatus,0,index);
        ServerLockRelease(g_recepStatusLock);
      

        if(GetMV(g_recepLineCount,index) > 0)
        {
            ServerLockAcquire(g_recepStatusLock);
            SetMV(g_recepStatus,1,index);
            ServerLockRelease(g_recepStatusLock);

            ServerCVSignal(g_recepLineCV[index], g_recepLineLock);
           MyPrint("Receptionist has signalled a patient", 
                sizeof("Receptionist has signalled a patient"), GenerateSingle(index +1 ,0), 0);
         }
        else
        {
            
            ServerLockAcquire(g_recepBreakLock[index]);
            
            ServerLockAcquire(g_recepStatusLock);
            SetMV(g_recepStatus,2,index);
            ServerLockRelease(g_recepStatusLock);
            
            ServerLockRelease( g_recepLineLock ); 
            MyPrint("Receptionist going on break", sizeof("Receptionist going on break"), GenerateSingle(index +1 ,0), 0);
            ServerCVWait(g_recepBreakCV[index],g_recepBreakLock[index]);

            ServerLockAcquire( g_recepLineLock );
            ServerLockRelease(g_recepBreakLock[index]);
            ServerCVSignal(g_recepLineCV[index], g_recepLineLock);
        }

        ServerLockAcquire( g_recepLock[index] ); 
        ServerLockRelease(  g_recepLineLock );   
        MyPrint("Receptionist Waiting for a patient", 
                sizeof("Receptionist Waiting for a patient"), GenerateSingle(index +1 ,0), 0);
        ServerCVWait(g_recepPatientCV[index], g_recepLock[index] ); 
       
        ServerLockAcquire( g_receptokenLock ); 

        SetMV(g_recepToken, GetMV(g_receptoken,0) ,index);
        SetMV(g_receptoken, GetMV(g_receptoken,0) +1,0);
        
    
     
        ServerLockRelease(  g_receptokenLock ); 

      
        ServerCVSignal(  g_recepPatientCV[index],  g_recepLock[index] ); 
        ServerCVWait(g_recepPatientCV[index], g_recepLock[index] ); 
        ServerLockRelease( g_recepLock[index] ); 
        
    }
}
