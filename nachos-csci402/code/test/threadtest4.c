#include "syscall.h"

int t4_l1;
int t4_c1;

/* --------------------------------------------------
// t4_waiter()
//     These threads will wait on the t4_c1 condition variable.  All
//     t4_waiters will be released
// -------------------------------------------------- */
void t4_waiter() {
    Acquire(t4_l1);
    Write("t4_waiter Acquired t4_l1, waiting on t4_c1",
    	sizeof("t4_waiter Acquired t4_l1, waiting on t4_c1"), 1);
    Wait(t4_c1, t4_l1);
    Write("t4_waiter freed from t4_c1",
    	sizeof("t4_waiter freed from t4_c1"), 1);
    Release(t4_l1);
    Exit(0);
}


/* --------------------------------------------------
// t4_signaller()
//     This thread will broadcast to the t4_c1 condition variable.
//     All t4_waiters will be released
// -------------------------------------------------- */
void t4_signaller() {
    /* Don't broadcast until someone's waiting */
    int i;

    for (i = 0; i < 10000 ; i++ ) Yield();
    Acquire(t4_l1);
    Write("t4_signaller Acquired t4_l1, broadcasting t4_c1",
    	sizeof("t4_signaller Acquired t4_l1, broadcasting t4_c1"), 1);
    Broadcast(t4_c1, t4_l1);
    Write("t4_signaller Releasing t4_l1", sizeof("t4_signaller Releasing t4_l1"), 1);
    Release(t4_l1);
    Exit(0);
}

int main()
{
    int i;

	char* buffer = "t4_l1";
	t4_l1 = CreateLock(buffer, sizeof(buffer));

	buffer = "t4_c1";
	t4_c1 = CreateCondition(buffer, sizeof(buffer));

    for(i = 0; i < 5; i++)
    {
	   Fork((void*)t4_waiter);
    }
    Fork((void*)t4_signaller);
}