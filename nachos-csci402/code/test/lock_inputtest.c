#include "syscall.h"

int lit_l1;

lit_t1()
{
	Write("Starting Lock Input Test", sizeof("Starting Lock Input Test"), 1);
	Write("Test Phase: Acquire_Syscall\n", sizeof("Test Phase: Acquire_Syscall\n"), 1);

	/* Test all possible invalid inputs for the acquire syscall */
	Write("Testing index < 0: ", sizeof("Testing index < 0: "), 1);
	Acquire(-1);

	Write("Testing index out of bounds: ", sizeof("Testing index out of bounds: "), 1);
	Acquire(lit_l1 + 1);

	Write("\nTest Phase: Release_Syscall\n", sizeof("Test Phase: Release_Syscall\n"), 1);

	/* Test all possible invalid inputs for the acquire syscall */
	Write("Testing index < 0: ", sizeof("Testing index < 0: "), 1);
	Release(-1);

	Write("Testing index out of bounds: ", sizeof("Testing index out of bounds: "), 1);
	Release(lit_l1 + 1);

	Exit(0);
}

int main()
{
	char* buffer = "Test Acquire Lock";

	/* Test invalid input for CreateLock */
	
	lit_l1 = CreateLock(-1, sizeof(buffer));
	lit_l1 = CreateLock(buffer, sizeof(buffer));

	Fork((void*)lit_t1);
}