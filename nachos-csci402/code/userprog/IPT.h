#ifndef IPT_H_
#define IPT_H_

#include "machine.h"
#include "addrspace.h"
#include "list.h"


//THESE CLASSES WILL BE USED FOR THE IPT
class IPTEntry : public TranslationEntry
{
public:
	IPTEntry() ;
	AddrSpace *m_addressSpace;
};

class InversePageTable
{
public:

	InversePageTable(int policy);
	~InversePageTable();
	int m_evictionPolicy; //0 is FIFO ... 1 is RAND

	List *m_table;
	IPTEntry *m_entryTable[NumPhysPages];

	void InsertNewEntry(AddrSpace *space, TranslationEntry *entry, int physicalPage);//will return -1 if no good entry
	
};






#endif
