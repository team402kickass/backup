#ifndef SERVER_DATA_H_
#define SERVER_DATA_H_
#include <stdio.h>
#include <string.h>
#include "list.h"
#include "../network/post.h"
const int MAX_LOCKS = 5000;
const int MAX_MVS = 5000;
const int MAX_CVS = 5000;

using namespace std;

typedef struct
{
	int m_ownerMailboxID,
		m_ownerMachineID,
		m_waitingLock;
} WaitingClient;



class Response
{
public:
	int m_ownerMachineID,
		m_ownerMailboxID;

	bool sendOnce;
	std::string m_msg;

	Response();
};

class ServerData 
{
	public:

		ServerData();
		
		int FindMVIndex(std::string name, int size, int initValue);
		int FindLockIndex(std::string name);
		int FindCVIndex(std::string name);

		int ValidateMV(int index, int valIndex);
		int ValidateLock(int index);
		int ValidateCV(int index);

		int GetMV(int index, int valIndex);
		int SetMV(int index, int val, int valIndex);
		void DestroyMV(int index);

		void DestroyLock(int index);

		void DestroyCV(int index);

		bool DoesClientOwnLock(int lockIndex , int machineID, int mailboxID);
		bool Acquire(int index, std::string msg, int machineID, int mailboxID);
		bool DoesLockMatchCV(int cvIndex, int lockIndex);

		WaitingClient* GetWaitingClient( int cvIndex);
		void  AddToWaitQueue(int cvIndex, int waitingLock, int machineID, int mailboxID);

		Response* Release(int lockindex, int machineID, int mailboxID);

	//	Response* Wait(int cvindex, int lockindex,int machineID, int mailboxID);

	//	bool Signal(int cvindex, int lockindex);

	//	bool Broadcast(int lockindex);
	private:

		void InsertNewMV(int index, std::string name, int size, int initValue);
		void InsertNewLock(int index, std::string name);
		void InsertNewCV(int index, std::string name);

		

		typedef struct 
		{
			List *m_waitQueue;

			int m_ownerMachineID,
			 	m_ownerMailboxID;

			bool m_free,
				 m_toBeDestroyed;
			std::string m_name;
		} ServerLock;


		typedef struct 
		{
			int *m_value,
				m_count;//number in the array, would be the size...
			
			std::string m_name;

			//int m_destroyed; // check to see if the MV is destroyed or not, 0 = live, 1 = destroyed
		} ServerMV;

		typedef	struct 
		{
			bool m_toBeDestroyed;

			int m_ownerMachineID,
			 	m_ownerMailboxID;

			int m_waitingLock;

			std::string m_name;
			List *m_waitQueue;

		} ServerCV;


		ServerCV *m_cvList[MAX_LOCKS];
		ServerMV *m_mvList[MAX_LOCKS];
		ServerLock *m_lockList[MAX_LOCKS];

	int m_lockCount,
		m_cvCount,
		m_mvCount;

	
		//add other methods here

};


#endif

