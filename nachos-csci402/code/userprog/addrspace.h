// addrspace.h 
//	Data structures to keep track of executing user programs 
//	(address spaces).
//
//	For now, we don't keep any information about address spaces.
//	The user level CPU state is saved and restored in the thread
//	executing the user program (see thread.h).
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef ADDRSPACE_H
#define ADDRSPACE_H

#include "copyright.h"
#include "filesys.h"
#include "table.h"

#define UserStackSize		1024 	// increase this as necessary!

#define MaxOpenFiles 256
#define MaxChildSpaces 256


class PageTableEntry : public TranslationEntry
{
public:
    // 0 is exec, 1 is swap, 2 is neither
    int m_diskLocation,
        m_byteOffSet;
};




class AddrSpace {
  public:
    AddrSpace(OpenFile *executable, int index);	// Create an address space,
					// initializing it with the program
					// stored in the file "executable"
    ~AddrSpace();			// De-allocate an address space

    void InitRegisters();		// Initialize user-level CPU registers,
					// before jumping to user code

    void RemovePages(int pageIndex);
    int ResizePageTable();
    void SaveState();			// Save/restore address space-specific
    void RestoreState();		// info on a context switch
    Table fileTable;			// Table of openfiles
    int GetProcessIndex() {return m_processIndex;} 

    PageTableEntry *pageTable; 
    
    void ReadPageIntoMemory(int VPN, int PPN, int swapFile);
    void InvalidatePage( int PPN);
    int GetNumberOfPages() {return numPages;}
    void InsertEntryForVPN(int vpn, int physicalPage);
    
 private:
    OpenFile *m_executable;
    Lock *m_pageTableResizeLock;
    Lock *m_executableLock;
    unsigned int numPages, // Number of pages in the virtual    
                 m_processIndex;							
                    // address space
};

#endif // ADDRSPACE_H
