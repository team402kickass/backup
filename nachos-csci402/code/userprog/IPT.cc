#include "IPT.h"
#include "system.h"


InversePageTable::InversePageTable(int policy)
{
	m_evictionPolicy = policy;
	m_table = new List();
	//init the IPT in the begginign, they will all be used at one point
	for(int i=0;i < NumPhysPages ; i++)
	{
		m_entryTable[i] = new IPTEntry();
		m_entryTable[i]->virtualPage = -1;
		m_entryTable[i]->physicalPage = -1;
		m_entryTable[i]->m_addressSpace = NULL;
	}
}



void InversePageTable::InsertNewEntry(AddrSpace *space, TranslationEntry *pageEntry, int physicalPage)
{
	//here will insert the chosen page table into the IPT
	pageEntry->valid = TRUE; //we are now using this page
	pageEntry->physicalPage = physicalPage;

	
	m_entryTable[physicalPage]->m_addressSpace = space;
	m_entryTable[physicalPage]->virtualPage = pageEntry->virtualPage;
	m_entryTable[physicalPage]->physicalPage = physicalPage;
	m_entryTable[physicalPage]->valid = pageEntry->valid;
	m_entryTable[physicalPage]->use = pageEntry->use;
	m_entryTable[physicalPage]->dirty = pageEntry->dirty;
	m_entryTable[physicalPage]->readOnly = pageEntry->readOnly;
	
	

}



InversePageTable::~InversePageTable()
{
	delete m_table;
}








IPTEntry::IPTEntry()
{

}
