// exception.cc 
//  Entry point into the Nachos kernel from user programs.
//  There are two kinds of things that can cause control to
//  transfer back to here from user code:
//
//  syscall -- The user code explicitly requests to call a procedure
//  in the Nachos kernel.  Right now, the only function we support is
//  "Halt".
//
//  exceptions -- The user code does something that the CPU can't handle.
//  For instance, accessing memory that doesn't exist, arithmetic errors,
//  etc.  
//
//  Interrupts (which can also cause control to transfer from user
//  code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include <stdio.h>
#include <iostream>
#include "synch.h"
#include <sstream>
#include <string.h>
#include "interrupt.h"




using namespace std;

const int MAX = 500;
const int MAX_THREADS = 50;
int  g_numberOfRunningProcesses = 0,
     g_nextLockIndex = 0,
     g_nextConditionIndex = 0;



typedef struct 
{
  bool m_isToBeDestroyed,
       m_isFree;
  Condition *m_condition;
  int m_virtualAddress;
  AddrSpace *m_addressSpace;

} KernelCondition;


typedef struct 
{
    bool m_isToBeDestroyed,
         m_isFree;
    Lock *m_lock;
    AddrSpace* m_addressSpace;
} KernelLock;




typedef struct 
{
  int m_stackPage,
      m_virtualAddress;
  Thread *m_thread;
  bool m_isToBeDestroyed;

} KernelThread;



void Fork_Thread(int kernelT)
{
    KernelThread *kernelThread = (KernelThread*)kernelT;
      
   // currentThread->space->InitRegisters();
    machine->WriteRegister(PCReg, kernelThread->m_virtualAddress);
    machine->WriteRegister(NextPCReg, (kernelThread->m_virtualAddress +4) );
    currentThread->space->RestoreState(); // call RestoreState()
    machine->WriteRegister(StackReg,kernelThread->m_stackPage);
    machine->Run();
}
Lock *g_threadLock = new Lock("thread lock");
KernelThread * KernelThreadConstruct(AddrSpace *addressSpace, int virtualAddress, Thread *thread )
{
   g_threadLock->Acquire();
    KernelThread *kernel = new KernelThread();
    kernel->m_isToBeDestroyed = false;
    kernel->m_virtualAddress = virtualAddress;
   if(thread == NULL)
    {
         kernel->m_thread  = new Thread("thread");
    }
    else
    {
       kernel->m_thread = thread; 
    }
    
    kernel->m_thread->space = addressSpace;

     kernel->m_stackPage = kernel->m_thread->space->ResizePageTable();
    if(kernel->m_stackPage == -1)
    {
      printf("Unable to allocat the pages\n");
      g_threadLock->Release();
      return NULL;
    }

    g_threadLock->Release();
    return kernel;
}



typedef struct 
{
  int m_threadCount;
  AddrSpace *m_addressSpace;
  KernelThread* m_threads[MAX_THREADS];

} KernelProcess;


//i dont think we need this... just need the Fork_Thread
void Exec_Thread(int index)
{
  currentThread->space->InitRegisters();
  currentThread->space->RestoreState();
  machine->Run();
}

Lock *g_processSpecificLock[MAX];
KernelProcess* s_kernelProcesses[MAX];
Lock *g_processLock = new Lock("process lock");



 KernelProcess* KernelProcessConstruct(OpenFile *executable, int virtualaddress, Thread *thread)
{
   g_processLock->Acquire();
    KernelProcess *kernel = new KernelProcess();
    kernel->m_addressSpace = new AddrSpace( executable , g_numberOfRunningProcesses);
    kernel->m_threadCount = 0;
    for(int i=0;i< MAX_THREADS;i++)
    {
      kernel->m_threads[i] = NULL;
    }

    KernelThread *kernelThread = KernelThreadConstruct( kernel->m_addressSpace, virtualaddress,thread );
    kernelThread->m_thread->SetThreadIndex( kernel->m_threadCount );
    kernel->m_threads[ kernel->m_threadCount++ ] = kernelThread;
  //  kernelThread->m_thread->Fork((VoidFunctionPtr)Exec_Thread,-69);
    Lock *lock = new Lock("process specific lock");

    g_processSpecificLock[ g_numberOfRunningProcesses++ ] = lock;
    s_kernelProcesses[ kernel->m_addressSpace->GetProcessIndex() ] = kernel;
     g_processLock->Release();
     if(thread == NULL)
    {

        kernelThread->m_thread->Fork((VoidFunctionPtr)Exec_Thread,(int)kernelThread);
    }
    else
    {
       kernelThread->m_thread->space->InitRegisters();
       kernelThread->m_thread->space->RestoreState();
       machine->Run();
    }

    return kernel;
}



//global variables

KernelLock* s_KernelLocks[MAX];
KernelCondition* s_KernelConditions[MAX];

int g_currentTLBIndex = -1;
Lock *s_KernelLocksLock = new Lock("Kernel Lock Lock");
Lock *s_KernelConditionsLock = new Lock("Kernel conditions lock");
Lock *s_MemoryFullLock = new Lock("Inverted Page Table Lock");
Lock *s_SwapFileBMLock = new Lock("Swap File BitMap Lock");
Lock *s_pageFaultLock = new Lock("page fault lock");
Lock *s_IPTMissLock = new Lock("ipt miss lock");
Lock *s_IPTLock = new Lock("ipt lock");

int copyin(unsigned int vaddr, int len, char *buf) {
    // Copy len bytes from the current thread's virtual address vaddr.
    // Return the number of bytes so read, or -1 if an error occors.
    // Errors can generally mean a bad virtual address was passed in.
    bool result;
    int n=0;      // The number of bytes copied in
    int *paddr = new int;

    while ( n >= 0 && n < len) {
      result = machine->ReadMem( vaddr, 1, paddr );
      while(!result) // FALL 09 CHANGES
    {
        result = machine->ReadMem( vaddr, 1, paddr ); // FALL 09 CHANGES: TO HANDLE PAGE FAULT IN THE ReadMem SYS CALL
    } 
      
      buf[n++] = *paddr;
     
      if ( !result ) {
  //translation failed
  return -1;
      }

      vaddr++;
    }

    delete paddr;
    return len;
}

int copyout(unsigned int vaddr, int len, char *buf) {
    // Copy len bytes to the current thread's virtual address vaddr.
    // Return the number of bytes so written, or -1 if an error
    // occors.  Errors can generally mean a bad virtual address was
    // passed in.
    bool result;
    int n=0;      // The number of bytes copied in

    while ( n >= 0 && n < len) {
      // Note that we check every byte's address
      result = machine->WriteMem( vaddr, 1, (int)(buf[n++]) );

      if ( !result ) {
  //translation failed
  return -1;
      }

      vaddr++;
    }

    return n;
}

void Create_Syscall(unsigned int vaddr, int len) {
    // Create the file with the name in the user buffer pointed to by
    // vaddr.  The file name is at most MAXFILENAME chars long.  No
    // way to return errors, though...
    char *buf = new char[len+1];  // Kernel buffer to put the name in

    if (!buf) return;

    if( copyin(vaddr,len,buf) == -1 ) {
  printf("%s","Bad pointer passed to Create\n");
  delete buf;
  return;
    }

    buf[len]='\0';

    fileSystem->Create(buf,0);
    delete[] buf;
    return;
}

int Open_Syscall(unsigned int vaddr, int len) {
    // Open the file with the name in the user buffer pointed to by
    // vaddr.  The file name is at most MAXFILENAME chars long.  If
    // the file is opened successfully, it is put in the address
    // space's file table and an id returned that can find the file
    // later.  If there are any errors, -1 is returned.
    char *buf = new char[len+1];  // Kernel buffer to put the name in
    OpenFile *f;      // The new open file
    int id;       // The openfile id

    if (!buf) {
  printf("%s","Can't allocate kernel buffer in Open\n");
  return -1;
    }

    if( copyin(vaddr,len,buf) == -1 ) {
  printf("%s","Bad pointer passed to Open\n");
  delete[] buf;
  return -1;
    }

    buf[len]='\0';

    f = fileSystem->Open(buf);
    delete[] buf;

    if ( f ) {
  if ((id = currentThread->space->fileTable.Put(f)) == -1 )
      delete f;
  return id;
    }
    else
  return -1;
}

void Write_Syscall(unsigned int vaddr, int len, int id) {
    // Write the buffer to the given disk file.  If ConsoleOutput is
    // the fileID, data goes to the synchronized console instead.  If
    // a Write arrives for the synchronized Console, and no such
    // console exists, create one. For disk files, the file is looked
    // up in the current address space's open file table and used as
    // the target of the write.
    
    char *buf;    // Kernel buffer for output
    OpenFile *f;  // Open file for output
    if ( id == ConsoleInput) return;
    
    if ( !(buf = new char[len]) ) {
  printf("%s","Error allocating kernel buffer for write!\n");
  return;
    } else {
        if ( copyin(vaddr,len,buf) == -1 ) {
      printf("%s","Bad pointer passed to to write: data not written\n");
      delete[] buf;
      return;
  }
    }

    if ( id == ConsoleOutput) {
      for (int ii=0; ii<len; ii++) {
  printf("%c",buf[ii]);
      }
      printf("\n");
    } else {
  if ( (f = (OpenFile *) currentThread->space->fileTable.Get(id)) ) {
      f->Write(buf, len);
  } else {
      printf("%s","Bad OpenFileId passed to Write\n");
      len = -1;
  }
    }

    delete[] buf;
}

int Read_Syscall(unsigned int vaddr, int len, int id) {
    // Write the buffer to the given disk file.  If ConsoleOutput is
    // the fileID, data goes to the synchronized console instead.  If
    // a Write arrives for the synchronized Console, and no such
    // console exists, create one.    We reuse len as the number of bytes
    // read, which is an unnessecary savings of space.
    char *buf;    // Kernel buffer for input
    OpenFile *f;  // Open file for output

    if ( id == ConsoleOutput) return -1;
    
    if ( !(buf = new char[len]) ) {
  printf("%s","Error allocating kernel buffer in Read\n");
  return -1;
    }

    if ( id == ConsoleInput) {
      //Reading from the keyboard
      scanf("%s", buf);

      if ( copyout(vaddr, len, buf) == -1 ) {
  printf("%s","Bad pointer passed to Read: data not copied\n");
      }
    } else {
  if ( (f = (OpenFile *) currentThread->space->fileTable.Get(id)) ) {
      len = f->Read(buf, len);
      if ( len > 0 ) {
          //Read something from the file. Put into user's address space
            if ( copyout(vaddr, len, buf) == -1 ) {
        printf("%s","Bad pointer passed to Read: data not copied\n");
    }
      }
  } else {
      printf("%s","Bad OpenFileId passed to Read\n");
      len = -1;
  }
    }

    delete[] buf;
    return len;
}

void Close_Syscall(int fd) {
    // Close the file associated with id fd.  No error reporting.
    OpenFile *f = (OpenFile *) currentThread->space->fileTable.Remove(fd);

    if ( f ) {
      delete f;
    } else {
      printf("%s","Tried to close an unopen file\n");
    }
}

void Yield_Syscall()
{
  currentThread->Yield();
}



int ValidatedVirtualAddress(int virtualaddress, int len, char *name)
{
  if(!name)
  {
    printf("Unable to allocate name \n");
    return -1;
  }

  if( (virtualaddress < 0) || (virtualaddress % 4) != 0)
  {
     printf("Invalid virtual address. Not valid number\n");
      return -1;
  }

  if(copyin(virtualaddress,len,name) == -1)
  {
    delete name;
    printf("Invalid virtual address\n");
    return -1;
  }

  name[len] = '\0'; 
  return 0;
}


void Fork_Syscall( int virtualaddress)
{
  char *name;
  name = new char[1];
  if(ValidatedVirtualAddress(virtualaddress, 1 , name) == -1)
  {
      printf("Error happened in Fork Syscall");
      return;
  }
  delete name;


  KernelThread *kernelThread = KernelThreadConstruct(currentThread->space, virtualaddress, NULL);
  

  int processIndex = currentThread->space->GetProcessIndex();
  g_processSpecificLock[ processIndex ]->Acquire();
  KernelProcess *process = s_kernelProcesses[ processIndex ];
  kernelThread->m_thread->SetThreadIndex( process->m_threadCount );
  process->m_threads[ process->m_threadCount++ ] = kernelThread; //assign the thread the process and then increment it

  g_processSpecificLock[ processIndex ]->Release();

  kernelThread->m_thread->space->RestoreState();
 
  kernelThread->m_thread->Fork((VoidFunctionPtr)Fork_Thread, (int)kernelThread);
}


int Exec_Syscall(int virtualaddress,int len)
{
  char *name;
  OpenFile *file;
  name = new char[++len]; //we do ++len here because we want to increase the value before we pass it in
  
  if(ValidatedVirtualAddress(virtualaddress, len, name) == -1)
  {
       printf("Root validation was in the Exec call\n");
      return -1;
  }

  
  // Open that file
 // g_processLock->Acquire();
  file = fileSystem->Open(name);
 // delete name;
  if(file == NULL)
  {
    printf("Unable to open the specified file\n");
    g_processLock->Release();
    return -1;
  }
  //this will handle the creation of the first process;
  KernelProcess *process = KernelProcessConstruct( file, virtualaddress, NULL );
 // s_kernelProcesses[ process->m_addressSpace->GetProcessIndex() ] = process;

 // g_processLock->Release();

  return process->m_addressSpace->GetProcessIndex();
}


int Exit_Syscall()
{

  if(currentThread == NULL)
  {
      printf("Current Thread is null??\n");
      return 0;
  }

  int processId = currentThread->space->GetProcessIndex();
  if(processId > MAX || processId < 0)
  {
    printf("Process ID is not within range of possible Ids\n");
    return 0;
  }

  g_processSpecificLock[processId]->Acquire();
  KernelProcess *process = s_kernelProcesses[processId];

  if(process == NULL)
  {
    g_processSpecificLock[processId]->Release(); //maybe delete the lock??
    printf("The process you are trying to delete is NULL\n");
    currentThread -> Finish();
    return 0;
  }

  
  if( process->m_threadCount == 1)
  {
      g_processLock->Acquire();
      delete process;
      s_kernelProcesses[processId] = NULL;
      
      g_numberOfRunningProcesses--;
      if(g_numberOfRunningProcesses == 0)
      {

        printf("\nEXIT VALUE: %d\n\n",machine->ReadRegister(4) );
        printf("No more processes running. Stopping now\n");
        currentThread->space->RemovePages( process->m_threads[ currentThread->GetThreadIndex() ]->m_stackPage );
        //TODO: Do we need to release the threads stack sapce here even though nachos exits?
     
        g_processSpecificLock[processId]->Release();
        interrupt->Halt();
        g_processLock->Release();
        currentThread->Finish();
        printf("Halting\n");
        
        return 0;//probalby wnat to return something else here for the last process
      }
      else
      {
         printf("Not the last process\n");
         process->m_threadCount--;
         currentThread->space->RemovePages( process->m_threads[ currentThread->GetThreadIndex() ]->m_stackPage );
         g_processSpecificLock[processId]->Release();
         g_processLock->Release();
         currentThread->Finish();
      }
      
  }
  else if(process->m_threadCount > 1)
  {
      printf("Destroying a thread \n");
       process->m_threadCount--;
      currentThread->space->RemovePages( process->m_threads[ currentThread->GetThreadIndex() ]->m_stackPage );
       g_processSpecificLock[processId]->Release();
      currentThread->Finish();
     }

  g_processSpecificLock[processId]->Release();

   return 1;
}



//we need to change these variables, just fixing compiler issues, will look at later

int  CreateLock_Syscall(int virtualaddress, int len)
{

  char *name = new char[++len];
  //validate the input
  if(ValidatedVirtualAddress(virtualaddress, len, name) == -1)
  {
       printf("Root validation was in the Create_Syscall call\n");
      return -1;
  }
  s_KernelLocksLock->Acquire();
  //check if the table is full or not
  if(g_nextLockIndex >= MAX)
  {
    printf("Lock Table is full!\n");
    s_KernelLocksLock->Release();
    return -1;
  }

  s_KernelLocks[g_nextLockIndex] = new KernelLock();
  s_KernelLocks[g_nextLockIndex]->m_lock = new Lock(name);
  
  s_KernelLocks[g_nextLockIndex]->m_addressSpace = currentThread->space;
  s_KernelLocks[g_nextLockIndex]->m_isFree = true;
  s_KernelLocks[g_nextLockIndex]->m_isToBeDestroyed = false;

  int index = g_nextLockIndex;
  g_nextLockIndex++;
  s_KernelLocksLock->Release();
  return index; //should return the correct index and then increment


}

void DestroyLock_Syscall(int index)
{
  s_KernelLocksLock->Acquire();


  //TODO: group standard errors
  if(s_KernelLocks[index] == NULL)
  {
    printf("The kernel Lock struct has been delete or was never made\n");
    s_KernelLocksLock->Release();
    return;
  }

  //validate input
  if(s_KernelLocks[index]->m_lock == NULL || index < 0 || index >= g_nextLockIndex)
  {
    printf("Invalid Lock index\n");
    s_KernelLocksLock->Release();
    return;
  }

  if(s_KernelLocks[index]->m_addressSpace != currentThread->space)
  {
    printf("Seperate process trying to destroy our lock!\n");
    s_KernelLocksLock->Release();
    return;
  }
  //TODO: END

  //check if a lock is in use
  if(s_KernelLocks[index]->m_isFree == false)
  {
    printf("Lock is in use, can't destroy now\n");
    s_KernelLocks[index]->m_isToBeDestroyed = true;
    s_KernelLocksLock->Release();
    return;
  }

  //check if a lock satisfy the conditions
  if(s_KernelLocks[index]->m_isToBeDestroyed == true && s_KernelLocks[index]->m_isFree == true)
  {
    printf("Lock Destroyed\n");
    delete s_KernelLocks[index]->m_lock;
    delete s_KernelLocks[index];
    s_KernelLocks[index] = NULL;
    s_KernelLocksLock->Release();
    return;
  }

  s_KernelLocksLock->Release();
}

void Acquire_Syscall(int index)
{

  s_KernelLocksLock->Acquire();
  
  //TODO: group standard errors
  if(s_KernelLocks[index] == NULL)
  {
    printf("Tried to Acquire an invalid KernelLock\n");
    s_KernelLocksLock->Release();
    return;
  }

  if(s_KernelLocks[index]->m_lock == NULL || index < 0 || index >= g_nextLockIndex)
  {
    printf("Invalid Lock index passed to Acquire_Syscall\n");
    s_KernelLocksLock->Release();
    return;
  }

  if(s_KernelLocks[index]->m_addressSpace != currentThread->space)
  {
    printf("Seperate process trying to aquire our lock!\n");
    s_KernelLocksLock->Release();
    return;
  }
  //TODO: END


  if(s_KernelLocks[index]->m_isToBeDestroyed && s_KernelLocks[index]->m_isFree)
  {
    printf("Trying to Acquire a lock that is supposed to be destroyed\nLock Destroyed\n");
    delete s_KernelLocks[index]->m_lock;
    delete s_KernelLocks[index];
    s_KernelLocks[index] == NULL;
    s_KernelLocksLock->Release();
    return;
  }
  s_KernelLocks[index]->m_isFree = false;
  s_KernelLocksLock->Release();
  //Acqurie Kernel Lock
  s_KernelLocks[index]->m_lock->Acquire();
  
  
  
}

void Release_Syscall(int index)
{
  s_KernelLocksLock->Acquire();


  //TODO: group standard errors
  if(s_KernelLocks[index] == NULL)
  {
    printf("Tried to Release an invalid KernelLock\n");
    s_KernelLocksLock->Release();
    return;
  }

  if(s_KernelLocks[index]->m_lock == NULL || index < 0 || index >= g_nextLockIndex)
  {
    printf("Invalid Lock index passed to Release_Syscall\n");
    s_KernelLocksLock->Release();
    return;
  }

  if(s_KernelLocks[index]->m_addressSpace != currentThread->space)
  {
    printf("Seperate process trying to release our lock!\n");
    s_KernelLocksLock->Release();
    return;
  }

  if(!s_KernelLocks[index]->m_lock->isHeldByCurrentThread() )
  {
    printf("This thread does not own this lock\n");
    s_KernelLocksLock->Release();
    return;
  }

  

  if(s_KernelLocks[index]->m_isToBeDestroyed)
  {
    printf("Destroying KernelLock %d\n", index);
    s_KernelLocks[index]->m_lock->Release();
    s_KernelLocks[index]->m_isFree = true;
    delete s_KernelLocks[index]->m_lock;
    delete s_KernelLocks[index];
    s_KernelLocks[index] = NULL;
    s_KernelLocksLock->Release();
    return;
  }

  s_KernelLocks[index]->m_isFree = true;

  s_KernelLocksLock->Release();
  //Release Kernel Lock
  s_KernelLocks[index]->m_lock->Release();
}

int CreateCondition_Syscall(int virtualaddress, int len)
{
  char *name = new char[++len];
  //validate the input
  if(ValidatedVirtualAddress(virtualaddress, len, name) == -1)
  {
       printf("Root validation was in the Create_Syscall call\n");
      return -1;
  }
  s_KernelConditionsLock->Acquire();
  //check if the table is full or not
  if(g_nextConditionIndex >= MAX)
  {
    printf("Condition Table is full!\n");
    s_KernelConditionsLock->Release();
    return -1;
  }

  s_KernelConditions[g_nextConditionIndex] = new KernelCondition();
  s_KernelConditions[g_nextConditionIndex]->m_condition = new Condition(name);
  

  s_KernelConditions[g_nextConditionIndex]->m_addressSpace = currentThread->space;
  s_KernelConditions[g_nextConditionIndex]->m_isFree = true;
  s_KernelConditions[g_nextConditionIndex]->m_isToBeDestroyed = false;

  int lock = g_nextConditionIndex;
  g_nextConditionIndex++;
  s_KernelConditionsLock->Release();
  return lock;
}

void DestroyCondition_Syscall(int index)
{
  s_KernelConditionsLock->Acquire();

  //validate input
  if(s_KernelConditions[index]->m_condition == NULL || index < 0 || index >= g_nextConditionIndex)
  {
    printf("Invalid condition index\n");
    s_KernelConditionsLock->Release();
    return;
  }

  //check if a condition is in use
  if(s_KernelConditions[index]->m_isFree == false)
  {
    printf("Condition is in use, can't destroy now\n");
    s_KernelConditions[index]->m_isToBeDestroyed = true;
    s_KernelConditionsLock->Release();
    return;
  }

  //check if a lock satisfy the conditions
  if(s_KernelConditions[index]->m_isToBeDestroyed == true && s_KernelConditions[index]->m_isFree == true)
  {
    printf("Condition Destroyed\n");
    delete s_KernelConditions[index]->m_condition;
    delete s_KernelConditions[index];

    s_KernelConditions[index] = NULL;
    s_KernelConditionsLock->Release();
    return;
  }

  s_KernelConditionsLock->Release();
}

void Wait_Syscall(int index, int lockIndex)
{
  s_KernelConditionsLock->Acquire();
  //validate input - index
  if(s_KernelConditions[index] == NULL)
  {
    printf("Tried to pass a non-existant KernelCondition to Wait_Syscall\n");
    s_KernelConditionsLock->Release();
    return;
  }
  if(s_KernelConditions[index]->m_condition == NULL || index < 0 || index >= g_nextConditionIndex)
  {
    printf("Invalid condition index\n");
    s_KernelConditionsLock->Release();
    return;
  }

  //validate input - lockIndex
  s_KernelLocksLock->Acquire();
  if(s_KernelLocks[lockIndex] == NULL)
  {
    printf("Tried to pass a non-existant KernelLock to Wait_Syscall\n");
    s_KernelLocksLock->Release();
    s_KernelConditionsLock->Release();
    return;
  }
  if(s_KernelLocks[lockIndex]->m_lock == NULL || lockIndex < 0 || lockIndex >= g_nextLockIndex)
  {
    printf("Invalid Lock index passed to Wait_Syscall\n");
    s_KernelLocksLock->Release();
    s_KernelConditionsLock->Release();
    return;
  }
  

  //do task
  if (s_KernelConditions[index]->m_isToBeDestroyed && s_KernelConditions[index]->m_isFree)
  {
    printf("Trying to wait on a Condition that is to be destroyed. Invalid\n");
    delete s_KernelConditions[index]->m_condition;
    delete s_KernelConditions[index];

    s_KernelConditions[index] = NULL;
    s_KernelLocksLock->Release();
    s_KernelConditionsLock->Release();
    return;
  }


  s_KernelConditions[index]->m_isFree = false;
  s_KernelLocksLock->Release();
  s_KernelConditionsLock->Release();
  //Wait on the kernel lock
  s_KernelConditions[index]->m_condition->Wait(s_KernelLocks[lockIndex]->m_lock);

  
 
}

void Signal_Syscall(int index, int lockIndex)
{
  s_KernelConditionsLock->Acquire();
  //validate input - index
  if(s_KernelConditions[index] == NULL)
  {
    printf("Tried to pass a non-existant KernelCondition to Signal_Syscall\n");
    s_KernelConditionsLock->Release();
    return;
  }
  if(s_KernelConditions[index]->m_condition == NULL || index < 0 || index >= g_nextConditionIndex)
  {
    printf("Invalid condition index\n");
    s_KernelConditionsLock->Release();
    return;
  }

  //validate input - lockIndex
  s_KernelLocksLock->Acquire();
  if(s_KernelLocks[lockIndex] == NULL)
  {
    printf("Tried to pass a non-existant KernelLock to Signal_Syscall\n");
    s_KernelLocksLock->Release();
    s_KernelConditionsLock->Release();
    return;
  }
  if(s_KernelLocks[lockIndex]->m_lock == NULL || lockIndex < 0 || lockIndex >= g_nextLockIndex)
  {
    printf("Invalid Lock index passed to Signal_Syscall\n");
    s_KernelLocksLock->Release();
    s_KernelConditionsLock->Release();
    return;
  }
  

  if(s_KernelConditions[index]->m_isToBeDestroyed)
  {
    printf("Destroying KernelCondition %d\n", index);
    //s_KernelConditions[index]->m_condition->Signal(s_KernelLocks[lockIndex]->m_lock);
    s_KernelConditions[index]->m_isFree = true;
    delete s_KernelConditions[index]->m_condition;
    delete s_KernelConditions[index];
    s_KernelConditions[index] = NULL;
    s_KernelLocksLock->Release();
    s_KernelConditionsLock->Release();
    return;
  }

  s_KernelConditions[index]->m_isFree = true;
 
  

   s_KernelLocksLock->Release();
  s_KernelConditionsLock->Release();
  //Signal the kernel lock
  s_KernelConditions[index]->m_condition->Signal(s_KernelLocks[lockIndex]->m_lock);
}

void Broadcast_Syscall(int index, int lockIndex)
{
  s_KernelConditionsLock->Acquire();

  //validate input - index
  if(s_KernelConditions[index] == NULL)
  {
    printf("Tried to pass a non-existant KernelCondition to Signal_Syscall\n");
    s_KernelConditionsLock->Release();
    return;
  }
  if(s_KernelConditions[index]->m_condition == NULL || index < 0 || index >= g_nextConditionIndex)
  {
    printf("Invalid condition index\n");
    s_KernelConditionsLock->Release();
    return;
  }

  //validate input - lockIndex
  s_KernelLocksLock->Acquire();
  if(s_KernelLocks[lockIndex] == NULL)
  {
    printf("Tried to pass a non-existant KernelLock to Signal_Syscall\n");
    s_KernelLocksLock->Release();
    s_KernelConditionsLock->Release();
    return;
  }
  if(s_KernelLocks[lockIndex]->m_lock == NULL || lockIndex < 0 || lockIndex >= g_nextLockIndex)
  {
    printf("Invalid Lock index passed to Signal_Syscall\n");
    s_KernelLocksLock->Release();
    s_KernelConditionsLock->Release();
    return;
  }
  

  //do task
  s_KernelConditions[index]->m_isFree = true;
  
  

  s_KernelLocksLock->Release();
  s_KernelConditionsLock->Release();
  //Broadcast the kernel lock
  s_KernelConditions[index]->m_condition->Broadcast(s_KernelLocks[lockIndex]->m_lock);
}
/*
  a * 100 + b
  c * 100 + d
*/
void MyPrint(unsigned int vaddr, int len, int number, int number2 )
{
  char *buf;
     if ( !(buf = new char[len]) ) {
  printf("%s","Error allocating kernel buffer for write!\n");
  return;
    } else {
        if ( copyin(vaddr,len,buf) == -1 ) {
      printf("%s","Bad pointer passed to to write: data not written\n");
      delete[] buf;
      return;
  }
    }

    
      for (int ii=0; ii<len; ii++) {
      printf("%c",buf[ii]);
      }
     
      

    int a = number % 100;
    int b = number / 100;
    //int c = number2 % 100;
   // int d = number2 / 100;
    printf(" ");
    printf("Action dooer: %d", b);
    if(a != 0)
    {
      printf("  Action reciever: %d", a);
    }
    printf("\n");
}

int HandleMemoryFails()
{
  //to test need to set numphyspage to 32 first
  int ppn = 0;
  //Random 
  
  if(g_ipt->m_evictionPolicy == 0) //FIFO
  {
    //remove the first entry from the table for FIFP
    ppn = (int)g_ipt->m_table->Remove();
  }
  else// random 
  { 
   
   //need locks aroudn the random section so only 1 thread is in there at a time
    s_MemoryFullLock->Acquire();
    while(true)
    {   
      //find some random page
       ppn = rand()%NumPhysPages;
       if(!g_ipt->m_entryTable[ppn]->use)
       {
        //need to set their used bit so no one else claims this page
          g_ipt->m_entryTable[ppn]->use = true;
          break;
       }

    }
    s_MemoryFullLock->Release();
  }

  //we need to see if the page we are evicting is currently in the TLB
  //need to copy the dirty bit and make that page invalid
 // int virtualPage = -1;
  //find the possible page in the TLB that we are going to evict and see if that page
  //is dirty
  IntStatus oldLevel = interrupt->SetLevel(IntOff);
  for(unsigned int i =0;i <TLBSize ;i++)
  {
    if(machine->tlb[i].physicalPage == ppn)
    {
        g_ipt->m_entryTable[ppn]->dirty = machine->tlb[i].dirty;
        machine->tlb[i].valid = false;
        currentThread->space->pageTable[ machine->tlb[i].virtualPage ].valid = false;

        break;
    }
  }
  (void) interrupt->SetLevel(oldLevel);


  //if the page is dirty we need to write it to the swap file
  if(g_ipt->m_entryTable[ppn]->dirty)
  {
    s_SwapFileBMLock->Acquire();

    int virtualPage = g_ipt->m_entryTable[ppn]->virtualPage;
    //we will just write back to where we were origionally stored
    int sfpn = -1;
    int swapPage; 
    //this is a nice littler conservation thing.
    //we know if thehir disklocation isnt 1 they have no been written to the 
    //swap file so we need to add it and give it a location
    //if it has been we will just write back to the same place
    if( currentThread->space->pageTable[ virtualPage ].m_diskLocation != 1 )
    {

       s_MemoryFullLock->Acquire();
      sfpn = g_swapBitMap->Find();

      //find a place for the swap file
      s_MemoryFullLock->Release();
      if(sfpn == -1)
      {
        printf("ERROR: SwapFile full!! Increase NumSwapPages in machine.h\n");
        interrupt->Halt();
        return 0;
      }
      //calculate the size of the page
      swapPage = sfpn*PageSize;
     }
     else
     {
      swapPage = currentThread->space->pageTable[virtualPage].m_byteOffSet;
     }
    s_SwapFileBMLock->Release();

    //here we will write back to the swap file
    g_swapFile->WriteAt(&(machine->mainMemory[ ppn * PageSize ]), PageSize, swapPage);
    //need to update the disklocation and all the stuff to the page table
    currentThread->space->pageTable[ virtualPage ].m_diskLocation = 1;
    currentThread->space->pageTable[ virtualPage ].m_byteOffSet = swapPage;
    currentThread->space->pageTable[ virtualPage ].valid = false;
  }
   
  return ppn;
}

  

int HandleIPTMiss( unsigned int VPN )
{
  int ppn = -1;

  //this will try and find the VPN in the addr sapce page table

  
  ppn = g_pageBitMap->Find();
  //check and see if there are any open places in the IPT 
  //if there is we continue otherwise handle memory eviction
  if(ppn == -1)
  {
    ppn = HandleMemoryFails();
  }



  s_IPTLock->Acquire();
  currentThread->space->InsertEntryForVPN( VPN , ppn );
 
  //depending on where the page is in memroy we will need to read from different places
  if(currentThread->space->pageTable[VPN].m_diskLocation == 0)
  {
    currentThread->space->ReadPageIntoMemory( VPN , ppn, 0); //0 means dont use swap file
  }
  else if (currentThread->space->pageTable[VPN].m_diskLocation == 1)
  {
     currentThread->space->ReadPageIntoMemory( VPN , ppn, 1); //1 means use swap fiel
  }
  //s_IPTLock->Release();
  //return phyiscal page that we found
  return ppn;
}

void PageFaultHandler(unsigned int virtualaddress)
{
    //validate the the address is correct
    if( (virtualaddress % 4) != 0 || virtualaddress < 0)
    {
        printf("Invalid virtual address in page fault handler: %d\n",virtualaddress );
        return;
    }




    int VPN = virtualaddress / PageSize;
    int ppn = -1;
  //  s_pageFaultLock->Acquire();
    s_IPTLock->Acquire();
// we will search and see if the VPN is in the IPT and it is ivalid and the address spaces are correct
    for(int i=0;i< NumPhysPages; i++)
    {

        if(g_ipt->m_entryTable[i]->virtualPage == VPN 
          && g_ipt->m_entryTable[i]->valid 
          && g_ipt->m_entryTable[i]->m_addressSpace == currentThread->space)
        {
           ppn = i;
           break;
        }
        
    }

    
    if( ppn == -1 )
    {
    //  s_pageFaultLock->Release();
       s_IPTLock->Release();
       ppn = HandleIPTMiss( VPN );
     //  s_pageFaultLock->Acquire();
    }

     //prevent overflow
     // if(g_currentTLBIndex > 100000 && g_currentTLBIndex % TLBSize == 0)
     // {
     //    g_currentTLBIndex = 0;
     // }
    
     g_currentTLBIndex = (g_currentTLBIndex + 1 ) % TLBSize; 
      IntStatus oldLevel = interrupt->SetLevel(IntOff);
     //if this page is valid we need to copy over the dirty bit
     if(machine->tlb[g_currentTLBIndex].valid)
     {
       g_ipt->m_entryTable[ machine->tlb[g_currentTLBIndex].physicalPage ]->dirty = machine->tlb[g_currentTLBIndex].dirty;
        
     }
    //now we just put the page in the the TLB from the chosen physical page in the IPT
     machine->tlb[g_currentTLBIndex].virtualPage = g_ipt->m_entryTable[ppn]->virtualPage;

     machine->tlb[g_currentTLBIndex].physicalPage = g_ipt->m_entryTable[ppn]->physicalPage;

     machine->tlb[g_currentTLBIndex].valid = g_ipt->m_entryTable[ppn]->valid;

     machine->tlb[g_currentTLBIndex].readOnly = g_ipt->m_entryTable[ppn]->readOnly;
     machine->tlb[g_currentTLBIndex].use = g_ipt->m_entryTable[ppn]->use;
     machine->tlb[g_currentTLBIndex].dirty = g_ipt->m_entryTable[ppn]->dirty;
   
     s_IPTLock->Release();

     (void) interrupt->SetLevel(oldLevel);
    
    //Add the ppn to the back of the FIFO queue here
    g_ipt->m_table->Append((void*)ppn);  
    g_ipt->m_entryTable[ppn]->use = false;


    

} 

int SendToServer(std::string data)
{
    PacketHeader outPktHdr, inPktHdr;
    MailHeader outMailHdr, inMailHdr;
    char buffer[MaxMailSize];
    int mvID;
    int lckID;
    int cvID;

    int mvVal;
    int valid;
    stringstream ss;
    std::string tempBuffer;
    // construct packet, mail header for original message
    // To: destination machine, mailbox 0
    // From: our machine, reply to: mailbox 1
    outPktHdr.to = 0;   //hard code the server id to be 0
    outMailHdr.to = 0;

    outMailHdr.from = currentThread->space->GetProcessIndex();
    outPktHdr.from = g_clientId;
    outMailHdr.length = data.size() + 1;

    bool success = postOffice->Send(outPktHdr, outMailHdr, const_cast<char*>(data.c_str())); 

    if ( !success ) {
      printf("The postOffice Send failed. You must not have the other Nachos running. Terminating Nachos.\n");
      interrupt->Halt();
    }
   
    postOffice->Receive( currentThread->space->GetProcessIndex() , &inPktHdr, &inMailHdr, buffer);
   // printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
    fflush(stdout);


   
    ss << buffer;
    ss >> tempBuffer;

 
    if(tempBuffer == "CMV")
    {
      ss >> mvID;
      return mvID;
    }

    else if(tempBuffer == "GMV")
    {
      ss >> mvVal;
      return mvVal;
    }

    else if(tempBuffer == "SMV")
    {
      ss >> valid;
      if(valid == -1)
      {
        printf("Set MV failed, invalid index\n");
      }

        return valid;
    }

    else if(tempBuffer == "DMV")
    { 
      ss >>valid;

      if(valid == -1)
      {
        printf("Destroy MV failed, invalid index\n");
        
      }
      return valid;
    }

    else if(tempBuffer == "CLK")
    {
      ss >> lckID;
    //  printf("the lckID returned is %d\n",lckID );
      return lckID;
    }

    else if(tempBuffer == "DLK")
    {
      ss >>valid;
    //  printf("the valid returned is %d\n",valid );
      if(valid == -1)
      {
        printf("Destroy Lock failed, invalid index\n");
      }

      return 0;
    }

    else if(tempBuffer == "ACQ")
    { 
      ss >>valid;
     // printf("the valid returned is %d\n",valid );

      return 0;
    }

    else if(tempBuffer == "REL")
    {
     // printf("Lock release\n");

      return 0;
    }
    else if(tempBuffer == "CCV")
    {
      ss >> cvID;
     // printf("the cvID returned is %d\n",cvID );

      return cvID;
    }
   
    else if(tempBuffer == "DCV")
    {
      ss >>valid;
    //  printf("the valid returned is %d\n",valid );
      if(valid == -1)
      {
        printf("Destroy CV failed, invalid index\n");
        
      }
       return valid;
    }
    
    else if(tempBuffer == "SIG")
    {
      ss >> lckID;
   //   printf("Got signaled %d\n",lckID );

      return lckID;
    }
    else if(tempBuffer == "BCT")
    {
     // printf("Got BCT\n");
      return 0;
    }
    else
    {
      printf("some weird input %s \n", tempBuffer.c_str());
      return 1;
    }
    printf("Going to return -1 here\n");
    return -1;
}



int GetMV_Syscall(int MVindex, int ValIndex)
{

  stringstream ss;
 
  ss << "GMV"<<" "<<MVindex<<" "<<ValIndex;

  int index = SendToServer(ss.str());
  return index; //should return the correct index and then increment

}
//send a MV call
void SetMV_Syscall(int MVindex, int value, int ValIndex)
{
  stringstream ss;
  
  ss << "SMV"<<" "<< MVindex << " " << value << " " << ValIndex;

  int index = SendToServer(ss.str());
}

void DestroyMV_Syscall(int MVindex)
{

  stringstream ss;
  
  ss << "DMV"<<" "<<MVindex;

  int index = SendToServer(ss.str());

}

int  CreateMV_Syscall(int virtualaddress, int len, int ValSize, int initValue)
{
  //validate the data
  char *name = new char[++len];
  if(ValidatedVirtualAddress(virtualaddress, len, name) == -1)
  {
       printf("Root validation was in the Create_Syscall call\n");
      return -1;
  }
  
  stringstream ss;
  
  //create the meesage
  ss << "CMV"<<" "<<name<<" "<<ValSize << " " << initValue;

  int index = SendToServer(ss.str());
  return index; //should return the correct index and then increment


}

int  CreateServerLock_Syscall(int virtualaddress, int len, int appendInt)
{

  char *name = new char[++len];
  if(ValidatedVirtualAddress(virtualaddress, len, name) == -1)
  {
       printf("Root validation was in the Create_Syscall call\n");
      return -1;
  }

  stringstream ss;

  
  ss << "CLK"<<" "<<name << appendInt;


  int index = SendToServer(ss.str());
  return index; //should return the correct index and then increment

}

//create a new CV and send it to the servers
int  CreateServerCV_Syscall(int virtualaddress, int len, int appendInt)
{

  char *name = new char[++len];
  if(ValidatedVirtualAddress(virtualaddress, len, name) == -1)
  {
       printf("Root validation was in the Create_Syscall call\n");
      return -1;
  }

  stringstream ss;
 
 
  ss << "CCV"<<" "<<name << appendInt;
 
  
  int index = SendToServer(ss.str());
  return index; //should return the correct index and then increment

}

//acqure a server lock and send to the servers
void ServerLockAcquire_Syscall(int lockindex)
{
 
  stringstream ss;
  
  ss << "ACQ"<<" "<<lockindex;

  int index = SendToServer(ss.str());
}
//Release a server lock
void ServerLockRelease_Syscall(int lockindex)
{
 
  stringstream ss;
  
  ss << "REL"<<" "<<lockindex;

 int index = SendToServer(ss.str());
}

//destory the locks
void DestroyServerLock_Syscall(int lockindex)
{
 
  stringstream ss;
 
  ss << "DLK"<<" "<<lockindex;

 int index = SendToServer(ss.str());
}




//destroy the CV with tthe index
void DestroyServerCV_Syscall(int cvindex)
{

  stringstream ss;

  ss << "DCV"<<" "<<cvindex;

  int index = SendToServer(ss.str());

}

//signal the server with the condition and lock
void ServerSingal_Syscall(int condition, int lock)
{
  stringstream ss;

  ss << "SIG" << " " << condition << " " << lock;

  int index = SendToServer(ss.str());

} 

//create the syscall for the erver, need to pass in the condition and lock index
void ServerWait_Syscall(int condition, int lock)
{

  stringstream ss;
  ss << "WEI" << " " << condition << " " << lock;

  int index = SendToServer(ss.str());
}


void ExceptionHandler(ExceptionType which) {
    int type = machine->ReadRegister(2); // Which syscall?
    int rv=0;   // the return value from a syscall

    if ( which == SyscallException ) {
  switch (type) {
      default:
                    DEBUG('a', "Unknown syscall - shutting down.\n");
      case SC_Halt:
                    DEBUG('a', "Shutdown, initiated by user program.\n");
                    interrupt->Halt();
                    break;
      case SC_Create:
                    DEBUG('a', "Create syscall.\n");
                    Create_Syscall(machine->ReadRegister(4), machine->ReadRegister(5));
                    break;
      case SC_Open:
                    DEBUG('a', "Open syscall.\n");
                    rv = Open_Syscall(machine->ReadRegister(4), machine->ReadRegister(5));
                    break;
      case SC_Write:
                    DEBUG('a', "Write syscall.\n");
                    Write_Syscall(machine->ReadRegister(4),
                    machine->ReadRegister(5),
                    machine->ReadRegister(6));
                    break;
      case SC_Read:
                    DEBUG('a', "Read syscall.\n");
                    rv = Read_Syscall(machine->ReadRegister(4),
                    machine->ReadRegister(5),
                    machine->ReadRegister(6));
                    break;
      case SC_Close:
                    DEBUG('a', "Close syscall.\n");
                    Close_Syscall(machine->ReadRegister(4));
                    break;
      case SC_Fork:
                    DEBUG('a',"Fork syscall.\n");
                    Fork_Syscall(machine->ReadRegister(4)); // what the fuck do we pass in here
                    break;
      case SC_Exec:
                    DEBUG('a',"Exec syscall.\n");
                    rv = Exec_Syscall(machine->ReadRegister(4),machine->ReadRegister(5));
                     break;
      case SC_Exit:
                     DEBUG('a',"Exit syscall.\n");
                     rv = Exit_Syscall();
                     break;
      case SC_Yield:
                     DEBUG('a',"Yield syscall.\n");
                     Yield_Syscall();
                     break;
      case SC_Release:
                      DEBUG('a',"Release syscall.\n");
                      Release_Syscall(machine->ReadRegister(4));
                      break;
      case SC_Acquire:
                      DEBUG('a',"Aquire syscall.\n");
                      Acquire_Syscall(machine->ReadRegister(4));
                      break;
      case SC_Wait:
                     DEBUG('a',"Wait syscall.\n");
                     Wait_Syscall(machine->ReadRegister(4), machine->ReadRegister(5));
                      break;
      case SC_Signal:
                     DEBUG('a',"Wait syscall.\n");
                     Signal_Syscall(machine->ReadRegister(4), machine->ReadRegister(5));
                     break;
      case SC_Broadcast:
                     DEBUG('a',"Broadcast syscall.\n");
                     Broadcast_Syscall(machine->ReadRegister(4),machine->ReadRegister(5));
                     break;
      case SC_CreateCondition:
                     DEBUG('a',"Create Condition syscall.\n");
                     rv = CreateCondition_Syscall(machine->ReadRegister(4), machine->ReadRegister(5));
                     break;
      case SC_DestroyCondition:
                     DEBUG('a',"Destroy Condition syscall.\n");
                     DestroyCondition_Syscall(machine->ReadRegister(4));
                     break;
      case SC_CreateLock:
                     DEBUG('a',"Destroy Condition syscall.\n");
                     rv = CreateLock_Syscall(machine->ReadRegister(4), machine->ReadRegister(5));
                     break;
     case SC_DestroyLock:
                     DEBUG('a',"Destroy Condition syscall.\n");
                     DestroyLock_Syscall(machine->ReadRegister(4) );
                     break;
      case SC_MyPrint:
                      DEBUG('a',"MyPrint syscall.\n");
                      MyPrint(machine->ReadRegister(4), machine->ReadRegister(5),
                              machine->ReadRegister(6), machine->ReadRegister(7) );
                      break;
      case SC_CreateMV:
                    DEBUG('a',"Create Monitor Variable syscall.\n");
                    rv = CreateMV_Syscall(machine->ReadRegister(4), machine->ReadRegister(5), machine->ReadRegister(6), machine->ReadRegister(7));
                    break;
      case SC_DestroyMV:
                    DEBUG('a',"Create Monitor Variable syscall.\n");
                    DestroyMV_Syscall(machine->ReadRegister(4));
                    break;
      case SC_GetMV:
                    DEBUG('a',"Get Monitor Variable syscall.\n");
                    rv = GetMV_Syscall(machine->ReadRegister(4),machine->ReadRegister(5));
                    break;
      case SC_SetMV:
                    DEBUG('a',"Set Monitor Variable syscall.\n");
                    SetMV_Syscall(machine->ReadRegister(4), machine->ReadRegister(5),machine->ReadRegister(6));
                    break;
      case SC_CreateServerLock:
                    DEBUG('a',"Create Server lock syscall.\n");
                    rv = CreateServerLock_Syscall(machine->ReadRegister(4), machine->ReadRegister(5), machine->ReadRegister(6));
                    break;
      case SC_DestroyServerLock:
                    DEBUG('a',"Destroy Server lock syscall.\n");
                    DestroyServerLock_Syscall(machine->ReadRegister(4));
                    break;
      case SC_CreateServerCV:
                    DEBUG('a',"Create Server CV syscall.\n");
                    rv = CreateServerCV_Syscall(machine->ReadRegister(4), machine->ReadRegister(5), machine->ReadRegister(6));
                    break;
      case SC_DestroyServerCV:
                    DEBUG('a',"Create Server CV syscall.\n");
                    DestroyServerCV_Syscall(machine->ReadRegister(4));
                    break;
      case SC_ServerCVWait:
                    DEBUG('a',"Server Wait syscall.\n");
                    ServerWait_Syscall(machine->ReadRegister(4),machine->ReadRegister(5));
                    break;
      case SC_ServerLockAcquire:
                    DEBUG('a',"Server lock acquire syscall.\n");
                    ServerLockAcquire_Syscall(machine->ReadRegister(4));
                    break;
      case SC_ServerCVBroadcast:
                    DEBUG('a',"Server Broadcast syscall.\n");
                    break;
      case SC_ServerCVSignal:
                    DEBUG('a',"Server signal syscall.\n");
                    ServerSingal_Syscall(machine->ReadRegister(4),machine->ReadRegister(5));
                    break;
      case SC_ServerLockRelease:
                    DEBUG('a',"Server lock release syscall.\n");
                    ServerLockRelease_Syscall(machine->ReadRegister(4));
                    break;
  }

      // Put in the return value and increment the PC
    machine->WriteRegister(2,rv);
    machine->WriteRegister(PrevPCReg,machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg,machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg,machine->ReadRegister(PCReg)+4);
    return;
    }
    else if(which == PageFaultException) 
    {
        //this will kick on the handler
        PageFaultHandler(machine->ReadRegister(39));
    }
    else 
    {
      cout<<"Unexpected user mode exception - which:"<<which<<"  type:"<< type<<endl;
      interrupt->Halt();
    }
}

