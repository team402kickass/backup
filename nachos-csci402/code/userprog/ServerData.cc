#include "ServerData.h"
#include <string.h>
#include <iostream>
#include <stdio.h>

//init the server data
Response::Response()
{
	/*m_ownerMachineID = 0;
	m_ownerMailboxID = 0;

	sendOnce = false;*/
}
ServerData::ServerData()
{
	
	for(int i=0; i< 5000;i++)
	{
		m_cvList[i] = NULL;
		m_mvList[i] = NULL;
		m_lockList[i] = NULL;
	}

	m_lockCount = 0;
	m_cvCount = 0;
	m_mvCount = 0;
}

//find the correct MV
int ServerData::FindMVIndex(std::string name, int valSize, int initValue)
{
	
	if(valSize <= 0)
	{
		return -1;
	}


	for(int i=0; i<m_mvCount; i++)
	{
		if(m_mvList[i]->m_name ==  name)
		{
			return i;
		}
	}
	InsertNewMV( m_mvCount, name, valSize, initValue);
	return m_mvCount++;
}

//make sure the Mv is valid and we have data for it
int ServerData::ValidateMV(int index, int valIndex)
{
	if(index < 0 || index > m_mvCount || m_mvList[index] == NULL)
	{
		printf("This error\n");
		return 0;
	}
	
	else if( m_mvList[index]->m_count < valIndex || valIndex < 0)
	{
		printf("This other error\n");
		return 0;
	}
	else return 1;
}

//see if the client has the lock. use for the wait and signal
bool ServerData::DoesClientOwnLock(int index, int machineID, int mailboxID)
{
	//make sure the incoming message is from the same client that the one is owning it 
	if( m_lockList[index]->m_free)
	{
		printf("Checking owning of a free lock\n");
		return false;
	}
	if(m_lockList[index]->m_ownerMachineID == machineID && m_lockList[index]->m_ownerMailboxID == mailboxID)
	{
			return true;
	}	
	
	printf("Lock owners do not match up\n");
	 return false;
}
//make sure there is valid lock data
int ServerData::ValidateLock(int index)
{
	if(index < 0 || index > m_lockCount || m_lockList[index] == NULL)
	{
		return 0;
	}
	else return 1;
}

//make sure the CV is valid
int ServerData::ValidateCV(int index)
{
	if(index < 0 || index > m_cvCount || m_cvList[index] == NULL)
	{
		printf("!!!!!!!!!!!!!indx: %d, m_cvCount: %d\n",index,m_cvCount );
		return 0;
	}
	else return 1;
}


//find a lock index given a name
int ServerData::FindLockIndex(std::string name)
{
	for(int i=0; i<m_lockCount; i++)
	{
		if(m_lockList[i]->m_name == name)
		{
			return i;
		}
	}
	InsertNewLock(m_lockCount, name);
	return m_lockCount++;
}


//find the condition variable
int ServerData::FindCVIndex(std::string name)
{
	for(int i=0; i<m_cvCount; i++)
	{
		if(m_cvList[i]->m_name ==  name)
		{
			//found an existed CV
			return i;
		}
	}
	InsertNewCV(m_cvCount, name);
	return m_cvCount++;
}

//server MV
void ServerData::InsertNewMV(int index, std::string  name, int size, int initValue)
{
	ServerMV *mv = new ServerMV();
	m_mvList[index] = mv;

	m_mvList[index]->m_name = name;

	m_mvList[index]->m_value = new int[size];
	for(int i=0;i< size;i ++)
	{
		m_mvList[index]->m_value[i] = initValue;
	}
	m_mvList[index]->m_count = size;
	//m_mvList[index]->m_destroyed = 0;
}


//return the MV and make sure it is valid
int ServerData::GetMV(int index, int valIndex)
{
	
	
	return m_mvList[index]->m_value[valIndex];
	
}

//set the member variable with some datas
int ServerData::SetMV(int index, int val, int valIndex)
{
	if(valIndex > (m_mvList[index]->m_count)|| valIndex < 0)
	{
		return -1;
	}
	else
	{
		m_mvList[index]->m_value[valIndex] = val;
		return 1;
	}
}

//delete that MV WOOOO!!!!
void ServerData::DestroyMV(int index)
{
	delete m_mvList[index];
	m_mvList[index] = NULL;

}


//server Lock
void ServerData::InsertNewLock(int index, std::string name)
{
	//all the init data we need for the server
	ServerLock *lck = new ServerLock();
	m_lockList[index] = lck;
	m_lockList[index]->m_name = name;

	m_lockList[index]->m_free = true;
	m_lockList[index]->m_toBeDestroyed = false;
	m_lockList[index]->m_ownerMachineID = -1;
	m_lockList[index]->m_ownerMailboxID = -1;
	m_lockList[index]->m_waitQueue = new List();
}

//destroy said lock which we created above. cool.
void ServerData::DestroyLock(int index)
{
	if( !m_lockList[index]->m_free)
	{
		m_lockList[index]->m_toBeDestroyed = true;
		return;
	}
	delete m_lockList[index]->m_waitQueue;
	delete m_lockList[index];
	m_lockList[index] = NULL;
}


//we will do the necessary checks to do an acquire
bool ServerData::Acquire(int index, std::string msg, int machineID, int mailboxID)

{	
	

	if(m_lockList[index]->m_free)
	{
		//destroy the lock if is marked to be destroyed
		if( m_lockList[index]-> m_toBeDestroyed )
		{
			printf("Destroying lock\n");
			m_lockList[index]->m_free = false;
			delete m_lockList[index]->m_waitQueue;
			delete m_lockList[index];
			m_lockList[index] = NULL;
			return false;
		}

		m_lockList[index]->m_free = false;
		m_lockList[index]->m_ownerMachineID = machineID;
		m_lockList[index]->m_ownerMailboxID = mailboxID;
		return true;
	}
	else
	{ 
		Response *reply = new Response();
		reply->m_msg = msg;
		reply->sendOnce = false;
		reply->m_ownerMachineID = machineID;
		reply->m_ownerMailboxID = mailboxID;
		m_lockList[index]->m_waitQueue->Append(reply);
		return false;
	}
	
}

Response* ServerData::Release(int index, int machineID, int mailboxID)
{
	if( m_lockList[index]->m_ownerMachineID != machineID 
		|| m_lockList[index]->m_ownerMailboxID != mailboxID)
	{
		return NULL;
	}


	if(m_lockList[index]->m_waitQueue->IsEmpty())
	{
		m_lockList[index]->m_free = true;
		return NULL;
	}
	else
	{
		m_lockList[index]->m_free = false;
		Response* reply = (Response *)m_lockList[index]->m_waitQueue->Remove(); 
		m_lockList[index]->m_ownerMachineID = reply->m_ownerMachineID;
		m_lockList[index]->m_ownerMailboxID = reply->m_ownerMailboxID;
		return reply;
	}
}


//get the next client id waiting for a singals
WaitingClient* ServerData::GetWaitingClient(int cvIndex)
{
	if(m_cvList[cvIndex]->m_waitQueue->IsEmpty())
	{
		return NULL;
	}
	return (WaitingClient*)m_cvList[cvIndex]->m_waitQueue->Remove();
}

//add the cleint to the wait queue so they can be singlaedd
void  ServerData::AddToWaitQueue(int cvIndex, int waitingLock, int machineID, int mailboxID)
{
	//if(m_cvList[cvIndex]->m_waitQueue->IsEmpty())
	//{
		m_cvList[cvIndex]->m_waitingLock = waitingLock;
		m_cvList[cvIndex]->m_ownerMailboxID = mailboxID;
		m_cvList[cvIndex]->m_ownerMachineID = machineID;
	///}
	WaitingClient *client = new WaitingClient();
	client->m_ownerMachineID = machineID;
	client->m_ownerMailboxID = mailboxID;
	client->m_waitingLock = waitingLock;
	m_cvList[cvIndex]->m_waitQueue->Append((void*)client);
}

//see if the cv data matches what we go
bool ServerData::DoesLockMatchCV(int cvIndex, int lockIndex)
{
	return m_cvList[cvIndex]->m_waitingLock == lockIndex;
}


//server CV
void ServerData::InsertNewCV(int index, std::string name)
{
	//create a new CV with all the trimmings
	ServerCV *cv = new ServerCV();
	m_cvList[index] = cv;
	m_cvList[index]->m_name  = name;

	m_cvList[index]->m_waitQueue = new List();
	m_cvList[index]->m_waitingLock = -1;
	m_cvList[index]->m_toBeDestroyed = false;
	m_cvList[index]->m_ownerMachineID = -1;
	m_cvList[index]->m_ownerMailboxID = -1;
}

void ServerData::DestroyCV(int index)
{
	delete m_cvList[index];
	m_cvList[index] = NULL;
}
