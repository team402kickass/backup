// addrspace.cc 
//	Routines to manage address spaces (executing user programs).
//
//	In order to run a user program, you must:
//
//	1. link with the -N -T 0 option 
//	2. run coff2noff to convert the object file to Nachos format
//		(Nachos object code format is essentially just a simpler
//		version of the UNIX executable object code format)
//	3. load the NOFF file into the Nachos file system
//		(if you haven't implemented the file system yet, you
//		don't need to do this last step)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "addrspace.h"
#include "noff.h"
#include "table.h"
#include "synch.h"

extern "C" { int bzero(char *, int); };

Table::Table(int s) : map(s), table(0), lock(0), size(s) {
    table = new void *[size];
    lock = new Lock("TableLock");
}

Table::~Table() {
    if (table) {
	delete table;
	table = 0;
    }
    if (lock) {
	delete lock;
	lock = 0;
    }
}

void *Table::Get(int i) {
    // Return the element associated with the given if, or 0 if
    // there is none.

    return (i >=0 && i < size && map.Test(i)) ? table[i] : 0;
}

int Table::Put(void *f) {
    // Put the element in the table and return the slot it used.  Use a
    // lock so 2 files don't get the same space.
    int i;	// to find the next slot

    lock->Acquire();
    i = map.Find();
    lock->Release();
    if ( i != -1)
	table[i] = f;
    return i;
}

void *Table::Remove(int i) {
    // Remove the element associated with identifier i from the table,
    // and return it.

    void *f =0;

    if ( i >= 0 && i < size ) {
	lock->Acquire();
	if ( map.Test(i) ) {
	    map.Clear(i);
	    f = table[i];
	    table[i] = 0;
	}
	lock->Release();
    }
    return f;
}

//----------------------------------------------------------------------
// SwapHeader
// 	Do little endian to big endian conversion on the bytes in the 
//	object file header, in case the file was generated on a little
//	endian machine, and we're now running on a big endian machine.
//----------------------------------------------------------------------

static void 
SwapHeader (NoffHeader *noffH)
{
	noffH->noffMagic = WordToHost(noffH->noffMagic);
	noffH->code.size = WordToHost(noffH->code.size);
	noffH->code.virtualAddr = WordToHost(noffH->code.virtualAddr);
	noffH->code.inFileAddr = WordToHost(noffH->code.inFileAddr);
	noffH->initData.size = WordToHost(noffH->initData.size);
	noffH->initData.virtualAddr = WordToHost(noffH->initData.virtualAddr);
	noffH->initData.inFileAddr = WordToHost(noffH->initData.inFileAddr);
	noffH->uninitData.size = WordToHost(noffH->uninitData.size);
	noffH->uninitData.virtualAddr = WordToHost(noffH->uninitData.virtualAddr);
	noffH->uninitData.inFileAddr = WordToHost(noffH->uninitData.inFileAddr);
}

//----------------------------------------------------------------------
// AddrSpace::AddrSpace
// 	Create an address space to run a user program.
//	Load the program from a file "executable", and set everything
//	up so that we can start executing user instructions.
//
//	Assumes that the object code file is in NOFF format.
//
//	"executable" is the file containing the object code to load into memory
//
//      It's possible to fail to fully construct the address space for
//      several reasons, including being unable to allocate memory,
//      and being unable to read key parts of the executable.
//      Incompletely consretucted address spaces have the member
//      constructed set to false.
//----------------------------------------------------------------------

AddrSpace::AddrSpace(OpenFile *executable, int index) : fileTable(MaxOpenFiles) {
    NoffHeader noffH;
    unsigned int i, size;

    // Don't allocate the input or output to disk files
    fileTable.Put(0);
    fileTable.Put(0);
    m_processIndex = index;

    m_pageTableResizeLock = new Lock("resize page table lock");
    m_executableLock = new Lock("exec lcok");
    m_executable = executable;
    m_pageTableResizeLock->Acquire();
    executable->ReadAt((char *)&noffH, sizeof(noffH), 0);
    if ((noffH.noffMagic != NOFFMAGIC) && 
		(WordToHost(noffH.noffMagic) == NOFFMAGIC))
    	SwapHeader(&noffH);
    ASSERT(noffH.noffMagic == NOFFMAGIC);

    size = noffH.code.size + noffH.initData.size ;
    unsigned int execSize = divRoundUp(size, PageSize);


    size = noffH.code.size + noffH.initData.size + noffH.uninitData.size ;
    numPages = divRoundUp(size, PageSize) + divRoundUp(UserStackSize,PageSize);
                                                // we need to increase the size
						// to leave room for the stack
    size = numPages * PageSize;

  

    DEBUG('a', "Initializing address space, num pages %d, size %d\n", 
					numPages, size);


// first, set up the translation 
    pageTable = new PageTableEntry[numPages];
    for (i = 0; i < numPages; i++) {

        
        //populate the page table
        pageTable[i].virtualPage = i;
    	pageTable[i].valid = FALSE;
    	pageTable[i].use = FALSE;
    	pageTable[i].dirty = FALSE;
    	pageTable[i].readOnly = FALSE;
        if(i <= execSize)
        {
            pageTable[i].m_diskLocation = 0;
        }
        else
        {
            pageTable[i].m_diskLocation = 2;
        }
        //set the offset so we can use it later
         int virtualPageNumber = ( i * PageSize);
         pageTable[i].m_byteOffSet = noffH.code.inFileAddr + virtualPageNumber;

    }
    
     m_pageTableResizeLock->Release();
  
}

//----------------------------------------------------------------------
// AddrSpace::~AddrSpace
//
// 	Dealloate an address space.  release pages, page tables, files
// 	and file tables
//----------------------------------------------------------------------

AddrSpace::~AddrSpace()
{
    delete pageTable;
    delete m_pageTableResizeLock;
}

void AddrSpace::ReadPageIntoMemory(int VPN, int PPN, int swapFile)
{
        //here is where we read in the respective pages from the filese
    m_executableLock->Acquire();
    if(swapFile == 0)
    {
        //load from the exec
         m_executable->ReadAt(&(machine->mainMemory[ PPN * PageSize] ), PageSize , pageTable[VPN].m_byteOffSet ) ;
    }
    else
    {
        //load in the from the swap file
        g_swapFile->ReadAt(&(machine->mainMemory[ PPN * PageSize] ), PageSize , pageTable[VPN].m_byteOffSet ) ;
    }
   
    m_executableLock->Release();
}


void AddrSpace::InsertEntryForVPN(int VPN, int physicalPage)
{
    m_pageTableResizeLock->Acquire();
    g_ipt->InsertNewEntry(this, &pageTable[VPN], physicalPage );
    m_pageTableResizeLock->Release();



}

//// we will resize the page table to accomidate a another thread joining the process
int AddrSpace::ResizePageTable()
{

    m_pageTableResizeLock->Acquire();
    //pageTable = new TranslationEntry[numPages];
    PageTableEntry *newTable = new PageTableEntry[numPages + 8];
    //copy over the new pages
    for(unsigned int i=0;i< numPages; i++)
    {
        newTable[i].virtualPage = pageTable[i].virtualPage;
        newTable[i].physicalPage = pageTable[i].physicalPage;
        newTable[i].valid       = pageTable[i].valid;
        newTable[i].use         = pageTable[i].use;
        newTable[i].dirty       = pageTable[i].dirty;
        newTable[i].readOnly    = pageTable[i].readOnly;
        newTable[i].m_diskLocation = pageTable[i].m_diskLocation;
        newTable[i].m_byteOffSet = pageTable[i].m_byteOffSet;
    }

    for(unsigned int i=numPages;i< numPages + 8; i++)
    {
        

        newTable[i].virtualPage = i;
    
        newTable[i].valid = FALSE;
        newTable[i].use = FALSE;
        newTable[i].dirty = FALSE;
        newTable[i].readOnly = FALSE;
        newTable[i].m_diskLocation = 2;
        newTable[i].m_byteOffSet = -1;
    }

    delete [] pageTable;
    pageTable = newTable;
    numPages += 8;

    //RestoreState();
    //this will get us to the top of the stack register
    m_pageTableResizeLock->Release();
    return ( (numPages*PageSize) - 16 );
}


void AddrSpace::RemovePages(int pageIndex)
{
    m_pageTableResizeLock->Acquire();

    pageIndex += 16;
    pageIndex /= PageSize;
    pageIndex -= 8;
    for(int i = pageIndex; i < (pageIndex + 8); i++)
    {
    //    g_pageBitMap->Clear( pageTable[i].physicalPage );
    }
    m_pageTableResizeLock->Release();
}

//----------------------------------------------------------------------
// AddrSpace::InitRegisters
// 	Set the initial values for the user-level register set.
//
// 	We write these directly into the "machine" registers, so
//	that we can immediately jump to user code.  Note that these
//	will be saved/restored into the currentThread->userRegisters
//	when this thread is context switched out.
//----------------------------------------------------------------------

void
AddrSpace::InitRegisters()
{
    int i;

    for (i = 0; i < NumTotalRegs; i++)
	machine->WriteRegister(i, 0);

    // Initial program counter -- must be location of "Start"
    machine->WriteRegister(PCReg, 0);	

    // Need to also tell MIPS where next instruction is, because
    // of branch delay possibility
    machine->WriteRegister(NextPCReg, 4);

   // Set the stack register to the end of the address space, where we
   // allocated the stack; but subtract off a bit, to make sure we don't
   // accidentally reference off the end!
    machine->WriteRegister(StackReg, numPages * PageSize - 16);
    DEBUG('a', "Initializing stack register to %x\n", numPages * PageSize - 16);
}

//----------------------------------------------------------------------
// AddrSpace::SaveState
// 	On a context switch, save any machine state, specific
//	to this address space, that needs saving.
//
//	For now, nothing!
//----------------------------------------------------------------------

void AddrSpace::SaveState() 
{}

//----------------------------------------------------------------------
// AddrSpace::RestoreState
// 	On a context switch, restore the machine state so that
//	this address space can run.
//
//      For now, tell the machine where to find the page table.
//----------------------------------------------------------------------

void AddrSpace::RestoreState() 
{
    machine->pageTable = NULL;
    //machine->pageTable = pageTable;
    //machine->pageTableSize = numPages;

     
     //use interrupts instead of locks
     IntStatus oldLevel = interrupt->SetLevel(IntOff);
    
     //when we context swithc we need to check if any of the pages are dirct
     //and set them to be dirty in the TLB
  for(int i=0; i < TLBSize; i++)
  {
        if( machine->tlb[i].valid )
        {
             g_ipt->m_entryTable[machine->tlb[i].physicalPage]->dirty = machine->tlb[i].dirty;
        }
       
        machine->tlb[i].valid = false;
  }
    (void) interrupt->SetLevel(oldLevel);
   
}
