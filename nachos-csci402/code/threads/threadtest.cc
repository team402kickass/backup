//  Simple test cases for the threads assignment.
//

#include "copyright.h"
#include "system.h"
#ifdef CHANGED
#include "synch.h"
#include "thread.h"
#include <vector>
#include <queue>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <string>
#endif

#ifdef CHANGED

// --------------------------------------------------
// Test Suite
// --------------------------------------------------


// --------------------------------------------------
// Test 1 - see TestSuite() for details
// --------------------------------------------------
Semaphore t1_s1("t1_s1",0);       // To make sure t1_t1 acquires the
                                  // lock before t1_t2
Semaphore t1_s2("t1_s2",0);       // To make sure t1_t2 Is waiting on the 
                                  // lock before t1_t3 releases it
Semaphore t1_s3("t1_s3",0);       // To make sure t1_t1 does not release the
                                  // lock before t1_t3 tries to acquire it
Semaphore t1_done("t1_done",0);   // So that TestSuite knows when Test 1 is
                                  // done
Lock t1_l1("t1_l1");          // the lock tested in Test 1

// --------------------------------------------------
// t1_t1() -- test1 thread 1
//     This is the rightful lock owner
// --------------------------------------------------
void t1_t1() {
    t1_l1.Acquire();
    t1_s1.V();  // Allow t1_t2 to try to Acquire Lock
 
    printf ("%s: Acquired Lock %s, waiting for t3\n",currentThread->getName(),
        t1_l1.getName());
    t1_s3.P();
    printf ("%s: working in CS\n",currentThread->getName());
    for (int i = 0; i < 1000000; i++) ;
    printf ("%s: Releasing Lock %s\n",currentThread->getName(),
        t1_l1.getName());
    t1_l1.Release();
    t1_done.V();
}

// --------------------------------------------------
// t1_t2() -- test1 thread 2
//     This thread will wait on the held lock.
// --------------------------------------------------
void t1_t2() {

    t1_s1.P();  // Wait until t1 has the lock
    t1_s2.V();  // Let t3 try to acquire the lock

    printf("%s: trying to acquire lock %s\n",currentThread->getName(),
        t1_l1.getName());
    t1_l1.Acquire();

    printf ("%s: Acquired Lock %s, working in CS\n",currentThread->getName(),
        t1_l1.getName());
    for (int i = 0; i < 10; i++)
    ;
    printf ("%s: Releasing Lock %s\n",currentThread->getName(),
        t1_l1.getName());
    t1_l1.Release();
    t1_done.V();
}

// --------------------------------------------------
// t1_t3() -- test1 thread 3
//     This thread will try to release the lock illegally
// --------------------------------------------------
void t1_t3() {

    t1_s2.P();  // Wait until t2 is ready to try to acquire the lock

    t1_s3.V();  // Let t1 do it's stuff
    for ( int i = 0; i < 3; i++ ) {
    printf("%s: Trying to release Lock %s\n",currentThread->getName(),
           t1_l1.getName());
    t1_l1.Release();
    }
}

// --------------------------------------------------
// Test 2 - see TestSuite() for details
// --------------------------------------------------
Lock t2_l1("t2_l1");        // For mutual exclusion
Condition t2_c1("t2_c1");   // The condition variable to test
Semaphore t2_s1("t2_s1",0); // To ensure the Signal comes before the wait
Semaphore t2_done("t2_done",0);     // So that TestSuite knows when Test 2 is
                                  // done

// --------------------------------------------------
// t2_t1() -- test 2 thread 1
//     This thread will signal a variable with nothing waiting
// --------------------------------------------------
void t2_t1() {
    t2_l1.Acquire();
    printf("%s: Lock %s acquired, signalling %s\n",currentThread->getName(),
       t2_l1.getName(), t2_c1.getName());
    t2_c1.Signal(&t2_l1);
    printf("%s: Releasing Lock %s\n",currentThread->getName(),
       t2_l1.getName());
    t2_l1.Release();
    t2_s1.V();  // release t2_t2
    t2_done.V();
}

// --------------------------------------------------
// t2_t2() -- test 2 thread 2
//     This thread will wait on a pre-signalled variable
// --------------------------------------------------
void t2_t2() {
    t2_s1.P();  // Wait for t2_t1 to be done with the lock
    t2_l1.Acquire();
    printf("%s: Lock %s acquired, waiting on %s\n",currentThread->getName(),
       t2_l1.getName(), t2_c1.getName());
    t2_c1.Wait(&t2_l1);
    printf("%s: Releasing Lock %s\n",currentThread->getName(),
       t2_l1.getName());
    t2_l1.Release();
}
// --------------------------------------------------
// Test 3 - see TestSuite() for details
// --------------------------------------------------
Lock t3_l1("t3_l1");        // For mutual exclusion
Condition t3_c1("t3_c1");   // The condition variable to test
Semaphore t3_s1("t3_s1",0); // To ensure the Signal comes before the wait
Semaphore t3_done("t3_done",0); // So that TestSuite knows when Test 3 is
                                // done

// --------------------------------------------------
// t3_waiter()
//     These threads will wait on the t3_c1 condition variable.  Only
//     one t3_waiter will be released
// --------------------------------------------------
void t3_waiter() {
    t3_l1.Acquire();
    t3_s1.V();      // Let the signaller know we're ready to wait
    printf("%s: Lock %s acquired, waiting on %s\n",currentThread->getName(),
       t3_l1.getName(), t3_c1.getName());
    t3_c1.Wait(&t3_l1);
    printf("%s: freed from %s\n",currentThread->getName(), t3_c1.getName());
    t3_l1.Release();
    t3_done.V();
}


// --------------------------------------------------
// t3_signaller()
//     This threads will signal the t3_c1 condition variable.  Only
//     one t3_signaller will be released
// --------------------------------------------------
void t3_signaller() {

    // Don't signal until someone's waiting
    
    for ( int i = 0; i < 5 ; i++ ) 
    t3_s1.P();
    t3_l1.Acquire();
    printf("%s: Lock %s acquired, signalling %s\n",currentThread->getName(),
       t3_l1.getName(), t3_c1.getName());
    t3_c1.Signal(&t3_l1);
    printf("%s: Releasing %s\n",currentThread->getName(), t3_l1.getName());
    t3_l1.Release();
    t3_done.V();
}
 
// --------------------------------------------------
// Test 4 - see TestSuite() for details
// --------------------------------------------------
Lock t4_l1("t4_l1");        // For mutual exclusion
Condition t4_c1("t4_c1");   // The condition variable to test
Semaphore t4_s1("t4_s1",0); // To ensure the Signal comes before the wait
Semaphore t4_done("t4_done",0); // So that TestSuite knows when Test 4 is
                                // done

// --------------------------------------------------
// t4_waiter()
//     These threads will wait on the t4_c1 condition variable.  All
//     t4_waiters will be released
// --------------------------------------------------
void t4_waiter() {
    t4_l1.Acquire();
    t4_s1.V();      // Let the signaller know we're ready to wait
    printf("%s: Lock %s acquired, waiting on %s\n",currentThread->getName(),
       t4_l1.getName(), t4_c1.getName());
    t4_c1.Wait(&t4_l1);
    printf("%s: freed from %s\n",currentThread->getName(), t4_c1.getName());
    t4_l1.Release();
    t4_done.V();
}


// --------------------------------------------------
// t2_signaller()
//     This thread will broadcast to the t4_c1 condition variable.
//     All t4_waiters will be released
// --------------------------------------------------
void t4_signaller() {

    // Don't broadcast until someone's waiting
    
    for ( int i = 0; i < 5 ; i++ ) 
    t4_s1.P();
    t4_l1.Acquire();
    printf("%s: Lock %s acquired, broadcasting %s\n",currentThread->getName(),
       t4_l1.getName(), t4_c1.getName());
    t4_c1.Broadcast(&t4_l1);
    printf("%s: Releasing %s\n",currentThread->getName(), t4_l1.getName());
    t4_l1.Release();
    t4_done.V();
}
// --------------------------------------------------
// Test 5 - see TestSuite() for details
// --------------------------------------------------
Lock t5_l1("t5_l1");        // For mutual exclusion
Lock t5_l2("t5_l2");        // Second lock for the bad behavior
Condition t5_c1("t5_c1");   // The condition variable to test
Semaphore t5_s1("t5_s1",0); // To make sure t5_t2 acquires the lock after
                                // t5_t1

// --------------------------------------------------
// t5_t1() -- test 5 thread 1
//     This thread will wait on a condition under t5_l1
// --------------------------------------------------
void t5_t1() {
    t5_l1.Acquire();
    t5_s1.V();  // release t5_t2
    printf("%s: Lock %s acquired, waiting on %s\n",currentThread->getName(),
       t5_l1.getName(), t5_c1.getName());
    t5_c1.Wait(&t5_l1);
    printf("%s: Releasing Lock %s\n",currentThread->getName(),
       t5_l1.getName());
    t5_l1.Release();
}

// --------------------------------------------------
// t5_t1() -- test 5 thread 1
//     This thread will wait on a t5_c1 condition under t5_l2, which is
//     a Fatal error
// --------------------------------------------------
void t5_t2() {
    t5_s1.P();  // Wait for t5_t1 to get into the monitor
    t5_l1.Acquire();
    t5_l2.Acquire();
    printf("%s: Lock %s acquired, signalling %s\n",currentThread->getName(),
       t5_l2.getName(), t5_c1.getName());
    t5_c1.Signal(&t5_l2);
    printf("%s: Releasing Lock %s\n",currentThread->getName(),
       t5_l2.getName());
    t5_l2.Release();
    printf("%s: Releasing Lock %s\n",currentThread->getName(),
       t5_l1.getName());
    t5_l1.Release();
}

// --------------------------------------------------
// TestSuite()
//     This is the main thread of the test suite.  It runs the
//     following tests:
//
//       1.  Show that a thread trying to release a lock it does not
//       hold does not work
//
//       2.  Show that Signals are not stored -- a Signal with no
//       thread waiting is ignored
//
//       3.  Show that Signal only wakes 1 thread
//
//   4.  Show that Broadcast wakes all waiting threads
//
//       5.  Show that Signalling a thread waiting under one lock
//       while holding another is a Fatal error
//
//     Fatal errors terminate the thread in question.
// --------------------------------------------------
void TestSuite() {
    Thread *t;
    char *name;
    int i;
    
    // Test 1

    printf("Starting Test 1\n");

    t = new Thread("t1_t1");
    t->Fork((VoidFunctionPtr)t1_t1,0);

    t = new Thread("t1_t2");
    t->Fork((VoidFunctionPtr)t1_t2,0);

    t = new Thread("t1_t3");
    t->Fork((VoidFunctionPtr)t1_t3,0);

    // Wait for Test 1 to complete
    for (  i = 0; i < 2; i++ )
    t1_done.P();

    // Test 2

    printf("Starting Test 2.  Note that it is an error if thread t2_t2\n");
    printf("completes\n");

    t = new Thread("t2_t1");
    t->Fork((VoidFunctionPtr)t2_t1,0);

    t = new Thread("t2_t2");
    t->Fork((VoidFunctionPtr)t2_t2,0);

    // Wait for Test 2 to complete
    t2_done.P();

    // Test 3

    printf("Starting Test 3\n");

    for (  i = 0 ; i < 5 ; i++ ) {
    name = new char [20];
    sprintf(name,"t3_waiter%d",i);
    t = new Thread(name);
    t->Fork((VoidFunctionPtr)t3_waiter,0);
    }
    t = new Thread("t3_signaller");
    t->Fork((VoidFunctionPtr)t3_signaller,0);

    // Wait for Test 3 to complete
    for (  i = 0; i < 2; i++ )
    t3_done.P();

    // Test 4

    printf("Starting Test 4\n");

    for (  i = 0 ; i < 5 ; i++ ) {
    name = new char [20];
    sprintf(name,"t4_waiter%d",i);
    t = new Thread(name);
    t->Fork((VoidFunctionPtr)t4_waiter,0);
    }
    t = new Thread("t4_signaller");
    t->Fork((VoidFunctionPtr)t4_signaller,0);

    // Wait for Test 4 to complete
    for (  i = 0; i < 6; i++ )
    t4_done.P();

    // Test 5

    printf("Starting Test 5.  Note that it is an error if thread t5_t1\n");
    printf("completes\n");

    t = new Thread("t5_t1");
    t->Fork((VoidFunctionPtr)t5_t1,0);

    t = new Thread("t5_t2");
    t->Fork((VoidFunctionPtr)t5_t2,0);

}

































const int MAX = 5;
int g_NumberBeingUsed = 0;
int g_patientCount = 0;



//variables for receptionists
Lock* g_recepLock[MAX];
Lock *g_recepLineLock;
int g_recepLineCount[MAX] = {0,0,0,0,0};
Condition *g_recepLineCV[MAX];
Condition *g_recepPatientCV[MAX];
int g_recepStatus[MAX] = {1,1,1,1,1}; // 0 is available, 1 = busy, 2 is on break
int g_receptoken = 0;
int g_recepToken[MAX] = {-1,-1,-1,-1,-1};
Lock *g_receptokenLock;

//variables for doorboy
int g_roomNumber = -1;
Lock *g_doorBoyLock[MAX];
Condition *g_doorBoyPatientCV[MAX];
//pair, waiting for doctor to alert us


//Doctor line
int g_doorBoyDoctorLine[MAX] = {0,0,0,0,0};
Condition *g_doorboyDoctorWaitLineCV[MAX];
Lock *g_doorBoyDoctorLineLock;



int g_doorBoyLine[MAX] = {0,0,0,0,0};
Lock * g_doctorRoomLock;
int g_doctorRoom[MAX] = {-1,-1,-1,-1,-1};

//pair
Lock *g_dbBreakLock;
Condition *g_dbOnBreak[MAX];

//pair waiting for patients
Lock *g_doorBoyWaitingForPatientLock;
Condition *g_doorBoyWaitingForPatientCV[MAX];

//pair
Condition *g_doorboyWaitLineCV[MAX];
Lock *g_doorBoyLineLock;

//pair 
Lock *g_doorBoyStateLock;
int g_DoorBoyState[MAX] = {1,1,1,1,1};

////////////////////////////////////////

//variables for doctor
Lock *g_pDoctorLock;    //patient doctor interaction
Lock *g_cashDoctorLock[MAX];    //doctor cashier interaction
Condition *g_pDocCV[MAX];
int g_DoctorState[MAX] = {1,1,1,1,1};
int g_currentPatient[MAX] = {-1, -1, -1, -1, -1};    //Stores token number of current patient for each doctor
enum g_diagnosis {NONE, FLU, COLD, FOOD_PSNG, STREP};
enum g_medicine {None, Ibuprofen, Antibiotic};
g_medicine medicine[MAX] = {None, None, None, None, None};


//variables for cashiers
Lock *g_cashLock[MAX];
Lock *g_cashierLineLock;
int g_cashLineCount[MAX] = {0,0,0,0,0};
Condition *g_cashLineCV[MAX];
Condition *g_cashPatientCV[MAX];
int g_cashStatus[MAX] = {1,1,1,1,1};
int g_cashPayment = 0;
int g_cashPayments[MAX] = {0,0,0,0,0};
Lock *g_cashtokenLock;
std::vector<int> g_cashierPaymentRecords;



//variables for pharmacy clerk
Lock *g_pharmLock[MAX];
Lock *g_pharmLineLock;
int g_pharmLineCount[MAX] = {0,0,0,0,0};
Condition *g_pharmLineCV[MAX];
Condition *g_pharmPatientCV[MAX];
int g_pharmStatus[MAX] = {1,1,1,1,1};

int g_pharmPayment = 0;
int g_pharmPayments[MAX] = {0,0,0,0,0};
Lock *g_pharmtokenLock;
std::vector<int> g_pharmPaymentRecords;


void HelperPrint(std::string who, std::string action, int index)
{
    std::cout << who << ": " << index << " " << action << std::endl;
}

void HelperPrint2(std::string who, std::string action, int index, int num)
{
    std::cout << who << ": " << index << " " << action <<  " " << num << std::endl;
}



void hospitalManager(int index)
{
    //we are checking up on the pharmacist and seeing if any of them are being lazy mofos
    //pharmacist alert
    const std::string manager = "Manager";
    while(true)
    {
        //HelperPrint(manager,"manager gunna manage",1);
       //  std::cout << "Made it here 11" << std::endl;
        g_pharmLineLock->Acquire();
        for(int i=0;i<g_NumberBeingUsed ;i++)
        {
            if(g_pharmLineCount[i] > 0 && g_pharmStatus[i] == 2)
            {
                HelperPrint(manager,"alerting a lazy pharmacist",1);
                //we need to signal them to go back to workkkkk, them lazy bitch
            }

        }
        g_pharmLineLock->Release();

     ///   std::cout << "Made it here " << std::endl;
        g_cashierLineLock->Acquire(); //we are going to check and see if anyone is waiting for us
        for(int i=0;i<g_NumberBeingUsed ;i++)
        {
            if(g_cashLineCount[i] > 0 && g_cashStatus[i] == 2)
            {
                HelperPrint(manager,"alerting a lazy cashier",1);
                //we need to signal them to go back to workkkkk, them lazy bitch
            }
        }
        g_cashierLineLock->Release();

     //   std::cout << "Made it here 222" << std::endl;

        //receptionist alert
        g_recepLineLock->Acquire(); //go in to critical section for the receptionist
        for(int i=0;i<g_NumberBeingUsed ;i++)
        {
            if(g_recepLineCount[i] > 0 && g_recepStatus[i] == 2)
            {
                HelperPrint(manager,"alerting a lazy receptionist",1);
                //we need to signal them to go back to workkkkk, them lazy bitch
            }
        }
        g_recepLineLock->Release();
     //    
        //door boy alert

        g_doorBoyStateLock -> Acquire();
        for(int i=0;i<g_NumberBeingUsed ;i++)
        {
            //not sure what is going on with all the line bullshit but yeah, need to do this 
            if(g_DoorBoyState[i] == 2 && g_doorBoyDoctorLine[i] > 0)
            {
                HelperPrint(manager,"alerting a lazy doorboy",1);
                g_DoorBoyState[i] = 1;
                std::cout << "DOOOR setting state in manager" << std::endl;
                g_dbOnBreak[i]->Signal(g_dbBreakLock);
                break;
                //we need to alert 
            }
        }
        g_doorBoyStateLock -> Release();

     //    std::cout << "Made it here444 222" << std::endl;

        srand (time(NULL));
        int paymentRequest = rand() % 100;
        //pharmacy payment 
        if(paymentRequest < 20)
        {
            int count = 0;
            g_pharmtokenLock->Acquire();
            for(unsigned int i=0;i<g_pharmPaymentRecords.size();i++)
            {
                count += g_pharmPaymentRecords[i];
            }
            g_pharmtokenLock->Release();

        //    std::cout << "The pharmacy total is: " << count << std::endl;
        }

        paymentRequest = rand() % 100;
        //this is the cashier total
        if(paymentRequest < 20)
        {
            int count = 0;
            g_cashtokenLock->Acquire();
            for(unsigned int i=0;i<g_cashierPaymentRecords.size();i++)
            {
                count += g_cashierPaymentRecords[i];
            }
            g_cashtokenLock->Release();
       //     std::cout << "The cashier total is: " << count << std::endl;
        }



        //we will now wait
        int yieldTimes = rand() % 1000 + 1000;

        for(int i=0;i<yieldTimes;i++)
        {
            currentThread->Yield();
        }
        
    }
}



void patient(int index)
{   
    const std::string patient = "Patients";

        g_recepLineLock->Acquire(); //go in to critical section for the patient wait in line
        HelperPrint(patient, "has arrived at the hospital",index);
        int p_shortestLine = g_recepLineCount[0]; // assume the shortest line is the first line
        int p_lineIndex = 0; // init the line index to be 0
        for(int i=0; i<g_NumberBeingUsed; i++)
        {
            if(g_recepLineCount[i] < p_shortestLine)
            {
                p_lineIndex = i; // set the line index to be the shortest line
                p_shortestLine = g_recepLineCount[i]; // set the shortest line to be the one we found
            }

            if(g_recepStatus[i] == 0)
            {
                g_recepStatus[i] = 1; // set the receptionist to be busy once we found an available
                p_lineIndex = i; // set the line index to bethe one we found
                p_shortestLine = -1; // set shortest line to be -1 for later use
                break;
            }
        }

        if(p_shortestLine > -1)
        {
        // all receptionists are busy, wait in line
            g_recepLineCount[p_lineIndex]++; // increment the count of that line
            HelperPrint2(patient, " is waiting on Receptionist ",index,p_lineIndex);
            g_recepLineCV[p_lineIndex] -> Wait(g_recepLineLock); //wait for the receptionist call my name
            g_recepLineCount[p_lineIndex]--; //decrement the count of that line since I'm called
             // allow the next patient or receptionist to enter the critical section
        }
        g_recepStatus[p_lineIndex] = 1;
        g_recepLock[p_lineIndex] -> Acquire(); // enter the critical section of patient-receptionist interaction
        g_recepLineLock -> Release(); //exit the critical section of wait in line

        g_recepPatientCV[p_lineIndex] -> Signal(g_recepLock[p_lineIndex]); // signal the receptionist in my line
     //   HelperPrint(patient, "signal the receptionist in my line",index);
      //  HelperPrint(patient, "wait for the receptionist give my token",index);
        g_recepPatientCV[p_lineIndex] -> Wait(g_recepLock[p_lineIndex]); // wait for the receptionist give me my token
        int p_myToken = g_recepToken[p_lineIndex]; // receieve my unique token
        std::cout << "Patient: " << index << "has received Token:" << p_myToken << "from Receptionist: " << p_lineIndex << std::endl;
     //   HelperPrint(patient, "signal receptionist I received the token",index);
        g_recepPatientCV[p_lineIndex] -> Signal(g_recepLock[p_lineIndex]); // signal receptionist that I received my token
        g_recepLock[p_lineIndex] -> Release(); // exit the patient-receptionist interaction
        


         //DOOR BOY INTERACTION
        g_doorBoyLineLock->Acquire();
        HelperPrint(patient, "acquired door boy line lock",index);

        p_shortestLine = g_doorBoyLine[0]; // assume the shortest line is the first line
        p_lineIndex = 0; // init the line index to be 0
        for(int i=0; i<g_NumberBeingUsed; i++)
        {
             g_doorBoyStateLock->Acquire();
            if(g_doorBoyLine[i] < p_shortestLine)
            {
                p_lineIndex = i; // set the line index to be the shortest line
                p_shortestLine = g_doorBoyLine[i]; // set the shortest line to be the one we found
            }

            if(g_DoorBoyState[i] == 3)
            {
                g_DoorBoyState[i] = 1; // set the receptionist to be busy once we found an available
                p_lineIndex = i; // set the line index to bethe one we found
                p_shortestLine = -1; // set shortest line to be -1 for later use
                break;
            }
            g_doorBoyStateLock->Release();
        }

        if(p_shortestLine > -1)
        {
            g_doorBoyLine[p_lineIndex]++; // increment the count of that line
            HelperPrint2(patient, " is waiting on Door boy ",index, p_lineIndex);
            g_doorboyWaitLineCV[p_lineIndex] -> Wait(g_doorBoyLineLock); //wait for the receptionist call my name
            g_doorBoyLine[p_lineIndex]--; //decrement the count of that line since I'm called
                // allow the next patient or receptionist to enter the critical section
            g_doorBoyLock[p_lineIndex]->Acquire();
        }
        else
        {
            g_doorBoyWaitingForPatientLock->Acquire();
           
            g_doorBoyWaitingForPatientCV[p_lineIndex]->Signal(g_doorBoyWaitingForPatientLock);

            g_doorBoyLock[p_lineIndex]->Acquire();
            g_doorBoyWaitingForPatientLock->Release();

        }
        
      //  g_doorBoyPatientCV[p_lineIndex]->Wait( g_doorBoyLock[p_lineIndex] );
        g_doorBoyLineLock->Release();
        HelperPrint(patient, "was signlaed by a doorboy",index);
        g_doorBoyPatientCV[p_lineIndex]->Signal(g_doorBoyLock[p_lineIndex]);
       // HelperPrint(patient, "waiting for room number",index);
        g_doorBoyPatientCV[p_lineIndex]->Wait(g_doorBoyLock[p_lineIndex]);
        g_doctorRoomLock->Acquire();
        int p_myRoom = g_roomNumber;
        std::cout << "Patient: " << index << " has been told by DoorBoy:" << p_lineIndex << " to go to Examining Room: " << p_myRoom << std::endl;
        g_roomNumber = -1;
        g_doctorRoomLock->Release();
       // HelperPrint(patient, "got the room number",index);
        g_doorBoyPatientCV[p_lineIndex]->Signal(g_doorBoyLock[p_lineIndex]);
        HelperPrint2(patient, " is going to Examining Room ",index, p_myRoom);
        g_doorBoyLock[p_lineIndex]->Release();





        //interacting with doctor
        g_pDoctorLock->Acquire();
        g_pDocCV[p_myRoom]->Signal(g_pDoctorLock); 
        g_currentPatient[p_myRoom] = p_myToken; // tell doctor my token #
        HelperPrint2(patient, " is waiting to be examined by the Doctor in ExaminingRoom ",index, p_myRoom);
        g_pDocCV[p_myRoom]->Wait(g_pDoctorLock);
        g_medicine p_medicine = medicine[p_myRoom]; // take prescription from doctor

        medicine[p_myRoom] = None;
        g_pDocCV[p_myRoom]->Signal(g_pDoctorLock);
        HelperPrint2(patient, "Thank you for the prescription. Medicine: ", index,p_medicine);
        g_pDocCV[p_myRoom]->Wait(g_pDoctorLock);
        g_pDocCV[p_myRoom]->Signal(g_pDoctorLock);
        HelperPrint2(patient, "Leaving doctor", index, p_myRoom);
        g_pDoctorLock->Release();

        //interacting with cashier
        g_cashierLineLock -> Acquire(); // enters the critical section of waititing in line
        p_shortestLine = g_cashLineCount[0]; // assume the shortest line is the first line
        p_lineIndex = 0; // init the line index to be 0
        for(int i=0; i<g_NumberBeingUsed; i++)
        {
            if(g_cashLineCount[i]<p_shortestLine)
            {
                p_lineIndex = i; //set the line index to be the shortest line
                p_shortestLine = g_cashLineCount[i]; // set the shortest line to be the one we found
            }
            if(g_cashStatus[i] == 0)
            {
                g_cashStatus[i] = 1; //set the cashier to be busy once we found an available
                p_lineIndex = i; // set the line index to be the one we found
                p_shortestLine = -1; // set shortest line to be -1 for later use
                break;
            }
        }

        if (p_shortestLine > -1)
        {
            // all cashiers are busy, wait in line
            g_cashLineCount[p_lineIndex]++; // increment the count of that line
            HelperPrint2(patient, "wait for the cashier call my name: Cashier: ",index, p_lineIndex);
            g_cashLineCV[p_lineIndex] -> Wait(g_cashierLineLock); // wait for the cashier call my name
        }
            g_cashStatus[p_lineIndex] = 1;
            g_cashLineCount[p_lineIndex]--; //decrement the count of that line since I'm called
            g_cashLock[p_lineIndex] -> Acquire();
            g_cashierLineLock -> Release(); // allow the next interaction go on
        
            //HelperPrint(patient, "signal the cashier in my line",index);
            g_cashPatientCV[p_lineIndex] -> Signal(g_cashLock[p_lineIndex]); // ask cashier about the consultancy fee
            // = p_myToken; // give my token number to cashier
         //   HelperPrint(patient, "wait for the cashier to give my bill statement",index);

            g_cashPatientCV[p_lineIndex] -> Wait(g_cashLock[p_lineIndex]); // wait for the cashier to tell me how much
            HelperPrint(patient, "signal the cashier to pay the bill",index);
            g_cashPatientCV[p_lineIndex] -> Signal(g_cashLock[p_lineIndex]); // it is chill
            HelperPrint2(patient, "leaving the: Cashier: ",index, p_lineIndex);
            g_cashLock[p_lineIndex] -> Release(); // exits the interaction between patient and 

            //interacting with pharamacy clerk
            g_pharmLineLock->Acquire(); // enters the critical section waiting in line
            p_shortestLine = g_cashLineCount[0]; // assume the shortest line is the first line
            p_lineIndex = 0; // init the line index to be 0
            for(int i=0; i<g_NumberBeingUsed; i++)
            {
                if(g_pharmLineCount[i]<p_shortestLine)
                {
                p_lineIndex = i; //set the line index to be the shortest line
                p_shortestLine = g_pharmLineCount[i]; // set the shortest line to be the one we found
                }
                if(g_pharmStatus[i] == 0)
                {
                g_pharmStatus[i] = 1; //set the clerk to be busy once we found an available
                p_lineIndex = i; // set the line index to be the one we found
                p_shortestLine = -1; // set shortest line to be -1 for later use
                break;
                }
            }

            if (p_shortestLine > -1)
            {
               //all clerks are busy, wait in line
                g_pharmLineCount[p_lineIndex]++; //increment the count of that line
                HelperPrint2(patient, "wait for the cashier call my name: Pharmacist: ",index, p_lineIndex);
                g_pharmLineCV[p_lineIndex] -> Wait(g_pharmLineLock); // wait for the clerk call my name
             }   
                g_pharmStatus[p_lineIndex] = 1;
                g_pharmLock[p_lineIndex] -> Acquire(); // enter the interaction between patient and pharmacy
                g_pharmLineLock -> Release(); // finish waiting in line
                 g_pharmLineCount[p_lineIndex]--; //decrement the count of that line
                //HelperPrint(patient, "signal the pharmacist in my line",index);
                g_pharmPatientCV[p_lineIndex] -> Signal(g_pharmLock[p_lineIndex]); // signal the pharmacy in my line
                //give out my token
                //HelperPrint(patient, "wait for the clerk give me my prescription",index);
                g_pharmPatientCV[p_lineIndex] -> Wait(g_pharmLock[p_lineIndex]); // wait for the clerk respond
                //HelperPrint(patient, "signal the clerk I got my meds",index);
                g_pharmPatientCV[p_lineIndex] -> Signal(g_pharmLock[p_lineIndex]); // signal that I got my meds
              //  HelperPrint(patient, "wait for the clerk to tell me the price",index);
                g_pharmPatientCV[p_lineIndex] -> Wait(g_pharmLock[p_lineIndex]); // wait for the clerk to tell the price
                
                HelperPrint(patient, "signal the clerk to pay the bill",index);
                g_pharmPatientCV[p_lineIndex] -> Signal(g_pharmLock[p_lineIndex]); // signal that I pay the bill
                
                HelperPrint(patient, "wait the clerk to recieve my payment",index);
                g_pharmPatientCV[p_lineIndex] -> Wait(g_pharmLock[p_lineIndex]); // wait the clerk to recieve the bill
                HelperPrint(patient, "signal the clerk I'm leaving",index);
                g_pharmPatientCV[p_lineIndex] -> Signal(g_pharmLock[p_lineIndex]); // signal the clerk that I leave
                g_pharmLock[p_lineIndex] -> Release(); // exits the patient-pharmacist interaction
                //leave the hospital

                HelperPrint(patient, "Leaving the hospital",index);
        
        
}



void receptionist( int index )
{
    const std::string recep = "Receptionist";
    while(true)
    {
        g_recepLineLock->Acquire(); //go in to critical section for the receptionist
       // HelperPrint(recep ,"aquired lock",index);
        g_recepStatus[index] = 0;  //set our status to available
        if(g_recepLineCount[index] > 0)//check if anyone is in line
        {
            //people are in line
           // HelperPrint(recep ,"alerting customer in line",index);
            g_recepStatus[index] = 1; 
            g_recepLineCV[index]->Signal(g_recepLineLock); //signal the person waiting that we are good to go
            //set recep status to busy
        }
        g_recepLock[index]->Acquire(); //getting ready for the next critical section for the line
        g_recepLineLock->Release();    //release the line lock critical section
        
        HelperPrint(recep ,"has signaled a patient",index);
       // HelperPrint(recep ,"waiting for patient to alert",index);
        g_recepPatientCV[index]->Wait( g_recepLock[index] ); //we will wait till the customer has arrived and has signaled us
        //HelperPrint(recep ,"customer arrived",index);
        g_receptokenLock->Acquire(); //aquire the lock for the critical secton for the tokens
        g_recepToken[index] = g_receptoken; //do all thefun token stuff
        g_receptoken++;
        std::cout << "Receptionist: "<< index << " gives Token: " << g_receptoken << " to a Patient" << std::endl;
       // HelperPrint(recep ,"putting token on table",index);
        g_receptokenLock->Release(); //release that token

       // HelperPrint(recep ,"tellign patient token is on the table",index);
        g_recepPatientCV[index]->Signal( g_recepLock[index] ); // we will now signal the waiting patient that we have done what we needed to do
        g_recepPatientCV[index]->Wait( g_recepLock[index] ); //waiting for the patient to tell us that they picked up the token
      //  HelperPrint(recep ,"patient grabbed the token",index);
        g_recepLock[index]->Release(); //exit the critical section

    }
}



void doorboy(int index)
{
    const std::string doorboy = "DoorBoy";
    while(true)
    {

        g_doorBoyDoctorLineLock->Acquire();

        g_doorBoyStateLock->Acquire();
        g_DoorBoyState[index] = 0;
        g_doorBoyStateLock->Release();
        if(g_doorBoyDoctorLine[index] > 0)
        {
            //HelperPrint(doorboy, "Found waiting doctors and signaling them",index);
            g_doorboyDoctorWaitLineCV[index ] -> Signal(g_doorBoyDoctorLineLock);
            g_doorBoyStateLock->Acquire();
            g_DoorBoyState[index] = 1;
            g_doorBoyStateLock->Release();
        }
        else
        {
            g_dbBreakLock->Acquire();
            g_doorBoyDoctorLineLock->Release();

            g_doorBoyStateLock->Acquire();
            g_DoorBoyState[index] = 2;
            g_doorBoyStateLock->Release();

            HelperPrint(doorboy, "is waiting for a doctor",index);
            g_dbOnBreak[index]->Wait(g_dbBreakLock);
            HelperPrint(doorboy, "is comign off break",index);
            g_doorBoyDoctorLineLock->Acquire();
            g_doorboyDoctorWaitLineCV[index]->Signal(g_doorBoyDoctorLineLock);
            g_dbBreakLock->Release();
        }   

        g_doorboyDoctorWaitLineCV[index]->Wait(g_doorBoyDoctorLineLock);

        g_doctorRoomLock->Acquire();
        int roomNumber = g_doctorRoom[index]; // tell the doorboy my room #
        g_doctorRoomLock->Release();
        std::cout << "DoorBoy:" << index << " has been told by Doctor:" << roomNumber << " to bring a Patient" <<  std::endl;
        g_doorboyDoctorWaitLineCV[index]->Signal(g_doorBoyDoctorLineLock);
        g_doorBoyDoctorLineLock->Release();
            

        //paitent section
        g_doorBoyLineLock->Acquire();

        if(g_doorBoyLine[index] > 0 )
        {
            ///means there is a patient waiting for us
            HelperPrint(doorboy, "signaled the patient",index);
            g_doorboyWaitLineCV[index]->Signal(g_doorBoyLineLock);

             g_doorBoyLock[index]->Acquire();
             g_doorBoyLineLock->Release();
        }
        else
        {
            //wait here till a patient says what the fuck is up
            g_doorBoyWaitingForPatientLock->Acquire();
            g_doorBoyLineLock->Release();
             
            g_doorBoyStateLock->Acquire();
            g_DoorBoyState[index] = 3;
            g_doorBoyStateLock->Release();

             HelperPrint(doorboy, "is going on break because there are no Patients",index);
            g_doorBoyWaitingForPatientCV[index]->Wait(g_doorBoyWaitingForPatientLock);

            g_doorBoyLock[index]->Acquire();
            g_doorBoyWaitingForPatientLock->Release();
        }

       

      //  HelperPrint(doorboy ,"waiting for patient to alert",index);
        g_doorBoyPatientCV[index]->Wait( g_doorBoyLock[index] );
      //  HelperPrint(doorboy ,"customer arrived",index);
        g_doctorRoomLock->Acquire();
        g_roomNumber = roomNumber;

        g_doctorRoomLock->Release();
        std::cout << "DoorBoy:" << index <<" has told Patient [identifier]" << " to go to Examining Room:" << g_roomNumber << std::endl;
      //  HelperPrint(doorboy ,"telling patient room number",index);
        g_doorBoyPatientCV[index]->Signal( g_doorBoyLock[index] );
        //waiting for patient confirmation
        g_doorBoyPatientCV[index]->Wait( g_doorBoyLock[index] );
      //  HelperPrint(doorboy ,"good bye patient",index);

         g_doorBoyLock[index]->Release();
        
    }
} 

void doctor(int index)
{
    const std::string doctor = "Doctor";
    while(true)
    {
       // g_dbDoctorLock -> Acquire(); // enter the critical section between doctor and doorboy
        g_doorBoyDoctorLineLock->Acquire();
       // HelperPrint(doctor,"Aquiring lock",index);
        g_DoctorState[index] = 0; // make self available
        int doorBoyIndex = 0;
        int shortestLine = g_doorBoyDoctorLine[0];
        for(int i = 0; i<g_NumberBeingUsed; i++)
        {
            if(g_doorBoyDoctorLine[i] < shortestLine)
            {
                shortestLine = g_doorBoyDoctorLine[i];
                doorBoyIndex = i;
            }
            g_doorBoyStateLock->Acquire();
            if(g_DoorBoyState[i] == 0)
            {
                doorBoyIndex = i;
                g_DoorBoyState[i] = 1;
                shortestLine = -1;
                break;
            } 
            g_doorBoyStateLock->Release();
             
        }
        //means al lthe lines are full, choose the shortest
        g_DoctorState[index] = 1;
        if(shortestLine > -1)
        {
           g_doorBoyDoctorLine[doorBoyIndex]++;
         //  HelperPrint2(doctor, "waiting for free doorboy", index, doorBoyIndex);
           g_doorboyDoctorWaitLineCV[doorBoyIndex ] -> Wait(g_doorBoyDoctorLineLock);
         //  HelperPrint2(doctor, "we have a free doorboy", index, doorBoyIndex);
           g_doorBoyDoctorLine[doorBoyIndex]--;
        }

        HelperPrint2(doctor, " has told a DoorBoy to bring a Patient to Examining Room", index, index);
        g_doctorRoomLock->Acquire();
        g_doctorRoom[doorBoyIndex] = index; // tell the doorboy my room #
        g_doctorRoomLock->Release();
        g_doorboyDoctorWaitLineCV[doorBoyIndex ] -> Signal(g_doorBoyDoctorLineLock);
        g_doorboyDoctorWaitLineCV[doorBoyIndex ] -> Wait(g_doorBoyDoctorLineLock);
      //  HelperPrint2(doctor, "door boy got the number ", index, doorBoyIndex);


        g_pDoctorLock->Acquire(); 
        g_doorBoyDoctorLineLock->Release();
    
        // enter doctor-patient interaction
        
       
      //  HelperPrint(doctor, "Waiting for patient", index);
        g_pDocCV[index]->Wait(g_pDoctorLock);
        
        //if all doorboys are busy, do what?
        //g_pDoctorLock->Acquire();
        int randomNum = rand()%10 + 10; //pick a random number to determine time for consultation
        for(int i=0; i<randomNum; i++)
        {
            currentThread->Yield();
        }
        HelperPrint2(doctor, "is examining a Patient with Token", index, g_currentPatient[index]);
        randomNum = rand()%5;
        g_diagnosis diagnosis = NONE;
        float fee = 25;
        switch(randomNum)   // diagnose the patient, precribe medication and calculate the consultiation fee
        {
            case 0:
                HelperPrint2(doctor, "is not sick ", index, g_currentPatient[index]);
                break;
            case 1:
                diagnosis = COLD;
                medicine[index] = Ibuprofen;
                fee += MAX;
                HelperPrint2(doctor, "has determined that the patient has a cold. Patient: ", index, g_currentPatient[index]);
                break;
            case 2:
                diagnosis = FOOD_PSNG;
                medicine[index] = Antibiotic;
                fee *= randomNum;
                 HelperPrint2(doctor, "food poisoning. Patient: ", index, g_currentPatient[index]);
                break;
            case 3:
                diagnosis = STREP;
                medicine[index] = Antibiotic;
                fee *- randomNum;
                 HelperPrint2(doctor, "has determined that the patient has strep. Patient: ", index, g_currentPatient[index]);
                break;
            case 4:
                diagnosis = FLU;
                medicine[index] = Ibuprofen;
                fee *= randomNum;
                HelperPrint2(doctor, "has determined that the patient has the flu. Patient: ", index, g_currentPatient[index]);
                break;
            default:
                HelperPrint(doctor, "Aids", index);
        }
        HelperPrint2(doctor, "handing prescription to patient. Medicine ID: ", index, medicine[index]);
        g_pDocCV[index]->Signal(g_pDoctorLock); // signal patient to store prescription
      //  HelperPrint(doctor, "Here's your prescription", index);
        g_pDocCV[index]->Wait(g_pDoctorLock);   // wait till patient has stored prescription
       // HelperPrint(doctor, "You're welcome. Please wait while I submit your bill", index);
        g_pDoctorLock->Release();

        // enter doctor cashier interaction
        g_cashDoctorLock[index]->Acquire();
        // cashier doctor code here
        HelperPrint(doctor, "Cashier code's gotta get implemented", index);
        g_cashDoctorLock[index]->Release();

        // re-enter doctor patient interaction
        g_pDoctorLock->Acquire();
        g_pDocCV[index]->Signal(g_pDoctorLock); // tell patient to leave
        HelperPrint2(doctor, "You may leave. Get well soon. Patient: ", index,g_currentPatient[index]);
        g_pDocCV[index]->Wait(g_pDoctorLock);
        //HelperPrint(doctor, "Patient has left the room", index);
        g_currentPatient[index] = -1;
        randomNum = rand()%10;
        //i do not like this
        if(randomNum == 9)
        {
            g_DoctorState[index] = 2;
        }
        g_pDoctorLock->Release();   // exit doctor patient interaction

        if(g_DoctorState[index] == 2)
        {
            randomNum = rand()%10 + 10;
            for(int i = 0; i < randomNum; i++)
            {
                currentThread->Yield();
            }
          //  g_DoctorState[index] = 1;
        }
    }
}



void cashier(int index)
{
    const std::string cashier = "Cashier";
    while(true)
    {
        g_cashierLineLock->Acquire(); //we are going to check and see if anyone is waiting for us
        HelperPrint(cashier ,"aquired lock",index);
        g_cashStatus[index] = 0;
        if(g_cashLineCount[index] > 0)
        {
            HelperPrint(cashier ,"alerting patient in line",index);
            g_cashStatus[index] = 1;
            g_cashLineCV[index]->Signal(g_cashierLineLock); //signal them we are now ready for them
            
        }
        g_cashLock[index]->Acquire(); //secure our spot in the next critical setion
        g_cashierLineLock->Release(); //exit  the checking the cashier line spot

        HelperPrint(cashier ,"waiting for patient to approach us",index);
        g_cashPatientCV[index]->Wait( g_cashLock[index] ); //we will wait for the patient here to alert us
        //they would tell us some amount of money here
        HelperPrint(cashier ,"patient arrived",index);
        g_cashtokenLock->Acquire();
        g_cashPayment += 20;
        g_cashPayments[index] = g_cashPayment; //do all thefun token stuff
        HelperPrint(cashier ,"putting money on display",index);
        g_cashtokenLock->Release();
       
        HelperPrint(cashier ,"telling customer about money",index);
        g_cashPatientCV[index]->Signal( g_cashLock[index] ); //tell the patient we have given them the price for the meds and shit
      

        g_cashPatientCV[index]->Wait( g_cashLock[index] ); //we need to wait for the patient to say that is chill 
        HelperPrint(cashier ,"customer paid the bill",index);
        //we will now get the payments form the paitent 
        g_cashtokenLock->Acquire();

        //we want to record our payments
        g_cashierPaymentRecords.push_back(  g_cashPayments[index] );
        g_cashPayments[index] = -1;
        HelperPrint(cashier ,"depositing the money",index);
        g_cashtokenLock->Release();

        HelperPrint(cashier ,"finishing",index);
        g_cashLock[index]->Release();
    }  
}




void pharmacyClerk(int index)
{
    const std::string pharmacist= "Pharmacist";
    while(true)
    {
        g_pharmLineLock->Acquire();
      //  HelperPrint(pharmacist ,"aquired lock",index);
        g_pharmStatus[index] = 0;
        if(g_pharmLineCount[index] > 0)
        {
            
             g_pharmStatus[index] = 1;
            g_pharmLineCV[index]->Signal(g_pharmLineLock);
           
        }
        
        g_pharmLock[index]->Acquire();
        HelperPrint(pharmacist ,"signaling a patient to come",index);
        g_pharmLineLock->Release();
       // HelperPrint(pharmacist ,"waiting for patient approach",index);
        g_pharmPatientCV[index]->Wait( g_pharmLock[index] );
       // HelperPrint(pharmacist ,"was just approached by the notified patient",index);
        //so a patient just cam up to us and gave us their prescription

        HelperPrint(pharmacist ,"gets prescription from patient",index);

        g_pharmPatientCV[index]->Signal( g_pharmLock[index] ); //signal the waiting patient for their meds
       // HelperPrint(pharmacist ,"notified the patient about their waiting meds",index);

        HelperPrint(pharmacist ,"give prescription to patient",index);
        g_pharmPatientCV[index]->Wait( g_pharmLock[index] ); //wait for the patient to say they got their meds
        
        //the patient got their meds, now we have to tell them about the price
        //should probably have some data in here?
        HelperPrint(pharmacist ,"You owe use about three fifty",index);

        g_pharmPatientCV[index]->Signal( g_pharmLock[index] ); //telling them about the price
       // HelperPrint(pharmacist ,"told patient of the price",index);
        g_pharmPatientCV[index]->Wait( g_pharmLock[index] ); //waiting for the patient to say that they paid it
        HelperPrint(pharmacist ,"got money from the patient",index);
        //the patient paid and deposited the funds
        g_pharmtokenLock->Acquire();
        //HelperPrint(pharmacist ,"depositing payment",index);
        g_pharmPaymentRecords.push_back( g_pharmPayments[index] );
        g_pharmPayments[index] = -1;
        g_pharmtokenLock->Release();


        g_pharmPatientCV[index]->Signal( g_pharmLock[index] ); //we got the patients money and everything is chilll
        //HelperPrint(pharmacist ,"got patients money and deposted",index);
        g_pharmPatientCV[index]->Wait( g_pharmLock[index] ); //we are just going to wait till the patient leaves
        //HelperPrint(pharmacist ,"patient left",index);

        g_pharmLock[index]->Release();

    }
}


void InitGlobals()
{   


    //PHARMACY VARIABLES
    g_pharmLineLock = new Lock("pharm_lock");
    g_pharmtokenLock = new Lock("pharm token lock");
    for(int i=0;i<g_NumberBeingUsed;i++)
    {
        g_pharmLock[i] = new Lock("cashier lock");
        g_pharmLineCV[i] = new Condition("pharm line cv");
        g_pharmPatientCV[i] = new Condition("pharm patient cv");
    }



    //CASHIER VARIABLES
    g_cashierLineLock = new Lock("cashier lock");
    g_cashtokenLock = new Lock("cashier token lock");
    for(int i=0;i<g_NumberBeingUsed;i++)
    {
        g_cashLock[i] = new Lock("cashier lock");
        g_cashLineCV[i] = new Condition("cashier line condition");
        g_cashPatientCV[i] = new Condition("cashier patient condition");
    }

    /// RECEPTIONIST VARIABLES
    g_recepLineLock = new Lock("line lock");
    g_receptokenLock = new Lock("token lock");
    for(int i=0;i<g_NumberBeingUsed;i++)
    {
        g_recepLineCV[i] = new Condition("recep line Condition");
        g_recepPatientCV[i] = new Condition("recep patient Condition");
        g_recepLock[i] = new Lock("recep lock");
    }



    //DOORBOY VARIABLES
    g_doorBoyWaitingForPatientLock =  new Lock("door boy waiting for patient lock");
    g_doctorRoomLock = new Lock("doorboy doctor room lock");
    g_doorBoyStateLock = new Lock("doorboy state lock");
    g_doorBoyLineLock = new Lock("doorboy line lock");
    g_dbBreakLock = new Lock("doorboy break lock");
    g_doorBoyDoctorLineLock = new Lock("door boy doctor wait lock");
    for(int i=0; i<g_NumberBeingUsed; i++)
    {
        g_doorboyWaitLineCV[i] = new Condition("door boy line condition");
        g_dbOnBreak[i] = new Condition("doorboy break condition");
        g_doorBoyPatientCV[i] = new Condition("door boy to patient condition");
        g_doorBoyLock[i] = new Lock("door boy lock");
        g_doorboyDoctorWaitLineCV[i] = new Condition("door boy doctor wait line");
        g_doorBoyWaitingForPatientCV[i] = new Condition("door boy waiting for patient lock");

    }

    //DOCTOR VARIABLES
    g_pDoctorLock = new Lock("patient doctor lock");
    for(int i=0; i<g_NumberBeingUsed;i++)
    {
        g_pDocCV[i] = new Condition("patient doctor condition");
        g_cashDoctorLock[i] = new Lock("doctor cashier lock");
    }
}


bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}


void PrintOutCounts()
{
    std::cout << "Number of Receptionists = " << g_NumberBeingUsed << std::endl;
    std::cout << "Number of Doctors = " << g_NumberBeingUsed << std::endl;
    std::cout << "Number of DoorBoys = " << g_NumberBeingUsed << std::endl;
    std::cout << "Number of Cashiers = " << g_NumberBeingUsed << std::endl;
    std::cout << "Number of PharmacyClerks = " << g_NumberBeingUsed << std::endl;
    std::cout << "Number of Patients = " << g_patientCount << std::endl;
}


void RunStandardSimulaiton()
{

   std::string input;
     
     std::cout<<"how many receptionists, doctors, doorboys, cashiers, pharmacists you want to create? (no more than 5) :"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many receptionists, doctors, doorboys, cashiers, pharmacists you want to create? (no more than 5) :"<<std::endl;
        std::getline (std::cin,input);
     }
     g_NumberBeingUsed = std::atoi( input.c_str() );
     
     std::cout<<"how many patients you want to create?:"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many patients you want to create?:"<<std::endl;
        std::getline (std::cin,input);
     }
     g_patientCount = std::atoi( input.c_str() );
     std::cout<<g_patientCount<<std::endl;

     PrintOutCounts();
     InitGlobals();
     Thread *thread;
     
     for(int i=0;i<g_NumberBeingUsed;i++)
     
     {


        thread = new Thread("receptionist");
        thread->Fork(receptionist, i );


        thread = new Thread("pharmacyClerk");
        thread->Fork(pharmacyClerk,i);


        thread = new Thread("cashier");
        thread->Fork(cashier,i);

        thread = new Thread("doorboy");
        thread->Fork(doorboy,i);

        thread = new Thread("doctor");
        thread->Fork(doctor,i);

        

     }

    for(int i = 0; i< g_patientCount; i ++)
    
    {
        thread = new Thread("patient");
        thread->Fork(patient,i);
    }


    thread = new Thread("hospital manager");
    thread->Fork(hospitalManager,1);

}

void Test2_PatientGetsToSeeDoctorThroughDoorboy()
{
     std::string input;

     std::cout<<"No door boys get created in this test"<<std::endl;
     std::cout<<"how many receptionists, doctors, cashiers, pharmacists you want to create? (no more than 5) :"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many receptionists, doctors, cashiers, pharmacists you want to create? (no more than 5) :"<<std::endl;
        std::getline (std::cin,input);
     }
     g_NumberBeingUsed = std::atoi( input.c_str() );
     
     std::cout<<"how many patients you want to create?:"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many patients you want to create?:"<<std::endl;
        std::getline (std::cin,input);
     }
     g_patientCount = std::atoi( input.c_str() );
     std::cout<<g_patientCount<<std::endl;

     PrintOutCounts();
     InitGlobals();
     Thread *thread;
     
     for(int i=0;i<g_NumberBeingUsed;i++)
     
     {


        thread = new Thread("receptionist");
        thread->Fork(receptionist, i );


        thread = new Thread("pharmacyClerk");
        thread->Fork(pharmacyClerk,i);


        thread = new Thread("cashier");
        thread->Fork(cashier,i);

        thread = new Thread("doctor");
        thread->Fork(doctor,i);

        

     }

    for(int i = 0; i< g_patientCount; i ++)
    
    {
        thread = new Thread("patient");
        thread->Fork(patient,i);
    }


    thread = new Thread("hospital manager");
    thread->Fork(hospitalManager,1);   
}

void Test3_AllDoorboysOnBreak()
{
     std::string input;
     
     std::cout<<"All doorboys will go on break in this test"<<std::endl;
     std::cout<<"how many receptionists, doctors, doorboys, cashiers, pharmacists you want to create? (no more than 5) :"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many receptionists, doctors, doorboys, cashiers, pharmacists you want to create? (no more than 5) :"<<std::endl;
        std::getline (std::cin,input);
     }
     g_NumberBeingUsed = std::atoi( input.c_str() );
     
     std::cout<<"how many patients you want to create?:"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many patients you want to create?:"<<std::endl;
        std::getline (std::cin,input);
     }
     g_patientCount = std::atoi( input.c_str() );
     std::cout<<g_patientCount<<std::endl;

     PrintOutCounts();
     InitGlobals();
     Thread *thread;
     
     for(int i=0;i<g_NumberBeingUsed;i++)
     
     {


        thread = new Thread("receptionist");
        thread->Fork(receptionist, i );


        thread = new Thread("pharmacyClerk");
        thread->Fork(pharmacyClerk,i);


        thread = new Thread("cashier");
        thread->Fork(cashier,i);

        thread = new Thread("doorboy");
        thread->Fork(doorboy,i);

        thread = new Thread("doctor");
        thread->Fork(doctor,i);
    

     }

    for(int i = 0; i< g_patientCount; i ++)
    
    {
        thread = new Thread("patient");
        thread->Fork(patient,i);
    }

}

void Test4_OnlyOnePatientSeeOneDoctor()
{
     std::string input;
     std::cout<<"Only one doctor gets created in this test"<<std::endl;
     std::cout<<"how many receptionists, doorboys, cashiers, pharmacists you want to create? (no more than 5) :"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many receptionists, doorboys, cashiers, pharmacists you want to create? (no more than 5) :"<<std::endl;
        std::getline (std::cin,input);
     }
     g_NumberBeingUsed = std::atoi( input.c_str() );
     
     std::cout<<"how many patients you want to create?:"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many patients you want to create?:"<<std::endl;
        std::getline (std::cin,input);
     }
     g_patientCount = std::atoi( input.c_str() );
     std::cout<<g_patientCount<<std::endl;

     PrintOutCounts();
     InitGlobals();
     Thread *thread;

     thread = new Thread("doctor");
     thread->Fork(doctor,1);
     
     for(int i=0;i<g_NumberBeingUsed;i++)
     
     {


        thread = new Thread("receptionist");
        thread->Fork(receptionist, i );


        thread = new Thread("pharmacyClerk");
        thread->Fork(pharmacyClerk,i);


        thread = new Thread("cashier");
        thread->Fork(cashier,i);

        thread = new Thread("doorboy");
        thread->Fork(doorboy,i);


     }

    for(int i = 0; i< g_patientCount; i ++)
    
    {
        thread = new Thread("patient");
        thread->Fork(patient,i);
    }


    thread = new Thread("hospital manager");
    thread->Fork(hospitalManager,1);
}

void Test5_PatientsChooseShortest()
{
     std::string input;
     std::cout<<"there is only one receptionist, cashier, pharmacist in this test, the other line counts are filled with 10, so the patients should go to receptionist 0, cashier 0, pharmacist0"<<std::endl;
     std::cout<<"how many doorboys, doctors you want to create? (no more than 5) :"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many doctors, doorboys you want to create? (no more than 5) :"<<std::endl;
        std::getline (std::cin,input);
     }
     g_NumberBeingUsed = std::atoi( input.c_str() );
     
     std::cout<<"how many patients you want to create?:"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many patients you want to create?:"<<std::endl;
        std::getline (std::cin,input);
     }
     int patientCount = std::atoi( input.c_str() );
     std::cout<<patientCount<<std::endl;

     InitGlobals();
     Thread *thread;

        for(int i = 1; i<MAX; i++)
        {
            g_cashLineCount[i] = 10;
            g_recepLineCount[i] = 10;
            g_pharmLineCount[i] = 10;
        }
        
    
        thread = new Thread("receptionist");
        thread->Fork(receptionist, 0 );


        thread = new Thread("pharmacyClerk");
        thread->Fork(pharmacyClerk, 0);


        thread = new Thread("cashier");
        thread->Fork(cashier,0);
     

     for(int i=0;i<g_NumberBeingUsed;i++)
     
     {


       

        thread = new Thread("doorboy");
        thread->Fork(doorboy,i);

        thread = new Thread("doctor");
        thread->Fork(doctor,i);

        

     }

    for(int i = 0; i< patientCount; i ++)
    
    {
        thread = new Thread("patient");
        thread->Fork(patient,i);
    }


    thread = new Thread("hospital manager");
    thread->Fork(hospitalManager,1);

}

void Test6_NoStaffWaitInLine()
{
     std::string input;
     std::cout<<"No receptionists, cashiers, pharmacists get created in this test"<<std::endl;
     std::cout<<"how many doctors, doorboys, you want to create? (any number less than 5) :"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many doctors, doorboys, you want to create? (any number less than 5) :"<<std::endl;
        std::getline (std::cin,input);
     }
     g_NumberBeingUsed = std::atoi( input.c_str() );
     
     std::cout<<"how many patients you want to create?:"<<std::endl;
     std::getline (std::cin,input);
     while(!is_number( input ))
     {
        std::cout<<"how many patients you want to create?:"<<std::endl;
        std::getline (std::cin,input);
     }
     g_patientCount = std::atoi( input.c_str() );
     std::cout<<g_patientCount<<std::endl;

     PrintOutCounts();
     InitGlobals();
     Thread *thread;
     
     for(int i=0;i<g_NumberBeingUsed;i++)
     
     {


        thread = new Thread("doorboy");
        thread->Fork(doorboy,i);

        thread = new Thread("doctor");
        thread->Fork(doctor,i);

        

     }

    for(int i = 0; i< g_patientCount; i ++)
    
    {
        thread = new Thread("patient");
        thread->Fork(patient,i);
    }


    thread = new Thread("hospital manager");
    thread->Fork(hospitalManager,1);
}

void Test7_DoctorGoOnBreak()
{

}

void Test8_NoPatientsGetToDoctorOnBreak()
{

}

void Test9_AllDoctorsOnBreak()
{

}

void Test10_StaffGetSignaledByManager()
{

}

void Test11_TotalNeverGetAffected()
{

}

void Test12_StaffGoOnBreakLineEmpty()
{

}

void Problem2()
{

    srand (time(NULL));
    std::string input;
/*
    while(true)
    {*/
        input.clear();
        std::cout << "Specify which test you would like to run" << std::endl;
        std::cout << "1. Let the simulation loose" << std::endl;
        std::cout << "2. Patients only gets in to see a Doctor when the DoorBoy asks them to." << std::endl;
        std::cout << "3. If all the Doorboys are on a break no patient gets in to see Doctors." << std::endl;
        std::cout << "4. Only one Patient get to see one Doctor at any given instance of time, in a room" << std::endl;
        std::cout << "5. Patients always choose the shortest line with the Cashier, PharmacyClerk and Receptionist." << std::endl;
        std::cout << "6. If there is no Cashier/PharmacyClerk/Receptionist the patient must wait in line. " << std::endl;
        std::cout << "7. Doctors go on break at random intervals." << std::endl;
        std::cout << "8. When the Doctor is at break no patient gets in to that examining room." << std::endl;
        std::cout << "9. If all Doctors are on break, all Patients wait." << std::endl;
        std::cout << "10. DoorBoy/Cashier/PharmacyClerk gett signaled by hospital manager when patients are waiting" << std::endl;
        std::cout << "11. The total sales of medicines and the total consultation fees never get affected by race conditions" << std::endl;
        std::cout << "12. DoorBoy/PharmacyClerk/Receptionist/Cashier go on break if their line is empty." << std::endl;

      
        std::getline (std::cin,input);
        if(is_number( input ))
        {
            int choice = std::atoi( input.c_str() );
            std::cout << "Choice " << choice << std::endl;
            switch( choice )
            {   
                case 1:
                    RunStandardSimulaiton();
                    break;
                case 2:
                    Test2_PatientGetsToSeeDoctorThroughDoorboy();
                    break;
                case 3:
                    Test3_AllDoorboysOnBreak();
                    break;
                case 4:
                    Test4_OnlyOnePatientSeeOneDoctor();
                    break;
                case 5:
                    Test5_PatientsChooseShortest();
                    break;
                case 6:
                    Test6_NoStaffWaitInLine();
                    break;
                case 7: 
                    Test7_DoctorGoOnBreak();
                    break;
                case 8:
                    Test8_NoPatientsGetToDoctorOnBreak();
                    break;
                case 9:
                    Test9_AllDoctorsOnBreak();
                    break;
                case 10:
                    Test10_StaffGetSignaledByManager();
                    break;
                case 11:
                    Test11_TotalNeverGetAffected();
                    break;
                case 12:
                    Test12_StaffGoOnBreakLineEmpty();
                    break;

                default:
                    std::cout << "You input an unrecognized option" << std::endl;
            }
        }
        else
        {
            std::cout << "Bad Input " << std::endl;
        }
    //}
}

#endif
