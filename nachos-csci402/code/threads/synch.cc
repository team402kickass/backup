// synch.cc 
//	Routines for synchronizing threads.  Three kinds of
//	synchronization routines are defined here: semaphores, locks 
//   	and condition variables (the implementation of the last two
//	are left to the reader).
//
// Any implementation of a synchronization routine needs some
// primitive atomic operation.  We assume Nachos is running on
// a uniprocessor, and thus atomicity can be provided by
// turning off interrupts.  While interrupts are disabled, no
// context switch can occur, and thus the current thread is guaranteed
// to hold the CPU throughout, until interrupts are reenabled.
//
// Because some of these routines might be called with interrupts
// already disabled (Semaphore::V for one), instead of turning
// on interrupts at the end of the atomic operation, we always simply
// re-set the interrupt state back to its original value (whether
// that be disabled or enabled).
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "synch.h"
#include "system.h"
#include <iostream>
//----------------------------------------------------------------------
// Semaphore::Semaphore
// 	Initialize a semaphore, so that it can be used for synchronization.
//
//	"debugName" is an arbitrary name, useful for debugging.
//	"initialValue" is the initial value of the semaphore.
//----------------------------------------------------------------------

Semaphore::Semaphore(char* debugName, int initialValue)
{
    name = debugName;
    value = initialValue;
    queue = new List;
}

//----------------------------------------------------------------------
// Semaphore::Semaphore
// 	De-allocate semaphore, when no longer needed.  Assume no one
//	is still waiting on the semaphore!
//----------------------------------------------------------------------

Semaphore::~Semaphore()
{
    delete queue;
}

//----------------------------------------------------------------------
// Semaphore::P
// 	Wait until semaphore value > 0, then decrement.  Checking the
//	value and decrementing must be done atomically, so we
//	need to disable interrupts before checking the value.
//
//	Note that Thread::Sleep assumes that interrupts are disabled
//	when it is called.
//----------------------------------------------------------------------

void
Semaphore::P()
{
    IntStatus oldLevel = interrupt->SetLevel(IntOff);	// disable interrupts
    
    while (value == 0) { 			// semaphore not available
	queue->Append((void *)currentThread);	// so go to sleep
	currentThread->Sleep();
    } 
    value--; 					// semaphore available, 
						// consume its value
    
    (void) interrupt->SetLevel(oldLevel);	// re-enable interrupts
}

//----------------------------------------------------------------------
// Semaphore::V
// 	Increment semaphore value, waking up a waiter if necessary.
//	As with P(), this operation must be atomic, so we need to disable
//	interrupts.  Scheduler::ReadyToRun() assumes that threads
//	are disabled when it is called.
//----------------------------------------------------------------------

void
Semaphore::V()
{
    Thread *thread;
    IntStatus oldLevel = interrupt->SetLevel(IntOff);

    thread = (Thread *)queue->Remove();
    if (thread != NULL)	   // make thread ready, consuming the V immediately
	scheduler->ReadyToRun(thread);
    value++;
    (void) interrupt->SetLevel(oldLevel);
}

// Dummy functions -- so we can compile our later assignments 
// Note -- without a correct implementation of Condition::Wait(), 
// the test case in the network assignment won't work!
Lock::Lock(char* debugName) 
{
    status = FREE;
    queue = new List;
    m_owningThread = NULL;
    name = debugName;
}
Lock::~Lock() 
{
    delete queue;
    m_owningThread = NULL;
}

bool Lock::isHeldByCurrentThread()
{
    return m_owningThread == currentThread;
}

void Lock::Acquire() 
{
    IntStatus oldLevel = interrupt->SetLevel(IntOff);//disable interrupts
    
    if(isHeldByCurrentThread())
    {
        (void) interrupt->SetLevel(oldLevel);//restore interrupts
        return;
    }
    if(status == FREE/*lock is available*/)
    {
        status = BUSY;//make lock Busy
        m_owningThread = currentThread; // the current thread now becomes the owner
    }
    else
    {
       // status = BUSY;//lock not available
    
        queue->Append((void *)currentThread);//Add my self to lock wait queue
        currentThread -> Sleep();
    }
    (void) interrupt->SetLevel(oldLevel);//restore interrupts
}


void Lock::Release() 
{
    IntStatus oldLevel = interrupt->SetLevel(IntOff);//disable interrupts
    
    if(!isHeldByCurrentThread())
    {
        //print error msg
         std::cout << " " << std::endl;  
         printf("?!?! ERROR: A thread tried to relase anothers thread, baddie:  %s \n", name);
         std::cout << " " << std::endl; 
        (void) interrupt->SetLevel(oldLevel);//restore interrupts
        return;
    }
    if(!(queue->IsEmpty()))
    {
        m_owningThread =  (Thread*)queue->Remove();  //remove from wait queue
        if(m_owningThread != NULL)
        {
            scheduler->ReadyToRun( m_owningThread );   //put in ready queue
        }
        else
        {
            std::cout << " " << std::endl; 
            std::cout << "?!?! ERROR: Nulll thread in the release" << std::endl;
             std::cout << " " << std::endl;         
        }
    }
    else
    {
        status = FREE;  //make lock free
        m_owningThread = NULL;  //clear lock ownership
    }
    (void) interrupt->SetLevel(oldLevel);//restore interrupts
}

bool Lock::WaitQueueEmpty()
{
    return queue->IsEmpty();
}

Condition::Condition(char* debugName) 
{
    name = debugName;
    m_waitingQueue = new List;
    m_waitingLock = NULL;
    //Condition variables: don't have state
}
Condition::~Condition() 
{
    delete m_waitingQueue;
    m_waitingLock = NULL;
}
void Condition::Wait(Lock* conditionLock) 
{ 
   // ASSERT(FALSE); 
    IntStatus oldLevel = interrupt->SetLevel(IntOff);//disable interrupts
    
    if(conditionLock == NULL)
    {
         std::cout << " " << std::endl; 
         printf("?!?! ERROR:  Null WAIT condition lock. Current Lock:  %s \n", name);
         std::cout << " " << std::endl; 
        (void) interrupt->SetLevel(oldLevel);//restore interrupts
        return;
    }
    if(m_waitingLock == NULL)
    {
        m_waitingLock = conditionLock;
    }
    if(m_waitingLock != conditionLock)
    {
          std::cout << " " << std::endl; 
          printf("?!?! ERROR:  Current WAIT lock doesnt match the passed in condition lock: Waiting lock: %s   Condition lock: %s  \n", name,conditionLock->getName() );
          std::cout << " " << std::endl;  
         (void) interrupt->SetLevel(oldLevel);//restore interrupts
        return;
    }
    
    conditionLock -> Release(); //exit monitor
    m_waitingQueue->Append((void*) currentThread ); //put myself in CV wait queue
    currentThread -> Sleep();
    conditionLock -> Acquire();

    (void) interrupt->SetLevel(oldLevel);//restore Interrupts

}
void Condition::Signal(Lock* conditionLock) 
{
     IntStatus oldLevel = interrupt->SetLevel(IntOff);//disable interrupts


     if(conditionLock == NULL)
     {
        std::cout << " " << std::endl; 
        printf("?!?! ERROR: SIGNAL Null condition lock. Current Lock:  %s \n", name);
         std::cout << " " << std::endl; 
        (void) interrupt->SetLevel(oldLevel);//restore interrupts
        return;
     }

     if(m_waitingLock == NULL)
     {
         std::cout << " " << std::endl; 
         printf("?!?! ERROR:  Null SIGNAL lock. Passed in condition lock:  %s \n", conditionLock->getName());
         std::cout << " " << std::endl;  
        (void) interrupt->SetLevel(oldLevel);//restore interrupts
        return;
     }   

     if(m_waitingQueue->IsEmpty())
     {
        std::cout << " " << std::endl; 
         printf("?!?! ERROR:  Wait queue is empty for Condition:  %s \n", name );
         std::cout << " " << std::endl;  
        (void) interrupt->SetLevel(oldLevel);//restore interrupts
        return;
     }

     if(m_waitingLock != conditionLock)
     {
         std::cout << " " << std::endl;  
         printf("?!?! ERROR:  Current SIGNAL lock doesnt match the passed in condition lock: Waiting lock: %s   Condition lock: %s  \n", name,conditionLock->getName() );
         std::cout << " " << std::endl;  
        (void) interrupt->SetLevel(oldLevel);//restore interrupts
        return;
     } 
    
    Thread* thread = (Thread*)m_waitingQueue->Remove();
    
    if(thread != NULL)
    {
        scheduler->ReadyToRun( thread );   
    }
    
    
    if(m_waitingQueue->IsEmpty())
    {
        m_waitingLock = NULL;
    }
    (void) interrupt->SetLevel(oldLevel);//restore Interrupts
}
void Condition::Broadcast(Lock* conditionLock) 
{
  //  IntStatus oldLevel = interrupt->SetLevel(IntOff);//disable interrupts
    while(! (m_waitingQueue->IsEmpty() ) )
    {
        Signal(conditionLock);
    }
  //  (void) interrupt->SetLevel(oldLevel);//restore Interrupts
}

bool Condition::WaitQueueEmpty()
{
    return m_waitingQueue->IsEmpty();
}
/*
//----------------------------------------------------------------------
// Network
//  ServerLock
//  ServerCondition
//  MonitorVariables
//  All start from here
//----------------------------------------------------------------------


ServerLock::ServerLock(char *debugName)
{
    status = FREE;
    queue = new List;
    name = debugName;
}

ServerLock::~ServerLock()
{
    delete queue;
}

bool ServerLock::WaitQueueEmpty()
{
    return queue->IsEmpty();
}

void ServerLock::Acquire(int owner, char *data)
{
    if(status == FREE)
    {
        status = BUSY;
        LockOwnerID = owner;
    }
    else
    {
        queue -> Append(data);//append the message
    }
}

void ServerLock::Release(int owner)
{
    if(LockOwnerID != owner)
    {
        //print error msg
         std::cout << " " << std::endl;  
         printf("?!?! ERROR: A thread tried to relase anothers thread, baddie:  %s \n", name);
         std::cout << " " << std::endl; 
         //(void) interrupt->SetLevel(oldLevel);//restore interrupts
        return;
    }
    if(!(queue->IsEmpty()))
    {
       msg = (char*)queue->Remove(); //remove from wait queue
       
       status = BUSY;
       LockOwnerID = owner;
       //send the msg to client in server


    }
}

ServerCondition::ServerCondition(char *debugName)
{
    name = debugName;
    m_waitingQueue = new List;
}

ServerCondition::~ServerCondition()
{
    delete m_waitingQueue;
}

bool ServerCondition::WaitQueueEmpty()
{
    return m_waitingQueue->IsEmpty();
}

void ServerCondition::Signal(int waitLock, int lockOwner)
{

}

void ServerCondition::Wait(int waitLock, int lockOwner)
{

}

KernelMV::KernelMV(char *MVname)
{
    name = MVname;
}

KernelMV::~KernelMV()
{
    delete name;
    delete value;
}
*/

