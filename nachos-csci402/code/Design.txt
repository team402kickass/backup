Design
	for the design of the server we will be creating new system calls for the locks, condition variables
	and adding member variable system calls. we will need to edit start.s syscall.h and exception.cc 
	for these new call we will prepend Server to all the calls and then add them into exception.cc
	for each syscall we will want to create a message that will pass in the necessary informaiton.
	for example create member variable would take the name, size of the name, and the number of values
	you would like created. the nubmer of values will range from 1-as many as you like in case you would like 
	an aray. 

	each syscalls message to the server will have an identifying 3 letters to the message to help us ID the message
	when the message has been created in the syscalls we will then pass it to a generic sendtoServer function
	which will create the headers. it will set the header.to to 0 which will be the destination fo the server and then
	the from which will be the client ID.
	the message will then be sent to the server and we will do an error check for the reponse.

	if the message was sent successfull we will then wait for a reponse from the server. when we have recieved  a 
	response we will then be able to parse the message and return the user program and resume it.

	the server will live in the netowrk directory

	in nettest.cc we will have the server function whcih will be in a while loop. we will constanlty check 
	for message each iteration of the loop. 
	when we receive the message we will parse the type of message it is and then do appropiate actions

	to help with the parsing of the message we have made a class call ServerData which will hold all the 
	server data. it will containt the ServerLock ServerCOndition and ServerMemberVariable.

	WE will then be able to do operations on the stuctures like finding a specific lock, condition or member variable 
	and retrive its data

	class ServerData
	Member Data:
	int m_lockCount, m_cvCount, m_mvCount

	struct Response {
		int ownerId;
		char* m_msg; 
	}

	struct ServerLock {
		List* m_waitQueue;
		bool m_free;
		bool m_toBeDestroyed;
		char* m_name;
		int ownerId;
	}

	struct ServerMV {
		int* m_value;
		int m_count, ownerMAchineID, ownerMailBoxID;
		char* m_name
	}

	struct ServerCV {
		bool m_toBeDestroyed;
		int m_waitingLock, m_ownerID;
		char* m_name;
		List *m_waitQueue;
	}

	ServerCV *m_cvlist[MAX_LOCKS];
	ServerMV *m_mvlist[MAX_LOCKS];
	ServerLock *m_locklist[MAX_LOCKS];

	+ Member Functions
	void InsertNewMV(int index, char* name, int size) {
		//create and initialize a new mv at m_mvlist[index];
	}


	int FindMVIndex (char* name, int valSize){
		//find the correct mv
		for(i = 0; i < m_mvcount; i++)
		{
			if mvList[i]->name = name, return i
		}
		else InsertNewMV(m_mvCount, name, valSize);
		return m_mvcount++;
	}

	void InsertNewLock(int index, char* name)
	{
		//create and initialize a new ServerLock at m_locklist[index];
	}

	int FindLockIndex(char* name) {
		//Just like FindMVIndex
	}

	void InsertNewCV(int index, char* name) {
		//create and initialize a new ServerCV at m_cvlist[index]
	} 

	int FindCVIndex(char* name) {
		//Just like FindLockIndex 
	}

	int ValidateMV(int index, int valIndex) {
		1. validate index and m_mvlist[index] != NULL;
		2. check valIndex < m_mvlist[index]->count and > 0;
		3. check m_mvlist[index]->m_value[valIndex] != NULL;
	}

	int ValidateLock(int index) {
		//Follow 1. from ValidateMV, but for m_locklist[index]
	}

	int ValidateCV(int index) {
		//Like ValidateLock
	}

	void Destroy MV(int index) {
		delete m_mvList[index];
		m_mvlist[index] = NULL;
	}

	We have various other such operations that can be performed on MVs, Locks, and CVs:
	- GetMV
	- SetMV
	- DestroyLock, DestroyCV
	- DoesClientOwnLock: to make sure a client owns a lock it is trying to release
	- Acquire
	- DoesLockMatchCV: make sure a client isnt trying to signal the wrong lock

	MESSAGE PARSING in Server function of nettest.cc :

	Variables used for message parsing
	- char buffer[MaxMailSize]
	- PacketHeader outPktHdr, inPktHdr
	- MailHeader outMailHdr, inMailHdr
	- char* name, *type, *reply
	- int valSize, mvID, mvIndex, valIndex, value, valid //Variables used for parsing MV operations
	//Variables used for parsing lock operations
	- int lockID, lockIndex
	- bool acq
	//Variables used for parsing CV ops
	- int cvID, cvIndex

	- stringstream ss


	The Server function has an infinite while loop which listens for messages, and once a message is received by the postOffice:
	- First set the outgoing mail and packet header fields
		> This is important as we need to track which machine to communicate with next
	- Then parse the type from a stringstream
		> The type will be 3 letters for the message as we talked about earlier
	- Then, based on the type, we carry out the necessary operation
	- For example, if type = CMV
		> "CMV" is our code for Create MV
		> Get the name and valSize from ss
		> mvID = g_serverData->FindMVIndex(name, valSize) //call corresponding serverdata function
		> Make sure the result is valid, in this case mvId != -1
		> Create a reply using the ss
			- ss << type << " " << mvID;
			- reply = cast ss as a c-string
			- sendToClient(reply, outPktHdr, inPktHdr, outMailHdr, inMailHdr)

	Similarly, we have codes for all the other operations required for MVs, CVs, and Locks:
	- GMV = Get MV
	- SMV = Set the value for an MV
	- DMV = Destroy MV
	- CLK = Create lock
	- DLK = Destroy lock
	- ACQ = Acquire
	- REL = Release
	- CCV = Create CV
	- And so on

	Message parsing is the core of the functionality of our network system




