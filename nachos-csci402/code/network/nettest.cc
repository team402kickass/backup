// nettest.cc 
//	Test out message delivery between two "Nachos" machines,
//	using the Post Office to coordinate delivery.
//
//	Two caveats:
//	  1. Two copies of Nachos must be running, with machine ID's 0 and 1:
//		./nachos -m 0 -o 1 &
//		./nachos -m 1 -o 0 &
//
//	  2. You need an implementation of condition variables,
//	     which is *not* provided as part of the baseline threads 
//	     implementation.  The Post Office won't work without
//	     a correct implementation of condition variables.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"

#include "system.h"
#include "network.h"
#include "post.h"
#include "interrupt.h"
#include "synch.h"
#include <sstream>
#include <string.h>
#include "ServerData.h"
#include <string>
/*int g_numberbeingused = 100;
int g_serverLockIndex = 0;

KernelMV *g_kernelMV[g_numberbeingused];
ServerLock *g_serverLock[g_numberbeingused];
ServerCondition *g_serverCV[g_numberbeingused];*/
// Test out message delivery, by doing the following:
//	1. send a message to the machine with ID "farAddr", at mail box #0
//	2. wait for the other machine's message to arrive (in our mailbox #0)
//	3. send an acknowledgment for the other machine's message
//	4. wait for an acknowledgement from the other machine to our 
//	    original message

void
MailTest(int farAddr)
{
    PacketHeader outPktHdr, inPktHdr;
    MailHeader outMailHdr, inMailHdr;
    char *data = "Hello there!";
    char *ack = "Got it!";
    char buffer[MaxMailSize];

    // construct packet, mail header for original message
    // To: destination machine, mailbox 0
    // From: our machine, reply to: mailbox 1
    outPktHdr.to = farAddr;		
    outMailHdr.to = 0;
    outMailHdr.from = 1;
    outMailHdr.length = strlen(data) + 1;

    // Send the first message
    bool success = postOffice->Send(outPktHdr, outMailHdr, data); 

    if ( !success ) {
      printf("The postOffice Send failed. You must not have the other Nachos running. Terminating Nachos.\n");
      interrupt->Halt();
    }

    // Wait for the first message from the other machine
    postOffice->Receive(0, &inPktHdr, &inMailHdr, buffer);
    printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
    fflush(stdout);

    // Send acknowledgement to the other machine (using "reply to" mailbox
    // in the message that just arrived
    outPktHdr.to = inPktHdr.from;
    outMailHdr.to = inMailHdr.from;
    outMailHdr.length = strlen(ack) + 1;
    success = postOffice->Send(outPktHdr, outMailHdr, ack); 

    if ( !success ) {
      printf("The postOffice Send failed. You must not have the other Nachos running. Terminating Nachos.\n");
      interrupt->Halt();
    }

    // Wait for the ack from the other machine to the first message we sent.
    postOffice->Receive(1, &inPktHdr, &inMailHdr, buffer);
    printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
    fflush(stdout);

    // Then we're done!
    interrupt->Halt();
}

bool IsMvValid(int mvIndex, int valIndex, std::string type, char *message, PacketHeader outPktHdr, MailHeader  outMailHdr)
{
     stringstream ss;
     
     if(g_serverData->ValidateMV(mvIndex, valIndex) == 0)
    {
        int length = strlen(message);
        for(int i=0;i< length; i++)
        {
            printf("%c", message[i]) ;
        }
        printf("\n" );

        ss << type << " " <<-1;
        outMailHdr.length = ss.str().size() + 1;
        bool success = postOffice->Send(outPktHdr, outMailHdr, const_cast<char *>( ss.str().c_str()) ); 

        if ( !success ) 
        {
            printf("The postOffice Send failed. You must not have the other Nachos running. Terminating Nachos.\n");
            interrupt->Halt();
        }
        return false;
    }
    return true;
}

bool IsLockValid(int lockIndex ,std::string type, char *message, PacketHeader outPktHdr, MailHeader  outMailHdr)
{
     stringstream ss;
     if(g_serverData->ValidateLock(lockIndex) == 0)
    {
        int length = strlen(message);
        for(int i=0;i< length; i++)
        {
            printf("%c", message[i]) ;
        }
        printf("\n" );

        ss << type << " " <<-1;
         outMailHdr.length = ss.str().size() + 1;
        bool success = postOffice->Send(outPktHdr, outMailHdr, const_cast<char *>( ss.str().c_str())); 

        if ( !success ) 
        {
            printf("The postOffice Send failed. You must not have the other Nachos running. Terminating Nachos.\n");
            interrupt->Halt();
        }
        return false;
    }
    return true;
}

bool IsCVValid(int cvIndex ,std::string type, char *message, PacketHeader outPktHdr, MailHeader  outMailHdr)
{
     stringstream ss;
     if(g_serverData->ValidateCV(cvIndex) == 0)
    {
        int length = strlen(message);
        for(int i=0;i< length; i++)
        {
            printf("%c", message[i]) ;
        }
        printf("\n" );


       
        ss << type << " " <<-1;
       outMailHdr.length = ss.str().size() + 1;
        bool success = postOffice->Send(outPktHdr, outMailHdr, const_cast<char *>( ss.str().c_str()) ); 

        if ( !success ) 
        {
            printf("The postOffice Send failed. You must not have the other Nachos running. Terminating Nachos.\n");
            interrupt->Halt();
        }
        return false;
    }
    return true;
}

void sendToClient(std::string reply, PacketHeader outPktHdr, PacketHeader inPktHdr,MailHeader outMailHdr,MailHeader inMailHdr)
{  
    outMailHdr.length = reply.size() + 1;
    bool success = postOffice->Send(outPktHdr, outMailHdr, const_cast<char *>( reply.c_str())); 
    if ( !success ) 
    {
        printf("The postOffice Send failed. You must not have the other Nachos running. Terminating Nachos.\n");
        interrupt->Halt();       
    }
}


void Server()
{
    char buffer[MaxMailSize];
    PacketHeader outPktHdr, inPktHdr;
    MailHeader outMailHdr, inMailHdr;

    //MV variables
    int valSize,
        mvID,
        mvIndex,
        valIndex,
        value,
        valid;

    //Lock variables
    int lockID,
        lockIndex;
    bool acq;
    //Condition Variables
    int cvID,
        cvIndex;

    std:string type,
                name;
    stringstream ss;
    printf("Server is up and running\n");

    while(true)
    {
        
        postOffice->Receive(0, &inPktHdr, &inMailHdr, buffer);
       // printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
        fflush(stdout);
        outMailHdr.from = 0; 
        outPktHdr.from = 0; 
        outPktHdr.to = inPktHdr.from;
        outMailHdr.to = inMailHdr.from;

        ss.clear();
        ss.str("");
        ss << buffer;
        ss >> type;

        if(type == "CMV")
        {
  
            ss >> name >> valSize >> value;

         //   printf("Creating MV, name: %s   %d   %d\n", name, valSize, value );

            mvID = g_serverData->FindMVIndex(name,valSize, value);
            if(mvID == -1)
            {
                printf("Bad name size passed in\n");
                interrupt->Halt();
            }
            ss.clear();
            ss.str("");
            ss << type << " " <<mvID;
        //    printf("Sending MV \"%s\" index %d back to client \n",name,mvID);
            sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
            
        }
        else if(type == "GMV")
        {
            ss >> mvIndex >> valIndex;
            
          //  printf("Getting MV at: %d Value: %d\n", mvIndex, valIndex);
          
            if(  IsMvValid( mvIndex, valIndex, type , "Getting MV failed!", outPktHdr, outMailHdr) )
            {

                    value = g_serverData->GetMV(mvIndex, valIndex);
                    printf("Get MV \"%d\" success, value: %d !\n",mvIndex, value);
                    ss.clear();
                    ss.str("");
                    ss << type << " " << value;
                    sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
            }
           
       }
        else if(type =="SMV")
       {
        ss >> mvIndex >> value >> valIndex;

        //set the member vairalbe to a specific value
      //  printf("Setting MV %d value %d at: %d\n", mvIndex, value, valIndex);

            if(  IsMvValid( mvIndex, valIndex, type , "Setting MV failed!", outPktHdr, outMailHdr))
            {
                //set the value in the struct
                valid = g_serverData->SetMV(mvIndex, value, valIndex);
              //  printf("Set value success!\n");
                ss.clear();
                ss.str("");
                ss << type << " " <<1;
                sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
                
            }

       }
    
        else if(type == "DMV")
       {

            ss >> mvIndex;
            //destroying a member variable
            printf("Destroying MV id: %d\n", mvIndex);
            if(  IsMvValid( mvIndex, valIndex, type , "Destroy MV failed!", outPktHdr, outMailHdr))
            {
             //   printf("Destroy MV success!\n");
                g_serverData->DestroyMV(mvIndex);
                ss.clear();
                ss.str("");
                ss << type << " " <<1;
 
                sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
            }
       }
        else if(type == "CLK")
        {
            ss >> name;

          //  printf("Creating Lock, name: %s\n", name);
            //creating a lock and 
            lockID = g_serverData->FindLockIndex(name);
            if(lockID == -1)
            {
                printf("Bad name size passed in\n");
                interrupt->Halt();
            }
            ss.clear();
            ss.str("");
            ss << type << " " <<lockID;
            sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);

        }

        else if(type == "DLK")
        {
            ss >> lockIndex;
            //delete the lock and make sure it is valid
            if(  IsLockValid( lockIndex, type , "Destroy Lock failed!", outPktHdr, outMailHdr))
            {
                g_serverData->DestroyLock(lockIndex);
                ss.clear();
                ss.str("");
                ss << type << " " <<1;
                sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
            }
        }

        else if(type == "ACQ")
        { 
            ss >> lockIndex;
          //  printf("Acquiring Lock id: %d\n", lockIndex);
            //make sure the lock is valid
            if(  IsLockValid( lockIndex, type , "Acquire Lock failed!", outPktHdr, outMailHdr))
            {
                //aquire the lock and if it is not free add ourselves to the wait queue
                acq = g_serverData->Acquire(lockIndex , buffer, inPktHdr.from, inMailHdr.from);

                if(acq)
                {
                    printf("Acquire Lock success!\n");
                    ss.clear();
                    ss.str("");
                    ss << type << " " <<1;
                    sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
                }
                else
                {
                    printf("Gotta wait\n");
                }
            }
        }

        else if(type == "REL")
        {
            ss >> lockIndex;

            //make sure the lock is valid
            if(  IsLockValid( lockIndex, type , "Release Lock failed!", outPktHdr, outMailHdr)
                && g_serverData->DoesClientOwnLock(lockIndex,inPktHdr.from, inMailHdr.from ))
            {
                
                Response *rep = g_serverData->Release(lockIndex ,inPktHdr.from, inMailHdr.from);
              
                ss.clear();
                ss.str("");
                if(rep != NULL)
                {
                    printf("Release Lock With addign other client!\n");
                    ss << type << " " <<1;
                    sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
                    outPktHdr.to = rep->m_ownerMachineID;
                    outMailHdr.to = rep->m_ownerMailboxID;
                    sendToClient(rep->m_msg,outPktHdr,inPktHdr,outMailHdr,inMailHdr);
                }
                else
                {
                    printf("Release Lock success!\n");
                    ss << type << " " <<1;
                    sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
                }
            }
        }

        else if(type == "CCV")
        {
            ss >> name;

           // printf("Creating CV, name: %s\n", name);
            //find the lock, and if we dont we will create the lock
            cvID = g_serverData->FindCVIndex(name);
            if(cvID == -1)
            {
                printf("Bad name size passed in\n");
                interrupt->Halt();
            }
            ss.clear();
            ss.str("");
            ss << type << " " <<cvID;
            //return the id of the CV we just created
         //   printf("Sending CV \"%s\" index %d back to client \n",name,cvID);
            sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
        }
   
        else if(type == "DCV")
        {   
            ss >> cvIndex;
           // printf("Destroying CV id: %d\n", cvIndex);
            //make sure the lock is valid to destroy
            if(  IsCVValid( cvIndex, type , "Destroy CV failed!", outPktHdr, outMailHdr))
            {
             //   printf("Destroy CV success!\n");
                g_serverData->DestroyCV(cvIndex);
                ss.clear();
                ss.str("");
                ss << type << " " <<1;
                sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
            }
        }
    
        else if(type == "SIG")
        {
            ss >> cvIndex >> lockIndex;
            //making sure the the locks are valid and wont kill anything
            if(  IsLockValid( lockIndex, type , "Signal lock failed!", outPktHdr, outMailHdr) 
                &&  IsCVValid( cvIndex, type , "Singal CV failed!", outPktHdr, outMailHdr))
            {
                 //make srue the lock is owned by the owner and the lock being passed in
                //matched the CV
                if( g_serverData->DoesClientOwnLock( lockIndex , inPktHdr.from, inMailHdr.from) 
                    && g_serverData->DoesLockMatchCV(cvIndex, lockIndex))
                {
                    WaitingClient* client = g_serverData->GetWaitingClient( cvIndex );
                    if(client)
                    {
                        ss.clear();
                        ss.str("");
                        ss << "SIG" << " " << lockIndex;
                        //this will reply to the client that sent the signal
                        sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);

                        outPktHdr.to = client->m_ownerMachineID;
                        outMailHdr.to = client->m_ownerMailboxID;
                        ss.clear();
                        ss.str("");
                        ss << "ACQ" << " " << lockIndex;
               
                        //if we acqure the lock we will then send a message to the client,
                        //otherwise we will wait for a release
                        if( g_serverData->Acquire(lockIndex ,  ss.str() , client->m_ownerMachineID, client->m_ownerMailboxID))
                        {
                            ss.clear();
                            ss.str("");
                            ss << type << " " << lockIndex;
                            //tell the signlaed client we just got woken up
                            sendToClient(ss.str(),outPktHdr,inPktHdr,outMailHdr,inMailHdr);
                        }
                    
                   }
                   else
                   {
                        printf("Signaling null wait lock\n");
                   }


                }
                else
                {
                    printf("Singal does not match up!\n");
                }

            }
        }
        else if(type == "WEI")//here we will handle thewait
        {
            ss >> cvIndex >> lockIndex;
            //printf("Waiting\n");
            //we need to make sure that this the lock and the singal lock is valid
             if(  IsLockValid( lockIndex, type , "Signal lock failed!", outPktHdr, outMailHdr) 
                &&  IsCVValid( cvIndex, type , "Singal CV failed!", outPktHdr, outMailHdr))
            {
                //make srue the lock is owned by the owner and the lock being passed in
                //matched the CV
                if( g_serverData->DoesClientOwnLock( lockIndex , inPktHdr.from, inMailHdr.from) )
                {
                    ss.clear();
                    ss.str("");
                    Response *rep = g_serverData->Release(lockIndex ,inPktHdr.from, inMailHdr.from);
                    if(rep != NULL)
                    {
                     //   printf("Alerting new acquire \"%s\"   %d    %d \n",rep->m_msg, rep->m_ownerMachineID, rep->m_ownerMailboxID );
                        printf("Alerting new acuire \n");
                        outPktHdr.to = rep->m_ownerMachineID;
                        outMailHdr.to = rep->m_ownerMailboxID;

                         sendToClient(rep->m_msg,outPktHdr,inPktHdr,outMailHdr,inMailHdr);
                    }
                    g_serverData->AddToWaitQueue(cvIndex, lockIndex, inPktHdr.from, inMailHdr.from );
                    
                }
                else
                {
                    printf("Client does not own the lock!\n");
                }

            }
        }

        else if(type == "BCT")
        {
      
        }

        else
        {
            printf("some weird input\n");
        }


    }
}


